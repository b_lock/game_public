<?php
namespace app\controllers;

use app\models\EventEmitter;
use app\models\Faq;
use app\models\FreePoints;
use app\models\Game;
use app\models\LoginForm;
use app\models\Ponger;
use app\models\Project;
use app\models\SharedMemory;
use app\models\SignupForm;
use app\models\Task;
use app\models\Tournament;
use app\models\User;
use app\models\UserAvatar;
use app\models\UserInfo;
use HttpInvalidParamException;
use phpDocumentor\Reflection\Types\String_;
use Yii;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\Cors;
use yii\web\BadRequestHttpException;
//use yii\web\Controller;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\rest\ActiveController;
use yii\web\UploadedFile;

//use yii\rest\Controller;

class ApiController extends ActiveController
{
    public $modelClass = false;

//    public $enableCsrfValidation = false;

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = true;
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function behaviors()
    {
        return [
            /*'corsFilter' => [
                'class' => Cors::className(),
                'cors' => [
                    'Access-Control-Allow-Credentials' => true,
                    // restrict access to
//                    'Origin' => ['*', '*'],
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET', 'OPTIONS'],
//                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers' => [
//                        'access-control-allow-origin',
                        'authorization',
                        'X-Requested-With',
                        'content-type',
                    ],
//                    'Access-Control-Request-Headers' => ['*'],
//                    'Access-Control-Allow-Headers' => ['access-control-allow-origin'],
//                    // Allow only headers 'X-Wsse'
//                    'Access-Control-Allow-Credentials' => true,
//                    // Allow OPTIONS caching
//                    'Access-Control-Max-Age' => 3600,
//                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
//                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],*/
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
//                    'Origin' => ['http://localhost:3000', 'http://localhost:3000/', 'http://localhost/', 'http://localhost'],
                    'Origin' => ['*'],
                    'Access-Control-Request-Method' => ['POST', 'PUT', 'GET', 'OPTIONS'],
                    // Allow only POST and PUT methods
                    'Access-Control-Request-Headers' => [
//                        'access-control-allow-origin',
                        'authorization',
//                        'X-Requested-With',
                        'content-type',
//                        'X-Wsse'
                    ],
                    // Allow only headers 'X-Wsse'
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Allow-Origin' => '*',
                    // Allow OPTIONS caching
                    'Access-Control-Max-Age' => 3600,
                    // Allow the X-Pagination-Current-Page header to be exposed to the browser.
                    'Access-Control-Expose-Headers' => ['X-Pagination-Current-Page'],
                ],

            ],
//            'authenticator' => [
//                'class' => CompositeAuth::className(),
//                'authMethods' => [
////                    HttpBasicAuth::className(),
//                    HttpBearerAuth::className(),
////                    QueryParamAuth::className(),
//                ],
//                'only' => ['projects', 'profile', 'check-token'],
////                'except' => ['*','login', 'register', 'ping', 'options', 'users-online-info', 'users-top', 'games-history', 'faq', 'initial-state'],
//            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'verbs' => ['POST'],
                        'actions' => [
                            'login',
                            'register',
                            'index',
                            'set-users-online'
                        ],
                        'roles' => ['?', 'user'],
                    ],
                    [
                        'allow' => true,
                        'verbs' => ['POST'],
                        'actions' => [
                            'take-place',
                            'take-off-place',
                            'set-user-choice',
                            'new-game',
                            'toggle-tournament-place',
                            'user-avatar',
                            'free-points'
                        ],
                        'roles' => ['user'],
                    ],
                    /*[
                        'allow' => true,
                        'verbs' => ['PUT'],
                        'actions' => ['update-task'],
                        'roles' => ['@', 'user'],
                    ],*/
                    [
                        'allow' => true,
                        'verbs' => ['GET'],
                        'actions' => [
                            'projects',
                            'get-tasks',
                            'get-task',
                            'index',
                            'ping',
                            'check-token',
                            'users-online-info',
                            'users-top',
                            'games-history',
                            'faq',
                            'initial-state',
                            'check-games'
                        ],
                        'roles' => ['?', 'user'],
                    ],
                    [
                        'allow' => true,
                        'verbs' => ['GET'],
                        'actions' => [
                            'check-token'
                        ],
                        'roles' => ['user'],
                    ],
                    [
                        'allow' => true,
                        'verbs' => ['OPTIONS'],
                        'actions' => ['options'],
                        'roles' => ['?', '@', 'user'],
                    ],
                    [
                        'allow' => true,
                        'verbs' => ['POST'],
                        'actions' => ['logout'],
                        'roles' => ['@', 'user'],
                    ],
                    // everything else is denied
                ],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();

        // отключить действия "delete" и "create"
//        unset($actions['delete'], $actions['create'], $actions['options'], $actions['index']);

        // настроить подготовку провайдера данных с помощью метода "prepareDataProvider()"
//        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }


    public function actionLogin()
    {
        $json = json_decode(Yii::$app->request->getRawBody());

        $login = $json->login;
        $password = $json->password;
        $model = new LoginForm();
        if ($model->load([
                'LoginForm' => [
                    'username' => $login,
                    'password' => $password
                ]
            ]) && $model->login()
        ) {
            $token = User::findByUsername($model->username)->getJWT();
            Yii::info('Login: ' . $token);
            $cookies = Yii::$app->response->cookies;
            $cookies->add(new \yii\web\Cookie([
                'name' => 'jwt_token',
                'value' => $token,
            ]));

            $user_info = UserInfo::findOne(['user_id' => Yii::$app->user->id]);
            EventEmitter::refresh();
            return [
                'username' => $model->username,
                'id' => Yii::$app->user->id,
                'avatar' => $user_info->avatar ? $user_info->avatar : "",
                'points' => $user_info->balance ? $user_info->balance : null,
                'userInfo' => User::getUserGamesInfo(Yii::$app->user->id)
            ];
        } else {
            throw new UnauthorizedHttpException('wrong password or username');
        }
    }

    public function actionRegister()
    {
        $reqData = json_decode(Yii::$app->request->getRawBody());
        $model = new SignupForm();
        if ($model->load(
            [
                'SignupForm' => [
                    'username' => $reqData->login,
                    'email' => $reqData->email,
                    'password' => $reqData->password,
                ]
            ]
        )
        ) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    $token = User::findByUsername($model->username)->getJWT();
                    $cookies = Yii::$app->response->cookies;
                    $cookies->add(new \yii\web\Cookie([
                        'name' => 'jwt_token',
                        'value' => $token,
                    ]));
                    EventEmitter::refresh();
                    return [
                        'username' => Yii::$app->user->identity['username'],
                        'id' => Yii::$app->user->id,
                        'avatar' => "",
                        'points' => null
                    ];
                }
            }
        }

        return [
            "errors" => $model->errors
        ];
    }

    public function actionLogout()
    {
        User::exit();
        UserInfo::updateAll(['last_ping' => 0], ['user_id' => Yii::$app->user->id]);
        Yii::$app->user->logout();
        $cookies = Yii::$app->response->cookies;
        $cookies->remove('jwt_token');
        EventEmitter::refresh();
        return ['logout' => true];
    }

    public function actionCheckToken()
    {
        $user = User::findByUsername(Yii::$app->user->identity->username);
        $token = $user ? $user->getJwt() : null;
        $user_info = UserInfo::findOne(['user_id' => Yii::$app->user->id]);

        return [
            'username' => Yii::$app->user->identity->username,
            'id' => Yii::$app->user->id,
            'avatar' => $user_info->avatar ? $user_info->avatar : "",
            'points' => $user_info->balance ? $user_info->balance : null,
            'userInfo' => User::getUserGamesInfo(Yii::$app->user->id)
        ];

    }

    public function actionPing($id = null)
    {
//        $cookies = Yii::$app->response->cookies;
//
//// добавление новой куки в HTTP-ответ
//        $cookies->add(new Cookie([
//            'name' => 'language',
//            'value' => 'zh-CN',
//        ]));

        $ponger = Ponger::getPonger($id);

        return $ponger;
    }

    public function actionTakePlace()
    {
        $json = json_decode(Yii::$app->request->getRawBody());

        if (Game::takePlace($json->duelId, $json->side)) {
            EventEmitter::refresh();
            $ponger = Ponger::getPonger();
            return $ponger;
        }
        throw new BadRequestHttpException();
    }

    public function actionTakeOffPlace()
    {
        $json = json_decode(Yii::$app->request->getRawBody());

        if (Game::takeOffPlace($json->duelId, $json->side)) {
            EventEmitter::refresh();
            $ponger = Ponger::getPonger();
            return $ponger;
        }
        throw new BadRequestHttpException();
    }

    public function actionSetUserChoice()
    {
        $json = json_decode(Yii::$app->request->getRawBody());
        if ($json->duelId && (int)$json->duelId > 0) {
            $res = Game::SetUserChoice($json);
            if ($res) {
                EventEmitter::refresh();
                return $res;
            }
        }
        throw new BadRequestHttpException();
        //.userChoise
    }

    public function actionNewGame()
    {
        $json = json_decode(Yii::$app->request->getRawBody());
        Game::newGame($json->duelId);
        EventEmitter::refresh();
        return ['ok'];
    }

    public function actionToggleTournamentPlace()
    {
        $json = json_decode(Yii::$app->request->getRawBody());
        if ($json->place && Tournament::ToggleUserPlace($json->place)) {
            EventEmitter::refresh();
            $ponger = Ponger::getPonger();
            return $ponger;
        }

        throw new BadRequestHttpException();
    }

    public function actionUserAvatar()
    {
        $model = new UserAvatar();

        if (Yii::$app->request->isPost) {
            $model->avatar = UploadedFile::getInstanceByName('avatar');

            if ($model->avatar && $model->validate()) {
                $new_avatar = 'uploads' . DIRECTORY_SEPARATOR . Yii::$app->security->generateRandomString() . '.' . $model->avatar->extension;
                $model->avatar->saveAs($new_avatar);
                $user_info = UserInfo::findOne(['user_id' => Yii::$app->user->id]);
                if ($user_info) {
                    $old_avatar = $user_info->avatar;
                    $old_avatar_path = Yii::$app->basePath . DIRECTORY_SEPARATOR . "web" . DIRECTORY_SEPARATOR . $old_avatar;
                    if (is_file($old_avatar_path)) {
                        unlink($old_avatar_path);
                    }
                } else {
                    $user_info = new UserInfo();
                    $user_info->user_id = Yii::$app->user->id;
                }
                $user_info->avatar = $new_avatar;
                $user_info->save();
                return ['userAvatar' => $user_info->avatar];
            } else {
                return ['errors' => $model->errors['avatar']];
            }
        }
    }

    public function actionUsersOnlineInfo()
    {
        return User::getUsersOnlineInfo();

//        return [
//            [10 => '1'],
//        ];
    }

    public function actionUsersTop($req)
    {
        return User::getUsersTop($req);
    }

    public function actionGamesHistory($req)
    {
        return Game::getGamesHistory($req);

    }

    public function actionFaq()
    {
        return Faq::getFaqArray();
    }

    public function actionInitialState()
    {
        return Yii::$app->params['settings'];
    }

    public function actionFreePoints()
    {
        $json = json_decode(Yii::$app->request->getRawBody());
        if ($json->code) {
            $res = FreePoints::activate($json->code);
            if ($res) {
                return [
                    'status' => true,
                    'points' => $res
                ];
            }
        }

        return [
            'status' => false,
            'points' => null
        ];
    }

    public function actionSetUsersOnline()
    {
        $json = json_decode(Yii::$app->request->getRawBody());
        if ($json && $json->tokens) {
            $users = [];
            $usersId = [];
            foreach ($json->tokens as $token) {
                $user = new User();
                Yii::info($token);
                Yii::info(urldecode($token));
                $value = Yii::$app->getSecurity()->validateData(urldecode($token), Yii::$app->request->cookieValidationKey);
                Yii::info(@unserialize($value));
                $data = @unserialize($value);
                if (is_array($data) && isset($data[0], $data[1]) && $data[0] === 'jwt_token') {
                    $userData = $user->decodeJWT($data[1]);
                    if ($userData) {
                        $users[] = $userData;
                        if ($userData->uid) {
                            $usersId[] = $userData->uid;
                        }
                    }
                }
//                $users[] = User::decodeJWT($token);
                /*if ($user->username) {
                    $users[] = $$user;
                }*/
            }
            if (count($usersId)) {
                // UPDATE `customer` SET `status` = 1 WHERE `email` LIKE `%@example.com%`
//                Customer::updateAll(['status' => Customer::STATUS_ACTIVE], ['like', 'email', '@example.com']);
                UserInfo::updateAll(['last_ping' => 1], ['in', 'user_id', $usersId]);
                UserInfo::updateAll(['last_ping' => 0], ['not in', 'user_id', $usersId]);
                EventEmitter::refresh();
            }
            return ['ok'];
        }
        return ['ok'];
    }

    public function actionCheckGames($key)
    {
        if ($key === 'EXPjwTf2jbYJN2_') {
            Game::checkGames();
            Tournament::checkTournament();
            User::checkUsers();
        }
        return true;
    }
}