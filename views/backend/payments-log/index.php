<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentsLogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payments Logs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payments-log-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--    <p>-->
    <!--        --><? //= Html::a('Create Payments Log', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'user_id',
                'label' => 'username',
                'value' => 'user.username'
            ],
            'tag',
            'points',
            'notes',
            'created_at' => [
                'attribute' => 'created_at',
                'label' => 'created_at',
                'format' => 'datetime',
                'filter' => false,
                'value' => 'created_at'
            ],
            'updated_at' => [
                'attribute' => 'updated_at',
                'label' => 'updated_at',
                'format' => 'datetime',
                'filter' => false,
                'value' => 'updated_at'
            ]

//            [
//                    'class' => 'yii\grid\ActionColumn',
//                'template' => ' {visible} <br> {view} <br>  {update} <br>  {delete}',
//            ],
        ],
    ]); ?>
</div>
