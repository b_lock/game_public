<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\FreePointsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Free Points';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="free-points-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Free Points', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'points',
            'code',
//            'paid_off',
            'paid_off' => [
                'attribute' => 'paid_off',
                'value' => function ($model) {
                    return $model->paid_off > 0 ? "Активированный" : "Свободный";
                },
                'filter' => array(0 => "Свободный", 1 => "Активированный"),
            ],

            ['class' => 'yii\grid\ActionColumn','template' => '{update} <br>  {delete}',],
        ],
    ]); ?>
</div>
