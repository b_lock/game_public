<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FreePoints */

$this->title = 'Create Free Points';
$this->params['breadcrumbs'][] = ['label' => 'Free Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="free-points-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
