<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $created */

$this->title = 'Created code';
$this->params['breadcrumbs'][] = ['label' => 'Free Points', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="free-points-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    </p>

    <?php
    $created = array_reverse($created);
    foreach ($created as $key => $value) {
        echo $value['code'] . "<br>";
    } ?>

</div>
