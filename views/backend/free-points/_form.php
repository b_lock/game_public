<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\FreePoints */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="free-points-form">

    <?php $form = ActiveForm::begin();

    if ($model->isNewRecord) {
        echo $form->field($model, 'points')->textInput();

        echo $form->field($model, 'count')->textInput(['maxlength' => true]);
    } else {
        $items = [
            '0' => 'Свободный',
            '1' => 'Активированный',
        ];
        $params = [
//        'prompt' => 'Выберите статус...'
        ];
        echo $form->field($model, 'paid_off')->dropDownList($items, $params);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
