<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin();

    $items = [
        '10' => 'Активный',
        '0' => 'Заблокирован'
    ];
    $params = [
//       'prompt' => 'Выберите статус...'
    ];
    echo $form->field($model, 'status')->dropDownList($items, $params);

    ?>



    <?
    $items = [
        'admin' => 'Admin',
        'user' => 'User'
    ];
    $params = [
//        'prompt' => 'Выберите роль...'
    ];
    echo $form->field($model, 'role')->dropDownList($items, $params);
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
