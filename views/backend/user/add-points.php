<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AddPointsForm */
/* @var $form ActiveForm */
?>
<div class="backend-user-add-points">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'points') ?>
    <?
    $items = [
        'add' => '+ Добавить',
        'sub' => '- Отнять'
    ];
    $params = [
//        'prompt' => 'Выберите роль...'
    ];
    echo $form->field($model, 'type')->dropDownList($items, $params);
    ?>
<!--    --><?//= $form->field($model, 'type') ?>
    <?= $form->field($model, 'notes')->textarea() ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- backend-user-add-points -->
