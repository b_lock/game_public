<?php

use yii\grid\DataColumn;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <!--        --><? //= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'username',
//            'auth_key',
//            'password_hash',
//            'password_reset_token',
            'userInfo.balance',
//            'userInfo.avatar:image',
            'userInfo.avatar' => [
                'class' => DataColumn::className(),
                'attribute' => 'avatar',
                'format' => 'image',
                'filter' => false,
                'value' => function ($model) {
                    return $model->userInfo->avatar ? ("\\" . $model->userInfo->avatar) : false;
                }
            ],
            'email:email',
            'status' => [
                'attribute' => 'status',
                'value' => function ($model) {
                    return $model->status > 0 ? "Активен" : "Заблокирован";
                },
                'filter' => array(0 => "Заблокирован", 10 => "Активен"),
            ],
            'role' => [
                'attribute' => 'role',
//                "label" => 'Выведен в футер',
//                'value' => 'bookmaker.name',
                'format' => 'raw',
                'value' => 'role',
                'filter' => array("admin" => "admin", "user" => "user"),
            ],
//            'created_at:datetime',
//            'updated_at:datetime',

//            ['class' => 'yii\grid\ActionColumn'],
            [
                'class' => \yii\grid\ActionColumn::className(),
                'buttons' => [

                    'visible' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-usd"></span>', \yii\helpers\Url::to(['add-points', 'id' => $model->id]), ['title' => 'добавить поинтов']);

                    },

                ],
                'template' => ' {visible} <br> {view} <br>  {update} <br>  {delete}',
            ]
        ],
    ]); ?>
</div>
