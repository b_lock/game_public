<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <!--<img src="<? /*= $directoryAsset */ ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>-->
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->isGuest ? "" : Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Settings', 'icon' => 'fa fa-file-code-o', 'url' => ['/backend-settings']],
                    ['label' => 'Users', 'icon' => 'fa fa-file-code-o', 'url' => ['/backend-user']],
                    ['label' => 'Payments Logs', 'icon' => 'fa fa-file-code-o', 'url' => ['/backend-payments-log']],
                    ['label' => 'Faq', 'icon' => 'fa fa-file-code-o', 'url' => ['/backend-faq']],
                    ['label' => 'Free Points', 'icon' => 'fa fa-file-code-o', 'url' => ['/backend-free-points']],
//                    ['label' => 'Form2', 'icon' => 'fa fa-file-code-o', 'url' => ['/admin-forms2']],
//                    ['label' => 'Form3', 'icon' => 'fa fa-file-code-o', 'url' => ['/admin-forms3']],
//                    ['label' => 'Form4', 'icon' => 'fa fa-file-code-o', 'url' => ['/admin-forms4']],
//                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    $_SERVER['REMOTE_ADDR'] !== "127.0.0.1" ? "" : [
                        'label' => 'Gii',
                        'icon' => 'fa fa-file-code-o',
                        'url' => ['/gii']
                    ],
                    $_SERVER['REMOTE_ADDR'] !== "127.0.0.1" ? "" : [
                        'label' => 'Debug',
                        'icon' => 'fa fa-dashboard',
                        'url' => ['/debug']
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Forms links',
                        'icon' => 'fa fa-share',
                        'active' => true,
                        'url' => '#',
                        'items' => [
//                            [
//                                'label' => 'form1',
//                                'icon' => 'fa fa-file-code-o',
//                                'url' => ['/forms/form1'],
//                                'template' => '<a href="{url}" target="_blank">{icon}{label}</a>',
//                            ],
//                            [
//                                'label' => 'form2',
//                                'icon' => 'fa fa-dashboard',
//                                'url' => ['/forms/form2'],
//                                'template' => '<a href="{url}" target="_blank">{icon}{label}</a>',
//                            ],
//                            [
//                                'label' => 'form3',
//                                'icon' => 'fa fa-dashboard',
//                                'url' => ['/forms/form3'],
//                                'template' => '<a href="{url}" target="_blank">{icon}{label}</a>',
//                            ],
//                            [
//                                'label' => 'form4',
//                                'icon' => 'fa fa-dashboard',
//                                'url' => ['/forms/form4'],
//                                'template' => '<a href="{url}" target="_blank">{icon}{label}</a>',
//                            ],
                            /*[
                                'label' => 'Level One',
                                'icon' => 'fa fa-circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'fa fa-circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],*/
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
