<?php
$params = require(__DIR__ . '/test_params.php');
$dbParams = require(__DIR__ . '/test_db.php');

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__),    
    'language' => 'en-US',
    'components' => [
        'db' => $dbParams,
        'mailer' => [
            'useFileTransport' => true,
        ],
       /* 'urlManager' => [
            'showScriptName' => true,
        ],*/
        'user' => [
            'identityClass' => 'app\models\User',
        ],        
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],
        'urlManager'   => [
            'enablePrettyUrl'     => true,
            'enableStrictParsing' => false,
            'showScriptName'      => false,
            'rules'               => [
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => 'api',
                    'pluralize'     => false,
                    'extraPatterns' => [
                        'OPTIONS <action>' => 'options',
                        'POST check-token' => 'check-token',
                    ],
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => 'user',
                    'pluralize'     => false,
                    'extraPatterns' => [
                        'POST test'        => 'post/create',
                        'OPTIONS <action>' => 'options',
                        'POST check-token' => 'check-token',
                        'GET optn'         => 'test',
                    ],

                ]

            ],
        ]
    ],
    'params' => $params,
];
