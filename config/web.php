<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'name' => 'KNB',
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'language' => 'ru',
    'bootstrap' => [
        'log',
        'bootsrtapCongTypeGames' => function () {
            $types = \app\models\GameType::find()->all();
            /** @var \app\models\GameType $type */
            $game_types = [];
            foreach ($types as $type) {
                $game_types[$type->id] = $type;
            }
            Yii::$app->params['game_types'] = $game_types;

            $settings = \app\models\Settings::find()->limit(1)->one();
            if ($settings) {
                Yii::$app->params['settings'] = $settings;
            }

        }
    ],
    'components' => [
//        'session' => [
//            'cookieParams' => [
////                'path' => '/path1/;localgost;/',
//                'HttpOnly' => true
//            ]
//        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'csUOGTzaMib0HoncCi59yWrb80p_pkVX',
            'parsers' => [
//                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => '/backend/login'
        ],
        'errorHandler' => [
            'errorAction' => 'backend/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logVars' => ['_POST'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

//        'urlManager' => [
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
////            'enableStrictParsing' => true,
//            'rules' => [
//                ['class' => 'yii\rest\UrlRule', 'controller' => 'user'],
//            ],
//        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => false,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'api',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'OPTIONS <action>' => 'options',
                        'POST check-token' => 'check-token',
                        'OPTIONS users-top-info/<req>' => 'options',
                        'GET users-top-info/<req:[a-z]+>' => 'users-top',
                        'OPTIONS games-history-info/<req>' => 'options',
                        'GET games-history-info/<req:[a-z]+>' => 'games-history',
                    ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'user',
                    'pluralize' => false,
                    'extraPatterns' => [
                        'POST test' => 'post/create',
                        'OPTIONS <action>' => 'options',
                        'POST check-token' => 'check-token',
                        'GET optn' => 'test',
                    ],

                ],
                '/history/tournament' => 'backend/frontend',
                '/history/duel' => 'backend/frontend',
                '/users-online' => 'backend/frontend',
                '/top/all' => 'backend/frontend',
                '/top/day' => 'backend/frontend',
                '/top/week' => 'backend/frontend',
                '/top/month' => 'backend/frontend',
                '/faq' => 'backend/frontend',
                '/' => 'backend/frontend'


            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['admin', 'user'],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_DEV && $_SERVER['REQUEST_URI'] !== '/api/check-games?key=EXPjwTf2jbYJN2_') {

    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'traceLine' => '<a href="phpstorm://open?url={file}&line={line}">{file}:{line}</a>',
        'historySize' => 500,
        'allowedIPs' => ['*'],
    ];
//    $config['components']['session']['cookieParams']['HttpOnly'] = false;

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

//Yii::info(print_r($_SERVER['REQUEST_URI'], true));
return $config;
