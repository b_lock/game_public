<?php
$db = require(__DIR__ . '/db.php');
// test database! Important not to run tests on production or development databases
$db['dsn'] = 'mysql:host=localhost;dbname=advanced-todo-test';
$db['username'] = 'advanced-todo-test';
$db['password'] = 'advanced-todo-test';

return $db;