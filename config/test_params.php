<?php

return [
    'adminEmail' => 'vovmog@gmail.com',
    'supportEmail' => 'vovmog@gmail.com',
    'JWT_SECRET' => 'JWT_Secret',
    'JWT_EXPIRE' => 10*24*60*60,
    'user.passwordResetTokenExpire'=> 1000*24*60*60
];
