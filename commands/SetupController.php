<?php
namespace app\commands;

namespace app\commands;

use app\models\User;
use app\models\UserInfo;
use yii\console\Application;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Test controller
 */
class SetupController extends Controller
{

    public function actionIndex()
    {

        $user = new User();
        $user->username = 'admin';
        $user->email = 'admin@mail.com';
        $user->setPassword('123456');
        $user->role = 'admin';
        $user->generateAuthKey();

        if ($user->save()) {
            $userInfo = new UserInfo();
            $userInfo->user_id = $user->id;
            if ($userInfo->save()) {
                echo "User created";
            }
        }
        echo "Something wrong ";

    }


}