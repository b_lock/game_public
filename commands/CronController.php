<?php
namespace app\commands;

namespace app\commands;

use app\models\EventEmitter;
use app\models\User;
use app\models\UserInfo;
use Symfony\Component\Validator\Constraints\Date;
use yii\console\Application;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

/**
 * Test controller
 */
class CronController extends Controller
{

    public function actionIndex()
    {
        EventEmitter::refresh();
        echo "OK \n";
    }

    public function actionSocket()
    {
        $client = new Client(new Version1X('http://localhost:8081'));
        $client->initialize();
        $client->of('/server');
        $time = time();
        while (true) {
            var_dump(time());
            if (($time + 1) <= time()) {
                $client->emit('server_ping', []);
                $time = time();
            }

            $r = $client->read();
            if (!empty($r)) {
                var_dump($r);
                $d = date('h:i:s', time());
                var_dump($d);
            }

//            sleep(1);
        }
        $client->close();

    }


}