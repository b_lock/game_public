export const config = {
	'host': process.env.NODE_ENV === 'production' ? '/' : 'http://localhost/',
	'translate': {
		'stone': 'камень',
		'paper': 'бумага',
		'scissors': 'ножницы'
	}
};
