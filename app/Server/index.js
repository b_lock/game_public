const server = require('http').createServer();
const req = require('req-fast');

const io = require('socket.io')(server);

server.listen(8081);

function getCookiesArr(cookies) {
    const list = {};
    cookies && cookies.split(';').forEach(function (cookie) {
        const parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
    });
    return list;
}

function setUsersOnline(clientSock) {
    const keys = Object.keys(clientSock.connected);
    if (keys) {
        console.log('keys', keys);
        const cookies = [];
        keys.forEach(key => {
            cookies.push(getCookiesArr(io.of('/client').connected[key].client.request.headers.cookie));
        });

        const jwtTokens = [];
        cookies.forEach(cookie => {
            if (cookie.jwt_token) {
                jwtTokens.push(cookie.jwt_token);
            }
        });
        // console.log('jwtTokens', jwtTokens);
        const options = {
            url: 'http://localhost/api/set-users-online',
            method: 'POST',
            dataType: 'json',
            data: {'tokens': jwtTokens}
        };

        req(options, function (err, resp) {
            if (err) {
                console.log('[ERROR]', err.message);
            } else {
                console.log("getUsersOnline");
            }
        });
    }
}


const clientSock = io.of('/client').on('connection', function (socket) {
    console.log('connected from client');
    socket.emit('pong', {pong: true});
    setUsersOnline(clientSock);
    socket.on('disconnect', function () {
        console.log('disconect');
        setUsersOnline(clientSock);
    });
});


io.of('/server').on('connection', function (socket) {
    console.log('connection from server');
    socket.on('refresh', function (data) {
        console.log('refresh');
        if (data.key === 'EXPjwTf2jbYJN2_' && clientSock.connected) {
            clientSock.emit('refresh', data.data);
        }
    });
});
setInterval(function () {
    const options = {
        url: 'http://localhost/api/check-games?key=EXPjwTf2jbYJN2_',
        method: 'GET'
    };

    req(options, function (err, resp) {
        if (err) {
            console.log('[ERROR]', err.message);
        }
    });
}, 1000);
