import React from 'react';
import { Route, IndexRoute, Redirect } from 'react-router';
import App from './components/App';
import MainPage from './components/MainPage';
import DuelPage from './components/DuelPage';
import TournamentPage from './components/TournamentPage';
import UsersOnline from './components/UsersOnline';
import TopTableComponent from './components/TopTableComponent';
import TopAllTableComponent from './components/TopAllTableComponent';
import TopDayTableComponent from './components/TopDayTableComponent';
import TopWeekTableComponent from './components/TopWeekTableComponent';
import TopMonthTableComponent from './components/TopMonthTableComponent';
import HistoryComponent from './components/HistoryComponent';
import HistoryTournamentComponent from './components/HistoryTournamentComponent';
import HistoryGamesComponent from './components/HistoryGamesComponent';
import FaqComponent from './components/FaqComponent';
export default (
    <Route path="/" component={App}>
        <IndexRoute component={MainPage}/>
        <Route path="/users-online" component={UsersOnline}/>
        <Redirect from="/top" to="/top/all"/>
        <Route path="/top" component={TopTableComponent}>
            <Route path="/top/all" component={TopAllTableComponent}/>
            <Route path="/top/day" component={TopDayTableComponent}/>
            <Route path="/top/week" component={TopWeekTableComponent}/>
            <Route path="/top/month" component={TopMonthTableComponent}/>
        </Route>
        <Route path="/faq" component={FaqComponent}/>
        <Redirect from="/history" to="/history/tournament"/>
        <Route path="/history" component={HistoryComponent}>
            <Route path="/history/tournament" component={HistoryTournamentComponent}/>
            <Route path="/history/duel" component={HistoryGamesComponent}/>
        </Route>
        <Route path="/duel" component={DuelPage}/>
        <Route path="/tournament" component={TournamentPage}/>
    </Route>
);
