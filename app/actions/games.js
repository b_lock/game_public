const _ = require('lodash');

import * as types from './types';
import * as api from '../helpers/api';
import {renderCanvasImg} from '../helpers/renderCanvasImg';
import {config} from '../config';
import {logout} from './user';


export function setPlayersInfo(playersObj) {
    return {
        type: types.SET_PLAYERS_INFO,
        playersObj
    };
}

export function checkPlayers(players) {
    return (dispatch, getState) => {
        const state = getState();

        const promiceImg = [];
        _.forIn(players, function (value) {
            if (!state.playersInfo[value.id] || (state.playersInfo[value.id] && state.playersInfo[value.id].avatar !== value.avatar)) {
                promiceImg.push(renderCanvasImg(value));
                // renderCanvasImg(value.avatar).then((val) => {
                //     console.log(val);
                //     if (!playersObj) {
                //         playersObj = {};
                //     }
                //     playersObj[value.id] = {
                //         avatar: value.avatar
                //     };
                // });
            }
        });
        Promise.all(promiceImg).then((val) => {
            const playersObj = {};
            val.forEach(player => {
                playersObj[player.id] = {
                    avatarHex: player.avatarHex,
                    avatarHexHover: player.avatarHexHover
                };
            });
            dispatch(setPlayersInfo(playersObj));
        });
        // if (playersObj) {
        //     console.log('setPlayersInfo', playersObj);
        //     dispatch(setPlayersInfo(playersObj));
        // }
    };
}

export function setGamesT(pong) {
    return {
        type: types.SET_GAMES,
        pong
    };
}

export function setGames(pong) {
    return (dispatch) => {
        dispatch(checkPlayers(pong.tournament.players));
        dispatch(setGamesT(pong));
    };
}

export function setActiveGame(activeGame) {
    return {
        type: types.SET_ACTIVE_GAME,
        activeGame
    };
}
export function setActiveGameType(activeGameType) {
    return {
        type: types.SET_ACTIVE_GAME_TYPE,
        activeGameType
    };
}
export function setPlayedGame(activeGame) {
    return {
        type: types.SET_PLAYED_GAME,
        activeGame
    };
}

export function resetPlayedGame(activeGame) {
    return {
        type: types.RESET_PLAYED_GAME,
        activeGame
    };
}

export function takePlace(side, duelId) {
    return (dispatch) => {
        return api.apiPost(
            `${config.host}api/take-place`,
            {
                side: side,
                duelId: duelId
            })
            .then(response => {
                if (response.status >= 400) {
                    if (response.status === 403 || response.status === 401) {
                        return dispatch(logout());
                    }
                    throw new Error("Bad response from server");
                    // return dispatch(errorFieldLogin({userName: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                // console.log(json);
                return dispatch(setGames(json.data));
            })
            .catch(() => {
                throw new Error("Something wrong");
            });
    };
}
export function takeOffPlace(side, duelId) {
    return (dispatch) => {
        return api.apiPost(
            `${config.host}api/take-off-place`,
            {
                side: side,
                duelId: duelId
            })
            .then(response => {
                if (response.status >= 400) {
                    if (response.status === 403 || response.status === 401) {
                        return dispatch(logout());
                    }
                    throw new Error("Bad response from server");
                    // return dispatch(errorFieldLogin({userName: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                // console.log(json);
                return dispatch(setGames(json.data));
            })
            .catch(() => {
                throw new Error("Something wrong");
            });
    };
}

export function setDuelReply(duels) {
    return {
        type: types.SET_DUEL_REPLY,
        duels
    };
}
export function setDuelReplyReset(duelId) {
    const duels = {};
    duels[duelId] = {'reply': false};
    return {
        type: types.SET_DUEL_REPLY_RESET,
        duels
    };
}

export function setUserChoice(userChoice, duelId) {
    return (dispatch) => {
        return api.apiPost(
            `${config.host}api/set-user-choice`,
            {
                userChoice: userChoice,
                duelId: duelId
            })
            .then(response => {
                if (response.status >= 400) {
                    if (response.status === 403 || response.status === 401) {
                        return dispatch(logout());
                    }
                    throw new Error("Bad response from server");
                }
                return response.json();
            })
            .then(json => {
                if (json.winner === 0) {
                    const duel = {};
                    duel[json.duelId] = {'reply': true};
                    return dispatch(setDuelReply(duel));
                } else if (json.winner === null) {
                    setTimeout(() => {
                        dispatch(setUserChoice(userChoice, duelId));
                    }, 1000);
                } else {
                    const duel = {};
                    duel[json.duelId] = {'reply': false};
                    return dispatch(setDuelReply(duel));
                }
                return false;
            })
            .catch(() => {
                throw new Error("Something wrong");
            });
    };
}

export function newGame(duelId) {
    return (dispatch) => {
        setTimeout(function () {
            console.log('new game');
            return api.apiPost(
                `${config.host}api/new-game`,
                {
                    duelId: duelId
                })
                .then(response => {
                    if (response.status >= 400) {
                        if (response.status === 403 || response.status === 401) {
                            return dispatch(logout());
                        }
                        throw new Error("Bad response from server");
                    }
                    return response.json();
                })
                .then((/* json */) => {
                    // console.log(json);
                    // dispatch(resetPlayedGame(duelId));
                })
                .catch(() => {
                    throw new Error("Something wrong");
                });
        }, 5000);
    };
}
export function toggleTournamentPlace(place) {
    return (dispatch) => {
        return api.apiPost(
            `${config.host}api/toggle-tournament-place`,
            {
                place: place
            })
            .then(response => {
                if (response.status >= 400) {
                    if (response.status === 403 || response.status === 401 || response.status === 401) {
                        return dispatch(logout());
                    }
                    throw new Error("Bad response from server");
                }
                return response.json();
            })
            .then((res) => {
                return dispatch(setGames(res.data));
            })
            .catch(() => {
                throw new Error("Something wrong");
            });
    };
}

