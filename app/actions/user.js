import { config } from '../config';
import * as types from './types';
import * as helpers from '../helpers/api';

import  socketConnect  from '../helpers/socket';

export function loginUser(json) {
    return {
        type: types.LOGIN,
        user: json,
    };
}

export function login(userName, password) {
    return (dispatch) => {
        return helpers.apiPost(
            `${config.host}api/login`,
            {
                login: userName,
                password: password
            })
            .then(response => {
                if (response.status >= 400) {
                    // throw new Error("Bad response from server");
                    return dispatch(errorFieldLogin({userName: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                if (json.username/* && json.token */) {
                    const userInfo = {...json, ...{avatar: json.avatar ? (config.host + json.avatar) : ""}};
                    localStorage.setItem('user', JSON.stringify(userInfo));
                    socketConnect.reconnect();
                    return dispatch(loginUser({...userInfo, isLogged: true}));
                }
                return json;
            });
    };
}


export function errorFieldFreePoints(status, points) {
    return {
        type: types.ERROR_FIELDS_FREE_POINTS,
        status: status,
        points: points
    };
}

export function freePoints(code) {
    return (dispatch) => {
        return helpers.apiPost(
            `${config.host}api/free-points`,
            {
                code: code,
            })
            .then(response => {
                if (response.status >= 400) {
                    // throw new Error("Bad response from server");
                    return dispatch(errorFieldFreePoints(false));
                }
                return response.json();
            })
            .then((json) => {
                return dispatch(errorFieldFreePoints(json.status, json.points));
            });
    };
}

export function registration(userName, email, password) {
    return (dispatch) => {
        return helpers.apiPost(
            `${config.host}api/register`,
            {
                login: userName,
                email,
                password
            })
            .then(response => {
                if (response.status >= 400) {
                    // throw new Error("Bad response from server");
                    return dispatch(errorFieldLogin({userName: false, email: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                if (json.username/* && json.token */) {
                    const userInfo = {...json, ...{avatar: json.avatar ? (config.host + json.avatar) : ""}};
                    localStorage.setItem('user', JSON.stringify(userInfo));
                    return dispatch(loginUser({...userInfo, isLogged: true}));
                }
                if (json.errors) {
                    console.log(json.errors);
                    return dispatch(errorFieldLogin({
                        userName: !json.errors.username,
                        email: !json.errors.email,
                        password: !json.errors.password
                    }));
                }
                return json;
            });
    };
}

function logoutUser() {
    return {
        type: types.LOGOUT,
        user: {
            isLogged: false,
            username: '',
            token: '',
            avatar: '',
            points: null,
            userInfo: {}
        },
    };
}
export function setUserAvatar(avatar) {
    let user = localStorage.getItem('user');
    if (user) {
        user = JSON.parse(user);
        user.avatar = config.host + avatar;
        localStorage.setItem('user', JSON.stringify(user));
    }
    return {
        type: types.SET_USER_AVATAR,
        avatar: avatar ? config.host + avatar : "",
    };
}

export function logout() {
    return (dispatch) => {
        localStorage.setItem('user', JSON.stringify({}));
        return helpers.apiPost(
            `${config.host}api/logout`, {})
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            })
            .then((json) => {
                if (json.logout) {
                    socketConnect.reconnect();
                    return dispatch(logoutUser());
                }
                throw new Error("Bad response from server");
            });
    };
}

export function showLoginModal() {
    return {
        type: types.SHOW_LOGIN_MODAL
    };
}
export function showUserSettingsModal() {
    return {
        type: types.SHOW_USER_SETTINGS_MODAL
    };
}

export function hideUserSettingsModal() {
    return {
        type: types.HIDE_USER_SETTINGS_MODAL
    };
}
export function showRegistrationModal() {
    return {
        type: types.SHOW_REGISTRATION_MODAL
    };
}
export function hideLoginModal() {
    return {
        type: types.HIDE_LOGIN_MODAL
    };
}
export function hideRegistrationModal() {
    return {
        type: types.HIDE_REGISTRATION_MODAL
    };
}

export function errorFieldLogin(fields) {
    return {
        type: types.ERROR_FIELDS_LOGIN,
        action: fields
    };
}
export function errorFieldRegistration(fields) {
    return {
        type: types.ERROR_FIELDS_REGISTRATION,
        action: fields
    };
}
export function hideInfoModal() {
    return {
        type: types.HIDE_INFO_MODAL
    };
}
export function showInfoModal(title, text) {
    return {
        type: types.SHOW_INFO_MODAL,
        infoModalProp: {
            'title': title,
            'infoText': text
        }
    };
}
export function showFreePointsModal() {
    return {
        type: types.SHOW_FREE_POINTS_MODAL
    };
}
export function hideFreePointsModal() {
    return {
        type: types.HIDE_FREE_POINTS_MODAL
    };
}
