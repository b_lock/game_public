import { config } from '../config';
import * as types from './types';
import * as api from '../helpers/api';

// function setOnlineUserCount(count) {
//    return {
//        type: types.SET_ONLINE_USER_COUNT,
//        count:count ,
//    };
// }
export function setUsersOnlineInfo(userInfoOnline) {
    return {
        type: types.SET_ONLINE_USERS_INFO,
        userInfoOnline: userInfoOnline,
    };
}

export function getUsersOnlineInfo() {
    return (dispatch) => {
        return api.apiGet(
            `${config.host}api/users-online-info`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                    // return dispatch(errorFieldLogin({userName: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                // console.log(json);
                return dispatch(setUsersOnlineInfo(json));
            })
            .catch(() => {
                throw new Error("Something wrong");
            });
    };
}
export function setUsersTop(usersTop) {
    return {
        type: types.SET_USERS_TOP_INFO,
        usersTop: usersTop,
    };
}
export function getUsersTop(req) {
    return (dispatch) => {
        return api.apiGet(
            `${config.host}api/users-top-info/${req}`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                    // return dispatch(errorFieldLogin({userName: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                // console.log(json);
                return dispatch(setUsersTop(json));
            })
            .catch(() => {
                throw new Error("Something wrong");
            });
    };
}

export function setGamesHistory(gamesHistory) {
    return {
        type: types.SET_USERS_HISORY_INFO,
        gamesHistory: gamesHistory,
    };
}
export function getGamesHistory(req) {
    return (dispatch) => {
        return api.apiGet(
            `${config.host}api/games-history-info/${req}`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                    // return dispatch(errorFieldLogin({userName: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                // console.log(json);
                return dispatch(setGamesHistory(json));
            })
            .catch((e) => {
                console.error(e);
                throw new Error("Something wrong");
            });
    };
}

