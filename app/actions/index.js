import fetch from 'isomorphic-fetch';
import * as api from '../helpers/api';
import { config } from '../config';
import * as types from './types';

export function setFaq(faq) {
    return {
        type: types.SET_FAQ,
        faq
    };
}
export function spinner(status) {
    if (status === true) {
        return {
            type: types.SPINNER_ON
        };
    }
    return {
        type: types.SPINNER_OFF
    };
}

export function getFaq() {
    return (dispatch) => {
        // dispatch(spinner(true));
        return api.apiGet(
            `${config.host}api/faq`)
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                    // return dispatch(errorFieldLogin({userName: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                // console.log(json);
                dispatch(spinner(false));
                return dispatch(setFaq(json));
            })
            .catch((e) => {
                console.error(e);
                dispatch(spinner(false));
                throw new Error("Something wrong");
            });
    };
}

export function initState(init) {
    return {
        type: types.INIT_STATE,
        init
    };
}

export function filterTable(filter) {
    return {
        type: types.FILTER,
        filter
    };
}

export function testAction(filter) {
    return {
        type: types.TEST,
        filter
    };
}
function receivePosts(json) {
    return {
        type: types.RECEIVE_POSTS,
        posts: json.map(posts => posts),
        receivedAt: Date.now()
    };
}


export function testReq() {
    return dispatch => {
        return fetch('//offline-news-api.herokuapp.com/stories', {method: 'GET'})
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            })
            .then(json => dispatch(receivePosts(json)));
    };
}
export function testApi() {
    return (dispatch) => {
        return dispatch(testReq());
    };
}


