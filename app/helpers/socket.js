import io from 'socket.io-client';

const socketObj = {
    socket: undefined
};

function connect() {
    socketObj.socket = io.connect(`http://localhost:8081/client`);

    socketObj.socket.on('disconnect', function () {
        console.log('disconnect socket');
    });
    socketObj.socket.on('connect', function () {
        socketObj.socket.emit('ping', {'ping-pong': true});
        console.log('socket connected');
        // socket.close();
        socketObj.socket.on('pong', function () {
            console.log("socket.pong");
        });
        socketObj.socket.emit('usersOnline');
    });
    console.log(io, socketObj.socket);
}

const socketConnect = (function () {
    connect();
    socketObj.reconnect = function () {
        // debugger;
        let subs = [];
        console.log(socketObj.socket.listeners('refresh'));
        subs = socketObj.socket.listeners('refresh');

        socketObj.socket.disconnect();
        socketObj.socket.close();
        connect();
        subs.forEach(sub => {
            socketObj.socket.on('refresh', sub);
        });
        console.log(socketObj.socket.listeners('refresh'));
    };
    return socketObj;
})();

export default socketConnect;

