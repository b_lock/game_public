const _ = require('lodash');
// import io from 'socket.io-client';
// import cookie from 'react-cookie';

import * as helpers from '../helpers/api';
import { config } from '../config';
import { setActiveGame, setActiveGameType, setGames, setPlayedGame } from '../actions/games';
import { logout, loginUser } from '../actions/user';
import { initState } from '../actions';

import  socketConnect  from '../helpers/socket';

export const ping = function (store) {
    function checkStore(json) {
        const state = store.getState();

        // console.log(json, store.getState());
        if (json.status === 'updated') {
            const equal = _.isEqual(state.games, json.data);
            if (!equal) {
                // console.log('not Equal', json);

                // json.tournament.players.forEach((player) => {
                //     console.log(player);
                // });

                store.dispatch(setGames(json.data));
                const activeGame = json.data.duels.find(el => el.id === state.settings.activeGame);
                if (state.settings.activeGame === null || !activeGame) {
                    store.dispatch(setActiveGame(json.data.duels[0].id));
                    store.dispatch(setActiveGameType(json.data.duels[0].type));
                }
                /**
                 * Проверяем на игру у которой статус 1(старт игры)
                 * принудительно переключаем активную игру если игрок в ней учавствует (state.settings.activeGame = duelId),
                 * и переводим ее в актив и устанавливаем Date.now() если она еще не в активном состоянии
                 * */
                const activeGames = json.data.duels.filter(el => el.status === 1);
                if (activeGames.length &&
                    (
                        state.activeDuels.active[state.settings.activeGame] === undefined
                        || state.activeDuels.active[state.settings.activeGame].activeGame === false
                    )
                ) {
                    console.log('activeGames', activeGames);
                    activeGames.forEach(game => {
                        // console.log('setPlayedGame', state.activeDuels.active, state.settings.activeGame, !state.activeDuels.active || !state.activeDuels.active[state.settings.activeGame] || state.activeDuels.active[state.settings.activeGame].activeGame !== true);
                        store.dispatch(setPlayedGame(game.id));
                    });
                    const startGame = activeGames.find(el => {
                        return el.user1.id === state.user.id || el.user2.id === state.user.id;
                    });
                    if (startGame) {
                        store.dispatch(setActiveGame(startGame.id));
                        store.dispatch(setActiveGameType(startGame.type));
                    }
                }
            }
        }
    }

    console.log(socketConnect);

    // const socketPing = {
    //     connect: function () {
    //         this.socket = io.connect(`http://localhost:8081/client`);
    //         this.socket.on('disconnect', function () {
    //             console.log('disconnect socket');
    //         });
    //         this.socket.on('connect', function () {
    //             this.socket.emit('ping', {'ping-pong': true});
    //             console.log('socket connected');
    //             // socket.close();
    //             this.socket.on('pong', function () {
    //                 console.log("socket.pong");
    //             });
    //         }.bind(this));
    //         this.socket.on('refresh', function (json) {
    //             console.log("refresh", json);
    //             console.log(socket());
    //             console.log(socket());
    //             checkStore(json);
    //             this.socket.emit('refreshFromClient', {'refr': 'ok'});
    //         }.bind(this));
    //         console.log(io, this.socket);
    //     },
    //     reconnect: function () {
    //         this.socket.disconnect();
    //         this.socket.close();
    //         this.connect();
    //     }
    // };
    // socketPing.connect();

    // const socket = io.connect(`http://localhost:8081/client`);
    // socket.on('disconnect', function () {
    //     console.log('disconnect socket');
    // });
    // socket.on('connect', function () {
    //     socket.emit('ping', {'ping-pong': true});
    //     console.log('socket connected');
    //     // socket.close();
    //     socket.on('pong', function () {
    //         console.log("socket.pong");
    //     });
    // });
    // socket.on('refresh', function (json) {
    //     console.log("refresh", json);
    //     checkStore(json);
    //     socket.emit('refreshFromClient', {'refr': 'ok'});
    // });
    // console.log(io, socket);

    // const socket = io.connect('http://localhost:8081');
    // socket.on('news', function (data) {
    //    console.log(data);
    //    socket.emit('my other event', {my: 'data'});
    // });
    socketConnect.socket.on('refresh', function (json) {
        console.log("refresh", json);
        checkStore(json);
        socketConnect.socket.emit('refreshFromClient', {'refr': 'ok'});
    });
    (function () {
        /**
         * Chech User Token
         * */
        const state = store.getState();
        if (state.user.isLogged) {
            helpers.apiGet(`${config.host}api/check-token`)
                .then(r => {
                    if (r.status >= 400) {
                        throw new Error("Bad response from server");
                    }
                    return r.json();
                })
                .then(json => {
                    console.log(json);
                    if (json.username) {
                        const userInfo = {...json, ...{avatar: json.avatar ? (config.host + json.avatar) : ""}};
                        // localStorage.setItem('user', JSON.stringify(userInfo));
                        // if (json.token) {
                        //     cookie.save('token', json.token);
                        //     socketConnect.reconnect();
                        //     socketConnect.socket.emit('test', {'test': true});
                        // }
                        store.dispatch(loginUser({...userInfo, isLogged: true}));
                    } else {
                        store.dispatch(logout());
                        throw new Error("Not Authorisation");
                    }
                })
                .catch(() => {
                    store.dispatch(logout());
                    console.info('Not Authorisation');
                });
        }

        /**
         * Chech Initial State
         * */
        helpers.apiGet(`${config.host}api/initial-state`)
            .then(r => {
                if (r.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return r.json();
            })
            .then(json => {
                if (json) {
                    store.dispatch(initState(json));
                }
            })
            .catch(() => {
                throw new Error("Something wrong");
            });
        helpers.apiGet(`${config.host}api/ping`)
            .then((response) => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            })
            .then((json) => {
                checkStore(json);
            })
            .catch((e) => {
                throw new Error("Something wrong", e);
            });
    }());

    setInterval(function () {
        // socket.send('ping', {'ping-pong': true});
        // console.log('ping');
        // const state = store.getState();
        // const id = state.games.shmod || 0;
        // helpers.apiGet(`${config.host}api/ping?id=${id}`)
        //     .then((response) => {
        //         if (response.status >= 400) {
        //             throw new Error("Bad response from server");
        //         }
        //         return response.json();
        //     })
        //     .then((json) => {
        //         checkStore(json);
        //     })
        //     .catch((e) => {
        //         throw new Error("Something wrong", e);
        //     });
    }, 1000);
};

