import {config} from '../config';

export const renderCanvasImg = function (player) {
    const p1 = new Promise(
        function (resolve) {
            const canvas = document.createElement("CANVAS");
            canvas.width = 110;
            canvas.height = 98;
            const cxt = canvas.getContext('2d');
            cxt.clearRect(0, 0, 110, 110);
            const numberOfSides = 6;
            const size = 50;
            const Xcenter = 55;
            const Ycenter = 49;

            cxt.beginPath();
            cxt.strokeStyle = "#03585d";
            cxt.lineWidth = 1;
            cxt.moveTo(Xcenter + size * Math.cos(0), Ycenter + size * Math.sin(0));

            for (let i = 1; i <= numberOfSides; i += 1) {
                cxt.lineTo(Xcenter + size * Math.cos(i * 2 * Math.PI / numberOfSides), Ycenter + size * Math.sin(i * 2 * Math.PI / numberOfSides));
            }
            cxt.stroke();

            const x = document.createElement("CANVAS");
            const img = new Image();
            img.setAttribute('crossOrigin', 'anonymous');
            img.src = player.avatar ? config.host + player.avatar : require('../img/user-avatar.png');
            let avatarHexagon;
            img.onload = () => {
                let pr;
                let sx;
                let sy;
                let imgWidth;
                let imgHeight;
                if (img.width > img.height) {
                    pr = img.width / img.height;
                    sx = (img.width - img.height) / 2;
                    sy = 0;
                    imgHeight = img.height;
                    imgWidth = img.width / pr;
                } else {
                    pr = img.height / img.width;
                    sx = 0;
                    sy = (img.height - img.width) / 2;
                    imgWidth = img.width;
                    imgHeight = img.height / pr;
                }
                x.id = "canv";
                x.width = 110;
                x.height = 98;
                const ctx = x.getContext("2d");
                console.log(sx, sy, imgWidth, imgHeight);
                ctx.drawImage(img, sx, sy, imgWidth, imgHeight, 0, 0, 110, 98);
                const dataURL = x.toDataURL("image/png");
                const newImg = new Image();
                newImg.src = dataURL;
                const pattern = cxt.createPattern(newImg, "no-repeat");
                cxt.fillStyle = pattern;
                cxt.fill();
                avatarHexagon = canvas.toDataURL("image/png");

                cxt.clearRect(0, 0, 110, 110);
                cxt.shadowBlur = 0;
                cxt.shadowOffsetX = 0;
                cxt.shadowOffsetY = 0;
                cxt.stroke();

                cxt.shadowColor = '#3a9ca1';
                cxt.shadowBlur = 15;

                cxt.shadowOffsetX = 0;
                cxt.shadowOffsetY = 0;
                cxt.stroke();
                cxt.stroke();
                cxt.stroke();

                cxt.fillStyle = pattern;
                cxt.fill();

                const avatarHover = canvas.toDataURL("image/png");

                resolve({
                    id: player.id,
                    avatarHex: avatarHexagon,
                    avatarHexHover: avatarHover
                });
            };
        }
    );

    return p1;
};
