import fetch from 'isomorphic-fetch';

// headers.append('Authorization', 'Bearer ' + localStorage.getItem('auth_token')); //Вместо localStorage можно использовать сервис авторизации, с методом, который возвращает токен
// headers.append('Access-Control-Allow-Origin', '*');
// headers.append('X-Requested-With', 'XMLHttpRequest');

// const state = store.getState();
// let token = "";


function getHeaders() {
    // let user = localStorage.getItem('user');
    // if (user) {
    //     user = JSON.parse(user);
    //     if (user.token) {
    //         token = user.token;
    //     }
    // }
    return new Headers({
        // Authorization: 'Bearer ' + token,
        'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        Cookie: "dqawd:qwdwqd"
    });
}

export function apiGet(url) {
    /* const invocation = new XMLHttpRequest();
     if (invocation) {
     invocation.open('GET', url, true);
     invocation.withCredentials = true;
     invocation.onreadystatechange = function (data) {
     console.log(data);
     };
     invocation.send();
     } */

    return fetch(
        url,
        {
            method: 'GET',
            headers: getHeaders(),
            credentials: 'include',
            mode: 'cors',
        }
    )
        ;
}
export function apiPost(url, data) {
    return fetch(
        url,
        {
            method: 'POST',
            headers: getHeaders(),
            credentials: 'include',
            mode: 'cors',
            body: JSON.stringify(data)
        }
    );
}
export function apiPostUserAvatar(url, file) {
    const formData = new FormData();
    formData.append('avatar', file);
    return fetch(
        url,
        {
            method: 'POST',
            headers: getHeaders(),
            credentials: 'include',
            mode: 'cors',
           /* headers: new Headers({
                // Authorization: 'Bearer ' + token,
            }),*/
            body: formData
        }
    );
}
