import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from '../reducers';
import { ping } from '../helpers/ping';
// import {browserHistory} from 'react-router';

// function logger({getState}) {
//     return (next) => (action) => {
//         console.log('will dispatch', action);
//
//         if (action.filter === 'ff') {
//             browserHistory.replace('/about');
//         }
//
//         // Call the next dispatch method in the middleware chain.
//         const returnValue = next(action);
//
//         console.log('state after dispatch', getState());
//
//         // This will likely be the action itself, unless
//         // a middleware further in chain changed it.
//         return returnValue;
//     };
// }

// const enhancer = compose(
//     applyMiddleware(thunkMiddleware,logger)
// );


export default function configureStore() {
    // const store = createStore(
    //     rootReducer,
    //     initialState,
    //     window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
    //     enhancer
    // );
    //
    // return store;

    console.log('initial state');

    let initialState = {
        // user: {
        //    isLogged: false,
        //    username: '',
        //    token: ''
        // }
    };

    let user = localStorage.getItem('user');
    if (user) {
        user = JSON.parse(user);
        if (user.username /* user.token && */) {
            initialState = {user: {...user, isLogged: true, points: +user.points}};
        }
    }


    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const store = createStore(rootReducer, initialState, composeEnhancers(
        applyMiddleware(thunkMiddleware)
    ));
    ping(store);
    return store;
}
