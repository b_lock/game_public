import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { takePlace, takeOffPlace } from '../../actions/games';
import { showLoginModal, showInfoModal } from '../../actions/user';

import UserAvatarComponent from '../UserAvatarComponent';

import './style.scss';

class DuelChoice extends Component {

    componentWillMount() {
        // console.log('component will mount');
        // console.log('active game: ', nextProps.activeGame);
        this.duel = this.props.duels.find(duel => duel.id === this.props.activeGame);
        // console.log(this.duel);
    }

    componentWillUpdate(nextProps) {
        // console.log('component will update');
        // console.log('active game: ', nextProps.activeGame);
        this.duel = nextProps.duels.find(duel => duel.id === nextProps.activeGame);
    }

    showUserName(user) {
        const duelGame = this.props.duels.find(duel => duel.id === this.props.activeGame);
        return duelGame && duelGame[user] ? duelGame[user].userName : "";
    }

    takePlace(event) {
        if (this.props.isLogged) {
            if (this.props.userPoints >= this.props.types[this.props.activeGameType].rate) {
                if (!this.disabledBut || this.disabledBut < (Date.now())) {
                    this.disabledBut = Date.now() + 1000;
                    this.props.takePlace(event.currentTarget.dataset.id, this.props.activeGame);
                }
            } else {
                this.props.showInfoModal("Не хватает поинтов", "на вашем счету нет поинтов");
            }
        } else {
            // console.log('not login');
            this.props.showLoginModal();
        }
    }

    takeOffPlace(event) {
        if (!this.disabledBut || this.disabledBut < (Date.now())) {
            this.disabledBut = Date.now() + 1000;
            this.props.takeOffPlace(event.currentTarget.dataset.id, this.props.activeGame);
        }
    }

    userPlaceClass(side) {
        const duelGame = this.props.duels.find(duel => duel.id === this.props.activeGame);
        const busy = duelGame && duelGame[side] ? "busy" : "";
        return "user-place " + busy;
    }

    getButtonHtml(userSide) {
        if (this.duel && this.duel['user' + userSide] && this.duel['user' + userSide].userName && this.duel['user' + userSide].id === this.props.userId) {
            return (
                <div className="button-wrap">
                    <button data-id={userSide} onClick={::this.takeOffPlace}>освободить место</button>
                    <span className="wrapper-top"/>
                    <span className="wrapper-bottom"/>
                </div>
            );
        } else if (this.duel
            && this.duel['user' + userSide]
            && this.duel['user' + userSide].userName
            && this.duel['user' + userSide].id !== this.props.userId
            || (this.duel && this.duel['user' + (userSide === 1 ? 2 : 1)] && this.duel['user' + (userSide === 1 ? 2 : 1)].id === this.props.userId)) {
            return (
                <div className="button-wrap">
                </div>
            );
        }
        return (
            <div className="button-wrap">
                <button data-id={userSide} onClick={::this.takePlace}>Занять место</button>
                <span className="wrapper-top"/>
                <span className="wrapper-bottom"/>
            </div>
        );
    }

    render() {
        return (
            <div className="duelChoice">
                <div className="user-places">
                    <div className={::this.userPlaceClass('user1')}>
                        <UserAvatarComponent
                            className={::this.userPlaceClass('user1')}
                            name={this.duel && this.duel.user1 ? this.duel.user1.userName : '\u00A0'}
                            avatarImg={this.duel && this.duel.user1 ? this.duel.user1.avatar : ''}/>
                        {::this.getButtonHtml(1)}

                    </div>
                    <span className="vs">VS</span>
                    <div className={::this.userPlaceClass('user2')}>
                        <UserAvatarComponent
                            className={::this.userPlaceClass('user2')}
                            name={this.duel && this.duel.user2 ? this.duel.user2.userName : "\u00A0"}
                            avatarImg={this.duel && this.duel.user2 ? this.duel.user2.avatar : ''}/>
                        {::this.getButtonHtml(2)}
                    </div>
                    <div className="description">
                        <p>займите <span
                            className="turquoise">место за {this.props.types[this.props.activeGameType] ? this.props.types[this.props.activeGameType].rate + " " : ""}
                            поинтов</span>
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

DuelChoice.propTypes = {
    placed: React.PropTypes.string,
    duels: PropTypes.array,
    types: PropTypes.object,
    activeGame: PropTypes.number,
    activeGameType: PropTypes.number,
    isLogged: PropTypes.bool,
    showLoginModal: PropTypes.func,
    takePlace: PropTypes.func,
    takeOffPlace: PropTypes.func,
    userId: PropTypes.number,
    userPoints: PropTypes.number,
    showInfoModal: PropTypes.func

};

const mapStateToProps = (state) => {
    return {
        duels: state.games.duels,
        types: state.games.types,
        activeGame: state.settings.activeGame,
        activeGameType: state.settings.activeGameType,
        isLogged: state.user.isLogged,
        userId: state.user.id,
        userPoints: state.user.points
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        showLoginModal: () => dispatch(showLoginModal()),
        showInfoModal: (title, text) => dispatch(showInfoModal(title, text)),
        takePlace: (side, duelId) => dispatch(takePlace(side, duelId)),
        takeOffPlace: (side, duelId) => dispatch(takeOffPlace(side, duelId)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DuelChoice);
// export default DuelChoice;
