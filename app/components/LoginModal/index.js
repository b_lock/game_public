import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Boron from 'boron';

import {hideLoginModal, login, errorFieldLogin} from '../../actions/user';

class LoginModal extends Component {

    componentWillReceiveProps(nextProps) {
        if (nextProps.showLoginModal) {
            this.showModal();
        }
        if (nextProps.isLogged) {
            this.hideModal();
        }
    }

    inputUserName;
    inputUserPassword;

    hideModal() {
        this.refs.modal.hide();
    }

    showModal() {
        this.refs.modal.show();
    }

    handleClose() {
        this.props.hideLoginModal();
        this.props.errorFieldLogin({
            userName: true,
            password: true
        });
    }

    handelSubmit(e) {
        e.preventDefault();
        console.log(this.inputUserName.value);
        if (this.inputUserName.value.length > 1 && this.inputUserPassword.value.length > 5) {
            this.props.login(this.inputUserName.value, this.inputUserPassword.value);
            this.props.errorFieldLogin({
                userName: true,
                password: true
            });
        } else {
            this.props.errorFieldLogin({
                userName: this.inputUserName.value.length > 1,
                password: this.inputUserPassword.value.length > 5
            });
        }
    }

    render() {
        const Modal = Boron.FadeModal;
        const contentStyle = {
            background: false,
        };
        const modalStyle = {
            width: false,
            height: false
        };
        const backdropStyle = {
            backgroundColor: '#171818'
        };
        return (
            <Modal ref="modal" onHide={::this.handleClose} className="login-modal" modalStyle={modalStyle}
                   backdropStyle={backdropStyle}
                   contentStyle={contentStyle}>
                <div className="modal-content content-block">
                    <div className="title">
                        <h1><span>Вход <i className="deliv"/></span></h1>
                    </div>
                    <span className="close-modal" onClick={::this.hideModal}/>
                    <div className="content">
                       <form onSubmit={::this.handelSubmit}>
                        <input className={this.props.usernameField ? '' : 'error'} name="username" type="text"
                               placeholder="Логин" ref={node => {
                            this.inputUserName = node;
                        }}/>
                        <input className={this.props.passwordField ? '' : 'error'} name="password" type="password"
                               placeholder="Пароль" ref={node => {
                            this.inputUserPassword = node;
                        }}/>
                        <div className="button-wrap">
                            <button>Вход</button>
                            <span className="wrapper-top"/>
                            <span className="wrapper-bottom"/>
                        </div>
                        </form>
                    </div>
                </div>
            </Modal>
        );
    }
}

LoginModal.propTypes = {
    showLoginModal: PropTypes.bool,
    hideLoginModal: PropTypes.func,
    errorFieldLogin: PropTypes.func,
    login: PropTypes.func,
    usernameField: PropTypes.bool,
    passwordField: PropTypes.bool,

};

const mapStateToProps = (state) => {
    return {
        showLoginModal: state.modal.showLoginModal,
        usernameField: state.errorFields.userName,
        passwordField: state.errorFields.password,
        isLogged: state.user.isLogged,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideLoginModal: () => dispatch(hideLoginModal()),
        login: (userName, password) => dispatch(login(userName, password)),
        errorFieldLogin: (fields) => dispatch(errorFieldLogin(fields)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LoginModal);

