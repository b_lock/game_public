import React, { Component, PropTypes } from 'react';

// import {config} from '../../config';

// import './style.scss';

class TimerComponent extends Component {

    constructor() {
        super();
    }

    componentWillMount() {
        console.log("componentWillMount", this.props);
        this.seconds = this.props.duelTimer;
        this.setState({
            seconds: this.props.duelTimer ? this.props.duelTimer : 0,
        });
    }


    componentDidMount() {
        this.seconds = this.props.duelTimer;
        // this.startTimer();
    }

    componentWillReceiveProps() {

    }

    componentWillUpdate(/* nextProps */) {
    }

    componentDidUpdate() {
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }


    start() {
        console.log('start timer');
        if (this.timerID) {
            clearTimeout(this.timerID);
        }
        this.setState({
            seconds: this.props.duelTimer,
        });
        this.startTimer();
    }

    startTimer() {
        let sec;
        console.log(this.seconds);
        if (this.replay) {
            this.setState({
                seconds: this.seconds
            });
            sec = this.seconds;
        } else {
            sec = this.state.seconds;
        }
        const startTime = this.seconds;
        const per = 360 / startTime;
        this.setCanvas(-90, 270 - (startTime - sec) * per);
        this.timerID = setInterval(
            () => {
                if (this.state.seconds > 0) {
                    this.setCanvas(-90, (270 - per) - (startTime - this.state.seconds) * per);
                    this.setState({
                        seconds: this.state.seconds - 1
                    });
                } else {
                    this.replay = false;
                    clearInterval(this.timerID);
                    this.props.timerStop();
                }
            },
            1000
        );
    }

    drawDefaultCirc() {
        const canvas = document.getElementById('timer-canvas');
        const ctx = canvas.getContext('2d');
        ctx.beginPath();
        ctx.lineWidth = 2;
        ctx.strokeStyle = "#626262";
        ctx.arc(54, 54, 50, -90 / 180 * Math.PI, 270 / 180 * Math.PI, false);
        ctx.stroke();
    }

    setCanvas(start, end) {
        const canvas = document.getElementById('timer-canvas');
        if (canvas.getContext) {
            const ctx = canvas.getContext('2d');
            ctx.clearRect(0, 0, 108, 108);
            this.drawDefaultCirc();
            ctx.beginPath();
            ctx.lineWidth = 4;
            ctx.strokeStyle = "#b48801";
            ctx.arc(54, 54, 50, start / 180 * Math.PI, end / 180 * Math.PI, false);
            ctx.stroke();

            ctx.beginPath();
            ctx.lineWidth = 8;
            ctx.strokeStyle = "#b48801";
            ctx.lineCap = 'round';
            ctx.arc(54, 54, 50, (end - 1) / 180 * Math.PI, (end + 1) / 180 * Math.PI, false);
            ctx.stroke();
            ctx.beginPath();

            ctx.lineWidth = 6;
            ctx.strokeStyle = "#303030";
            ctx.arc(54, 54, 50, (end) / 180 * Math.PI, (end) / 180 * Math.PI, false);
            ctx.stroke();
            ctx.lineWidth = 3;
            ctx.strokeStyle = "#626262";
            ctx.arc(54, 54, 50, (end) / 180 * Math.PI, (end) / 180 * Math.PI, false);
            ctx.stroke();
        }
    }


    render() {
        return (
            <div className="timer">
                <span className="seconds">{this.state.seconds}</span>
                <canvas id="timer-canvas" width={108} height={108}/>
            </div>
        );
    }
}

TimerComponent.propTypes = {
    duelTimer: PropTypes.number,
    timerStop: PropTypes.func,
    // name: PropTypes.string,
};

export default TimerComponent;


