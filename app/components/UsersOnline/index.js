import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { config } from '../../config';

// import { logout, showLoginModal, showRegistrationModal, showUserSettingsModal } from '../../actions/user';
import { getUsersOnlineInfo } from '../../actions/usersInfo';
// import { Link } from 'react-router';
import './style.scss';

class UsersOnline extends Component {

    componentDidMount() {
        this.props.getUsersOnlineInfo();
    }

    render() {
        return (
            <div className="users-online-info">
                <div className="content-block">
                    <div className="title">
                        <h1><span>Онлайн<i className="deliv"/></span></h1>
                        <div className="logo-background"></div>
                    </div>
                    <table className="table">
                        <thead className="status-message head">
                        <tr>
                            <th className="col">Игрок</th>
                            <th className="col">Игр</th>
                            <th className="col">Побед</th>
                            <th className="col">Win Rate</th>
                            <th className="col">Win Points</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.userInfoOnline ? this.props.userInfoOnline.map(user => {
                            return (
                                <tr className="status-message" key={user.id}>
                                    <td className="username"><span className="userAvatar"><img
                                        srcSet={user.avatar ? config.host + user.avatar : require('../../img/user-avatar.png')}
                                        alt="Avatar"/></span>
                                        {user.userName}
                                    </td>
                                    <td>{user.gamesCount}</td>
                                    <td>{user.gamesWinCount}</td>
                                    <td>{user.gamesCount ? ((100 / user.gamesCount) * user.gamesWinCount).toFixed(2) : 0}%</td>
                                    <td>{user.winPoints ? user.winPoints : 0 }</td>
                                </tr>
                            );
                        }) : ''}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}
UsersOnline.propTypes = {
    getUsersOnlineInfo: PropTypes.func,
    userInfoOnline: PropTypes.array,

};
const mapStateToProps = (state) => {
    return {
        userInfoOnline: state.usersInfo.userInfoOnline,

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUsersOnlineInfo: () => dispatch(getUsersOnlineInfo()),

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UsersOnline);

// export default Header;

// import React from 'react';


// const Header = () =>
//     <div>
//         <h1>Header</h1>
//     </div>;
//
//
// export default Header;

