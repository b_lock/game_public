import React, {Component} from 'react';
// import DuelChoice from '../DuelChoice';
import TournamentComponent from '../TournamentComponent';
import './style.scss';

class TournamentPage extends Component {
    constructor() {
        super();
        this.state = {hideDuelComponent: false};
    }

    startGame() {
        this.hideDuelComponent = true;
        this.setState({hideDuelComponent: !this.state.hideDuelComponent});
    }

    render() {
        return (
            <div className="tournament-page">
                {this.state.hideDuelComponent ? <TournamentComponent/> : <TournamentComponent/>}

            </div>
        );
    }
}

TournamentPage.propTypes = {
    hideDuelComponent: React.PropTypes.bool,
};

export default TournamentPage;
// export default foot;

