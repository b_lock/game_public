import React, { Component } from 'react';

import TournamentComponent from '../TournamentComponent';
import DuelComponent from '../DuelComponent';
// import TimerComponent from '../TimerComponent';


class MainPage extends Component {

    constructor() {
        super();
    }

    render() {
        return (
            <div className="main-page container ">
                <TournamentComponent placed="main-page"/>
                <DuelComponent placed="main-page"/>

            </div>
        );
    }
}

export default MainPage;
