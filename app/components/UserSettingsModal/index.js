import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Boron from 'boron';

import * as helpers from '../../helpers/api';
import { config } from '../../config';

import { hideUserSettingsModal, setUserAvatar } from '../../actions/user';

import './style.scss';

class UserSettingsModal extends Component {

    componentWillReceiveProps(nextProps) {
        if (nextProps.showUserSettingsModal) {
            this.showModal();
        }
    }

    inputUserAvatar;

    hideModal() {
        this.refs.modal.hide();
    }

    showModal() {
        this.refs.modal.show();
    }

    handleClose() {
        this.props.hideUserSettingsModal();
    }

    handelSubmit(e) {
        e.preventDefault();
        console.log(this.inputUserAvatar.files);
        helpers.apiPostUserAvatar(
            `${config.host}api/user-avatar`,
            this.inputUserAvatar.files[0])
            .then(response => {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                    // return dispatch(errorFieldLogin({userName: false, password: false}));
                }
                return response.json();
            })
            .then(json => {
                console.log(json);
                if (json.userAvatar) {
                    this.avatar = config.host + json.userAvatar;
                    this.props.setUserAvatar(json.userAvatar);
                    // this.forceUpdate();
                }
            });
    }

    render() {
        const Modal = Boron.FadeModal;
        const contentStyle = {
            background: false,
        };
        const modalStyle = {
            width: false,
            height: false
        };
        const backdropStyle = {
            backgroundColor: '#171818'
        };
        return (
            <Modal ref="modal" onHide={::this.handleClose} className="user-settings-modal" modalStyle={modalStyle}
                   backdropStyle={backdropStyle}
                   contentStyle={contentStyle}>
                <div className="modal-content content-block">
                    <div className="title">
                        <h1><span>Профиль <i className="deliv"/></span></h1>
                    </div>
                    <span className="close-modal" onClick={::this.hideModal}/>
                    <div className="content">
                        <div className="user-avatar">
                            <form onSubmit={::this.handelSubmit}>
                                <label htmlFor="user-avatar">
                                    <img src={this.props.avatar} alt=""/>
                                </label>
                                <input type="file"
                                       id="user-avatar"
                                       onChange={::this.handelSubmit}
                                       ref={node => {
                                           this.inputUserAvatar = node;
                                       }}/>
                            </form>
                        </div>
                        <div className="user-info-top">
                            <div>
                                <span className="username">{this.props.userInfo.userName}</span> / <span className="user-points">{this.props.user.points}</span> <span className="text-points">points</span>
                            </div>
                            <div className="user-games-info">
                                <div className="game-count">
                                    <div className="user-games-info-title">СЫГРАНО ИГР</div>
                                    <div className="text">{this.props.userInfo.gamesCount}</div>
                                </div>
                                <div className="game-win">
                                    <div className="user-games-info-title">ИЗ НИХ ПОБЕД</div>
                                    <div className="text">{this.props.userInfo.gamesWinCount}</div>
                                </div>
                                <div className="win-points">
                                    <div className="user-games-info-title">Win points</div>
                                    <div className="text">{this.props.userInfo.winPoints}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Modal>
        );
    }
}

UserSettingsModal.propTypes = {
    showUserSettingsModal: PropTypes.bool,
    hideUserSettingsModal: PropTypes.func,
    avatar: PropTypes.string,
    setUserAvatar: PropTypes.func,
    userInfo: PropTypes.object,
    user: PropTypes.object

};

const mapStateToProps = (state) => {
    return {
        showUserSettingsModal: state.modal.showUserSettingsModal,
        avatar: state.user.avatar,
        userInfo: state.user.userInfo,
        user: state.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideUserSettingsModal: () => dispatch(hideUserSettingsModal()),
        setUserAvatar: (avatar) => dispatch(setUserAvatar(avatar)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserSettingsModal);

