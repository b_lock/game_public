import React, {Component} from 'react';
// import {connect} from 'react-redux';
// import {Link} from 'react-router';
// import {config} from '../../config';
// import {toggleTournamentPlace} from '../../actions/games';
// import {showLoginModal} from '../../actions/user';
// import './style.scss';

import * as constImg from '../../helpers/const';

class HexagonComponent extends Component {

    componentDidMount() {
        this.refs.hexImg.onmousemove = () => {
            if (this.refs.hexImg.dataset.empty === "0") {
                if (this.refs.hexImg.src !== this.props.playersInfo[this.props.players[this.props.keyComponent].id].avatarHexHover) {
                    this.refs.hexImg.src = this.props.playersInfo[this.props.players[this.props.keyComponent].id].avatarHexHover;
                }
            } else if (this.refs.hexImg.dataset.empty === "1") {
                if (this.refs.hexImg.src !== constImg.EMPTY_AVATAR_HOVER) {
                    this.refs.hexImg.src = constImg.EMPTY_AVATAR_HOVER;
                }
            } else {
                if (this.refs.hexImg.src !== constImg.EMPTY_AVATAR) {
                    this.refs.hexImg.src = constImg.EMPTY_AVATAR;
                }
            }
        };
        this.refs.hexImg.onmouseleave = () => {
            if (this.refs.hexImg.dataset.empty === "0" && this.refs.hexImg.src !== this.props.playersInfo[this.props.players[this.props.keyComponent].id].avatarHex) {
                this.refs.hexImg.src = this.props.playersInfo[this.props.players[this.props.keyComponent].id].avatarHex;
            } else if (this.refs.hexImg.dataset.empty === "1" && this.refs.hexImg.src !== constImg.EMPTY_AVATAR) {
                this.refs.hexImg.src = constImg.EMPTY_AVATAR;
            } else if (this.refs.hexImg.src !== constImg.EMPTY_AVATAR_HOVER) {
                this.refs.hexImg.src = constImg.EMPTY_AVATAR_HOVER;
            }
        };
        // this.refs.hexImg.onclick(() => {
        //     this.props.onClickEvent(this.props.keyComponent);
        // });
    }

    // componentWillReceiveProps(nextProps) {
    //     // console.log(nextProps.keyComponent, nextProps);
    // }

    render() {
        if (this.props.players && this.props.players[this.props.keyComponent] && this.props.playersInfo && this.props.playersInfo[this.props.players[this.props.keyComponent].id]) {
            return (
                <img src={this.props.playersInfo[this.props.players[this.props.keyComponent].id].avatarHex }
                     ref="hexImg"
                     data-empty={0}
                     data-place={this.props.keyComponent}
                     className="hexagon-component"
                     onClick={this.props.onClick}
                     alt=""/>
            );
        }
        return (
            <img data-empty={1}
                 data-place={this.props.keyComponent}
                 src={constImg.EMPTY_AVATAR} className="hexagon-component"
                 ref="hexImg"
                 onClick={this.props.onClick}
                 alt=""/>
        );
    }
}

HexagonComponent.propTypes = {
    // players: React.PropTypes.object,
    players: React.PropTypes.oneOfType([
        React.PropTypes.object,
        React.PropTypes.array
    ]),
    playersInfo: React.PropTypes.object,
    keyComponent: React.PropTypes.number,
    onClick: React.PropTypes.func,
};

export default HexagonComponent;
