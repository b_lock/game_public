import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

import {config} from '../../config';

// import { logout, showLoginModal, showRegistrationModal, showUserSettingsModal } from '../../actions/user';
import {getGamesHistory} from '../../actions/usersInfo';
// import { Link } from 'react-router';
// import './style.scss';

class HistoryTournamentComponent extends Component {

    componentDidMount() {
        this.props.getGamesHistory("tournament");
    }

    render() {
        return (
            <table className="table">
                <thead className="status-message head">
                <tr>
                    <th className="col">#Турнира</th>
                    <th className="col">Банк</th>
                    <th className="col">Победитель</th>
                </tr>
                </thead>
                <tbody>
                {this.props.tournamentHistory ? this.props.tournamentHistory.map((tournament, key) => {
                        return (
                            <tr className="status-message" key={key}>
                                <td className="id-game">#{tournament.id}</td>
                                <td>{tournament.bank}</td>
                                <td className="username">
                                    {tournament.winner && tournament.winner.id ? (
                                            <span className="userAvatar">
                                        <img
                                            srcSet={ tournament.winner.avatar ? config.host + tournament.winner.avatar : require('../../img/user-avatar.png')}
                                            alt=""/>
                                    </span>
                                        ) : ''}
                                    {tournament.winner ? tournament.winner.userName : ""}
                                </td>
                            </tr>
                        );
                    }) : ''}
                </tbody>
            </table>
        );
    }
}
HistoryTournamentComponent.propTypes = {
    getGamesHistory: PropTypes.func,
    tournamentHistory: PropTypes.array

};
const mapStateToProps = (state) => {
    return {
        tournamentHistory: state.usersInfo.gamesHistory,

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getGamesHistory: (req) => dispatch(getGamesHistory(req)),

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HistoryTournamentComponent);

// export default Header;

// import React from 'react';


// const Header = () =>
//     <div>
//         <h1>Header</h1>
//     </div>;
//
//
// export default Header;

