import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Boron from 'boron';

import { hideInfoModal } from '../../actions/user';

class InfoModal extends Component {

    componentWillReceiveProps(nextProps) {
        if (nextProps.showInfoModal) {
            this.showModal();
        }
    }

    inputUserName;
    inputUserPassword;

    hideModal() {
        this.refs.modal.hide();
    }

    showModal() {
        this.refs.modal.show();
    }

    handleClose() {
        this.props.hideInfoModal();
    }


    render() {
        const Modal = Boron.FadeModal;
        const contentStyle = {
            background: false,
        };
        const modalStyle = {
            width: false,
            height: false
        };
        const backdropStyle = {
            backgroundColor: '#171818'
        };
        return (
            <Modal ref="modal" onHide={::this.handleClose} className="Info-modal" modalStyle={modalStyle}
                   backdropStyle={backdropStyle}
                   contentStyle={contentStyle}>
                <div className="modal-content content-block">
                    <div className="title">
                        <h1><span>{this.props.infoModalProp.title} <i className="deliv"/></span></h1>
                    </div>
                    <span className="close-modal" onClick={::this.hideModal}/>
                    <div className="content" dangerouslySetInnerHTML={{__html: this.props.infoModalProp.infoText}}>
                    </div>
                </div>
            </Modal>
        );
    }
}

InfoModal.propTypes = {
    showInfoModal: PropTypes.bool,
    hideInfoModal: PropTypes.func,
    infoModalProp: PropTypes.object

};

const mapStateToProps = (state) => {
    return {
        showInfoModal: state.modal.showInfoModal,
        infoModalProp: state.modal.infoModalProp,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideInfoModal: () => dispatch(hideInfoModal()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(InfoModal);

