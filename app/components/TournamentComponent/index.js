import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { config } from '../../config';
import { toggleTournamentPlace } from '../../actions/games';
import { showLoginModal, showInfoModal } from '../../actions/user';
import HexagonComponent from '../HexagonComponent';
import './style.scss';

class TournamentComponent extends Component {
    constructor() {
        super();
        this.players = {};
    }

    componentWillMount() {
        this.mounted = false;
        this.countPlayers = this.props.tournament.players && this.props.tournament.players.length >= 15 ? this.props.tournament.players.length : 15;

        if (this.props.types) {
            const keys = Object.keys(this.props.types);
            const findKey = keys.find(key => {
                return +this.props.types[key].tournament === 1;
            });
            if (findKey) {
                this.rate = this.props.types[findKey].rate;
            }
        }

        // this.rate = this.props.types.forEch;
        // {this.props.types[this.props.activeGameType] ? this.props.types[this.props.activeGameType].rate + " " : ""}
    }

    componentDidMount() {
        // console.log('componentDidMount');
        this.renderHexagon();
        this.mounted = true;
        window.addEventListener('resize', this.resizeEvent.bind(this));
    }

    componentWillUpdate() {
        this.mounted = false;
        this.countPlayers = this.props.tournament.players && this.props.tournament.players.length >= 15 ? this.props.tournament.players.length : 15;

        if (this.props.types) {
            const keys = Object.keys(this.props.types);
            const findKey = keys.find(key => {
                return +this.props.types[key].tournament === 1;
            });
            if (findKey) {
                this.rate = this.props.types[findKey].rate;
            }
        }
    }

    componentDidUpdate() {
        // console.log('componentWillUpdate');
        this.renderHexagon();
        this.mounted = true;
        // console.log("status: ", this.props.tournament.status);
        if (this.props.tournament.status === 0) {
            this.refs.timer.innerText = "";
        }
        if (this.props.tournament.status === 1) {
            if (!this.interval) {
                this.startTimer();
            }
        }
        if (this.props.tournament.status === 2) {
            this.refs.timer.innerText = "Идет турнир";
        }
    }

    componentWillUnmount() {
        this.mounted = false;
        window.removeEventListener("resize", this.resizeEvent);
    }

    startTimer() {
        //  console.log('start timer');
        const el = this.refs.timer;
        this.timer = 10;
        this.interval = setInterval(() => {
            el.innerText = "До начала турнира осталосю " + this.timer + "сек.";
            this.timer = this.timer - 1;
            //  console.log(this.timer);
            if (this.timer < 0) {
                clearInterval(this.interval);
                this.interval = null;
            }
        }, 1000);
    }

    renderHexagon() {
        const players = this.props.tournament.players || [];
        //  console.log('renderHexagon');
        const that = this;
        // img.src = "http://i1.sndcdn.com/avatars-000070344618-tv261u-large.jpg?30a2558";

        function fillImage(cxt, player) {
            //  console.log(that);
            const img = new Image();
            img.setAttribute('crossOrigin', 'anonymous');
            img.src = config.host + player.avatar;
            // console.log(img.width);
            // console.log(img.height);
            //  console.log('fillImage', player);

            function rendImg() {
                const x = document.createElement("CANVAS");
                let pr;
                let sx;
                let sy;
                let imgWidth;
                let imgHeight;
                if (img.width > img.height) {
                    pr = img.width / img.height;
                    sx = (img.width - img.height) / 2;
                    sy = 0;
                    imgHeight = img.height;
                    imgWidth = img.width / pr;
                } else {
                    pr = img.height / img.width;
                    sx = 0;
                    sy = (img.height - img.width) / 2;
                    imgWidth = img.width;
                    imgHeight = img.height / pr;
                }
                x.id = "canv";
                x.width = 110;
                x.height = 98;
                const ctx = x.getContext("2d");
                //  console.log(sx, sy, imgWidth, imgHeight);
                // ctx.drawImage(img, sx, sy, img.width / pr, img.height, 5, 5, 110, 98);
                ctx.drawImage(img, sx, sy, imgWidth, imgHeight, 5, 5, 110, 98);
                const dataURL = x.toDataURL("image/png");
                const newImg = new Image();
                // console.log(dataURL);
                newImg.src = dataURL;
                const pattern = cxt.createPattern(newImg, "no-repeat");
                cxt.fillStyle = pattern;
                // cxt.drawImage(img, 10, 10, 100, 88);
                // cxt.fillRect(0, 0, 100, 88);
                cxt.fill();
            }

            if (that.players[player.id] && that.players[player.id].avatar === player.avatar) {
                rendImg();
            } else {
                img.onload = () => {
                    that.players[player.id] = {
                        avatar: player.avatar
                    };
                    rendImg();
                };
            }
        }

        for (let j = 1; j <= this.countPlayers; j++) {
            const canvas = document.getElementById('cell-0' + j);
            if (canvas) {
                // console.log(canvas);
                const cxt = canvas.getContext('2d');
                cxt.clearRect(0, 0, 110, 110);

                /* c2.fillStyle = '#f00';
                 c2.beginPath();
                 c2.moveTo(0, 0);
                 c2.lineTo(100, 50);
                 c2.lineTo(50, 100);
                 c2.lineTo(0, 90);
                 c2.closePath();
                 c2.fill(); */
// hexagon
                const numberOfSides = 6;
                const size = 50;
                const Xcenter = 55;
                const Ycenter = 49;

                cxt.beginPath();
                cxt.strokeStyle = "#03585d";
                cxt.lineWidth = 1;
                cxt.moveTo(Xcenter + size * Math.cos(0), Ycenter + size * Math.sin(0));

                for (let i = 1; i <= numberOfSides; i += 1) {
                    cxt.lineTo(Xcenter + size * Math.cos(i * 2 * Math.PI / numberOfSides), Ycenter + size * Math.sin(i * 2 * Math.PI / numberOfSides));
                }

                cxt.stroke();
                if (players[j]) {
                    fillImage(cxt, players[j]);
                }
                let shadow = false;

                canvas.onclick = function () {
                    this.tooglePlace(j);
                }.bind(this);

                canvas.onmousemove = (e) => {
                    // console.log(e);
                    setTimeout(() => {
                        const rect = e.target.getBoundingClientRect();
                        const x = e.clientX - rect.left;
                        const y = e.clientY - rect.top;

                        /* i = 0, r */

                        const check = cxt.isPointInPath(x, y);
                        // if (shadow === undefined) {
                        //     let shadow = false;
                        // }
                        if (check && !shadow) {
                            cxt.shadowColor = '#3a9ca1';
                            cxt.shadowBlur = 15;

                            cxt.shadowOffsetX = 0;
                            cxt.shadowOffsetY = 0;
                            cxt.stroke();
                            cxt.stroke();
                            cxt.stroke();
                            // cxt.fill();
                            shadow = true;
                            e.target.classList.add('hover');
                            if (players[j]) {
                                fillImage(cxt, players[j]);
                            }
                        } else if (!check) {
                            cxt.clearRect(0, 0, 110, 110);
                            cxt.shadowBlur = 0;

                            cxt.shadowOffsetX = 0;
                            cxt.shadowOffsetY = 0;
                            cxt.stroke();
                            shadow = false;
                            if (players[j]) {
                                fillImage(cxt, players[j]);
                            }
                            e.target.classList.remove('hover');
                        }
                    }, 100);
                };
                canvas.onmouseleave = (e) => {
                    setTimeout(() => {
                        cxt.clearRect(0, 0, 110, 110);
                        cxt.shadowBlur = 0;
                        cxt.shadowOffsetX = 0;
                        cxt.shadowOffsetY = 0;
                        cxt.stroke();
                        shadow = false;
                        if (players[j]) {
                            fillImage(cxt, players[j]);
                        }
                        e.target.classList.remove('hover');
                        // console.log(e);
                        // console.log('leave');

                        /* const rect = this.getBoundingClientRect();
                         const x = e.clientX - rect.left;
                         const y = e.clientY - rect.top;

                         /!* i = 0, r *!/


                         const check = cxt.isPointInPath(x, y);
                         // if (shadow === undefined) {
                         //     let shadow = false;
                         // }*/
                        // if (check && !shadow) {
                        // cxt.shadowColor = '#328287';
                        // cxt.shadowBlur = 0;
                        //
                        // cxt.shadowOffsetX = 0;
                        // cxt.shadowOffsetY = 0;
                        // cxt.stroke();
                        // // console.log(check);
                        // shadow = true;

                        // }
                    }, 100);
                };
            }
        }
    }

    getConteinerHexagon() {
        //  console.log('getConteinerHexagon');
        // let col = 5;
        // console.log(this.props.tournament);
        const rootEls = [];
        let els;
        if (this.props.tournament.players && this.props.tournament.players.length > 15) {
            this.countPlayers = this.props.tournament.players.length;
        }
        const width = this.refs.hexagonContainer ? this.refs.hexagonContainer.clientWidth : (110 * 5);

        this.col = parseInt(width / 110, 10) > 5 ? 5 : parseInt(width / 110, 10);
        //  console.log('this.col', this.col);
        if (this.col > 0) {
            const row = Math.ceil(this.countPlayers / this.col);
            for (let i = 0; i < row; i++) {
                els = [];
                for (let j = 1; j <= this.col && (this.countPlayers >= ((i * this.col) + j)); j++) {
                    // const el = (
                    //     <canvas key={(i * this.col) + j} id={"cell-0" + ((i * this.col) + j)} width="110" height="98"/>
                    // );
                    const el = (
                        <HexagonComponent key={(i * this.col) + j} keyComponent={(i * this.col) + j}
                                          onClick={::this.tooglePlace}
                                          players={this.props.tournament.players }
                                          playersInfo={this.props.playersInfo}/>
                    );
                    els.push(el);
                }
                const rel = (
                    <div className="row-canvas" key={i.toString()}>
                        {els}
                    </div>
                );
                rootEls.push(rel);
            }
        }

        // console.log(rootEls);
        return rootEls;
    }

    resizeEvent() {
        const width = this.refs.hexagonContainer ? this.refs.hexagonContainer.clientWidth : (110 * 5);
        const col = parseInt(width / 110, 10) > 5 ? 5 : parseInt(width / 110, 10);
        if (col !== this.col) {
            //  console.log('resize', parseInt(width / 110, 10));
            if (this.mounted) {
                this.forceUpdate();
            }
        }
        // console.log(width);
        //
    }

    tooglePlace(e) {
        //  console.log(e.target);
        if (this.props.isLogged) {
            if (this.rate <= this.props.userPoints) {
                if (!this.disabledBut || this.disabledBut < (Date.now())) {
                    this.disabledBut = Date.now() + 1000;
                    this.props.toggleTournamentPlace(e.target.dataset.place);
                }
            } else {
                this.props.showInfoModal("Не хватает поинтов", "на вашем счету нет поинтов");
            }
        } else {
            // console.log('not login');
            this.props.showLoginModal();
        }
    }

    render() {
        return (
            <div className="container-tournament">
                <div className="content-block">
                    <div className="title">
                        <h1><span>Турнир<i className="deliv"/></span></h1>
                        <div className="logo-background"></div>
                    </div>
                    <div className="content">
                        <div className="head-block">
                            <a href="#" className="question"/>
                            <a href="#" className="security"/>
                            <h2 className="timer hide" ref="timer"/>
                            {this.props.placed === 'main-page' ? <Link to="/tournament" className="expand"/> :
                                <Link to="/" className="expand"/>}
                        </div>
                        <div className="hexagon-container" ref="hexagonContainer">
                            {this.getConteinerHexagon()}
                        </div>
                        <div className="bottom-container">
                            <div className="selected">
                                <div className="place">
                                    <span className="count">1 </span>
                                    <span className="text">место = </span>
                                </div>
                                <div className="points">
                                    <span className="count">{this.rate}</span>
                                    <span className="text"> поинтов</span>
                                </div>
                            </div>
                            {/* <div className="button-wrap"> */}
                                {/* <button>вступить в игру</button> */}
                                {/* <span className="wrapper-top"/> */}
                                {/* <span className="wrapper-bottom"/> */}
                            {/* </div> */}
                            <div className="players">
                                <span className="text">Игроков </span>
                                <span
                                    className="count">{this.props.tournament.players ? Object.keys(this.props.tournament.players).length : 0}</span>
                                <span className="text"> из</span>
                                <span className="count">{this.countPlayers}</span>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
//

TournamentComponent.propTypes = {
    placed: PropTypes.string,
    tournament: PropTypes.object,
    toggleTournamentPlace: PropTypes.func,
    isLogged: PropTypes.bool,
    showLoginModal: PropTypes.func,
    userId: PropTypes.number,
    userPoints: PropTypes.number,
    playersInfo: PropTypes.object,
    types: PropTypes.object,
    showInfoModal: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        tournament: state.games.tournament,
        isLogged: state.user.isLogged,
        userId: state.user.id,
        userPoints: state.user.points,
        playersInfo: state.playersInfo,
        types: state.games.types,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleTournamentPlace: (place) => dispatch(toggleTournamentPlace(place)),
        showLoginModal: () => dispatch(showLoginModal()),
        showInfoModal: (title, text) => dispatch(showInfoModal(title, text)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TournamentComponent);


//
// export default connect(
// )(TournamentMainPage);
// export default TournamentComponent;
