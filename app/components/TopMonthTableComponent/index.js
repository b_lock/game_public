import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

import {config} from '../../config';

// import { logout, showLoginModal, showRegistrationModal, showUserSettingsModal } from '../../actions/user';
import {getUsersTop} from '../../actions/usersInfo';
// import { Link } from 'react-router';
// import './style.scss';

class TopMonthTableComponent extends Component {

    componentDidMount() {
        this.props.getUsersTop("month");
    }

    render() {
        return (
            <table className="table">
                <thead className="status-message head">
                <tr>
                    <th>Место</th>
                    <th className="col">Игрок</th>
                    <th className="col">Игр</th>
                    <th className="col">Побед</th>
                    <th className="col">Win Rate</th>
                    <th className="col">Win Points</th>
                </tr>
                </thead>
                <tbody>
                {this.props.usersTop ? this.props.usersTop.map((user, key) => {
                        return (
                            <tr className="status-message" key={key}>
                                <td className={"rate-" + (key + 1)}>{key + 1}</td>
                                <td className="username"><span className="userAvatar"><img
                                    srcSet={user.avatar ? config.host + user.avatar : require('../../img/user-avatar.png')}
                                    alt=""/></span>
                                    {user.userName}
                                </td>
                                <td>{user.gamesCount}</td>
                                <td>{user.gamesWinCount}</td>
                                <td>{((100 / user.gamesCount) * user.gamesWinCount).toFixed(2)}%</td>
                                <td>{user.winPoints}</td>
                            </tr>
                        );
                    }) : ''}
                </tbody>
            </table>
        );
    }
}

TopMonthTableComponent.propTypes = {
    getUsersTop: PropTypes.func,
    usersTop: PropTypes.array

};
const mapStateToProps = (state) => {
    return {
        usersTop: state.usersInfo.usersTop,

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUsersTop: (req) => dispatch(getUsersTop(req)),

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TopMonthTableComponent);

// export default Header;

// import React from 'react';


// const Header = () =>
//     <div>
//         <h1>Header</h1>
//     </div>;
//
//
// export default Header;

