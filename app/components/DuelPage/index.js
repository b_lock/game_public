import React, { Component } from 'react';
import { connect } from 'react-redux';
import { config } from '../../config';
import DuelComponent from '../DuelComponent';
import DuelProcComponent from '../DuelProcComponent';
import './style.scss';

class DuelPage extends Component {
    constructor() {
        super();
        this.state = {hideDuelComponent: false};
        this.logs = null;
    }


    componentWillMount() {
        if (this.props.logs && this.props.activeGameType) {
            this.logs = this.props.logs[this.props.activeGameType];
        }
    }

    componentWillUpdate(nextProps) {
        if (nextProps.logs && nextProps.activeGameType) {
            this.logs = nextProps.logs[nextProps.activeGameType];
        }
    }


    startGame() {
        this.hideDuelComponent = true;
        this.setState({hideDuelComponent: !this.state.hideDuelComponent});
    }

    render() {
        return (
            <div className="duel-page">
                {this.state.hideDuelComponent ? <DuelProcComponent/> : <DuelComponent/>}
                <div className="status-messages">
                    {this.logs ? this.logs.map((log => {
                        return (
                            <div key={log.id} className="status-messages">
                                <div className="status-message">

                                    <div className="user-avatar">
                                        <img src={config.host + log.user1.avatar} alt="avatar"
                                             className="avatar"/>
                                    </div>
                                    <div className="user-name">{log.user1.username}</div>
                                    <div className="message">{
                                        log.user1.user_choice === 'fail' ? <span>Не сделал выбор</span> : " Выбрал "}
                                        <span> {config.translate[log.user1.user_choice]} </span>
                                    </div>

                                </div>
                                <div className="status-message">

                                    <div className="user-avatar">
                                        <img src={config.host + log.user2.avatar} alt="avatar"
                                             className="avatar"/>
                                    </div>
                                    <div className="user-name">{log.user2.username}</div>
                                    <div className="message">{
                                        log.user1.user_choice === 'fail' ? <span>Не сделал выбор</span> : " Выбрал "}
                                        <span> {config.translate[log.user2.user_choice]} </span>
                                    </div>

                                </div>

                                {log.winner === -1 ? (
                                    <div className="status-message">
                                        <div className="message"><span className="winnerText">Никто не выиграл</span>
                                        </div>
                                    </div>
                                ) : ( <div className="status-message">
                                        <div className="user-avatar">
                                            <img
                                                src={config.host + (log.user1.id === log.winner ? log.user1.avatar : log.user2.avatar)}
                                                alt="avatar"
                                                className="avatar"/>
                                        </div>
                                        < div className="user-name">{(log.user1.id === log.winner ? log.user1.username : log.user2.username)}</div>
                                        <div className="message"><span className="winnerText"> Выиграл</span></div>
                                    </div> )
                                }
                            </div >
                        );
                    })) : ""}
                </div>
            </div>
        );
    }
}
DuelPage.propTypes = {
    hideDuelComponent: React.PropTypes.bool,
    activeGameType: React.PropTypes.number,
    logs: React.PropTypes.object
};

const mapStateToProps = (state) => {
    return {
        logs: state.games.logs,
        activeGameType: state.settings.activeGameType,
        // user: state.user,
        // activeDuels: state.activeDuels,
    };
};

const mapDispatchToProps = (/* dispatch */) => {
    return {
        // setUserChoice: (userChoice, duelId) => dispatch(setUserChoice(userChoice, duelId)),
        // setDuelReplyReset: (duelId) => dispatch(setDuelReplyReset(duelId)),
        // newGame: (duelId) => dispatch(newGame(duelId))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DuelPage);

// export default DuelPage;
// export default foot;

