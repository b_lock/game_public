import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

// import {config} from '../../config';
//
// import { logout, showLoginModal, showRegistrationModal, showUserSettingsModal } from '../../actions/user';
// import {getUsersOnlineInfo} from '../../actions/usersInfo';
// import { Link } from 'react-router';
import './style.scss';
class TopTableComponent extends Component {
    render() {
        return (
            <div>
                <h1 className="title-table"><span>Топ игроков</span></h1>
                <div className="users-top-info">
                    <div className="content-block">
                        <div className="title">
                            <h1>
                           <span>
                               <Link to="/top/all" activeClassName="active">Все время</Link>
                               <Link to="/top/day" activeClassName="active">24 часа</Link>
                               <Link to="/top/week" activeClassName="active">Неделя</Link>
                               <Link to="/top/month" activeClassName="active">Месяц</Link>
                               <i className="deliv"/>
                            </span>
                            </h1>
                        </div>
                        {this.props.children }
                    </div>
                </div>
            </div>
        );
    }
}
TopTableComponent.propTypes = {
    // getUsersOnlineInfo: PropTypes.func,
    children: PropTypes.object

};
const mapStateToProps = (state) => {
    return {
        userInfoOnline: state.usersInfo.userInfoOnline,
    };
};

const mapDispatchToProps = (/* dispatch */) => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TopTableComponent);

// export default Header;

// import React from 'react';


// const Header = () =>
//     <div>
//         <h1>Header</h1>
//     </div>;
//
//
// export default Header;

