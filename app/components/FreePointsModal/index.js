import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import Boron from 'boron';
import './style.scss';

import { hideFreePointsModal, freePoints, errorFieldFreePoints, showInfoModal } from '../../actions/user';

class FreePointsModal extends Component {

    componentWillReceiveProps(nextProps) {
        if (nextProps.showFreePointsModal) {
            this.showModal();
        }
    }

    inputUserName;
    inputUserPassword;

    hideModal() {
        this.refs.modal.hide();
    }

    showModal() {
        this.refs.modal.show();
    }

    handleClose() {
        this.props.hideFreePointsModal();
    }

    handelSubmit(e) {
        e.preventDefault();
        if (this.codeField.value.length > 1) {
            this.props.freePoints(this.codeField.value).then(json => {
                if (json.points) {
                    this.hideModal();
                    console.log(json.points);
                    this.props.showInfoModal("Бесплатные поинты", `Поздравляем вам зачислено <span class="gold">${json.points}</span> поинта`);
                }
            });
            this.props.errorFieldFreePoints(true);
        } else {
            this.props.errorFieldFreePoints(this.codeField.value.length > 1);
        }
    }


    render() {
        const Modal = Boron.FadeModal;
        const contentStyle = {
            background: false,
        };
        const modalStyle = {
            width: false,
            height: false
        };
        const backdropStyle = {
            backgroundColor: '#171818'
        };
        return (
            <Modal ref="modal" onHide={::this.handleClose} className="free-points-modal" modalStyle={modalStyle}
                   backdropStyle={backdropStyle}
                   contentStyle={contentStyle}>
                <div className="modal-content content-block">
                    <div className="title">
                        <h1>
                            <span>
                                <b className="twoLines">Бесплатные <br/> поинты</b>
                                <i className="deliv"/>
                            </span>
                        </h1>
                    </div>
                    <span className="close-modal" onClick={::this.hideModal}/>
                    <div className="content">
                        <div className="logos">
                            <img src={require('../../img/logo2.png')} alt="Logo"/>
                            <img src={require('../../img/logo2.png')} alt="Logo"/>
                            <img src={require('../../img/logo2.png')} alt="Logo"/>
                        </div>
                        <p>Введите код и получите БЕСПЛАТНЫЕ ПОИНТЫ. <br/>
                            Вы можете использовать код только 1 раз</p>
                        <form onSubmit={::this.handelSubmit}>
                            <input className={this.props.freePointsField ? '' : 'error'} name="points" type="text"
                                   placeholder="Введите бонус код и получите поинты" ref={node => {
                                this.codeField = node;
                            }}/>

                            <br/>
                            <div className="button-wrap">
                                <button>активировать код</button>
                                <span className="wrapper-top"/>
                                <span className="wrapper-bottom"/>
                            </div>
                        </form>
                    </div>
                </div>
            </Modal>
        );
    }
}

FreePointsModal.propTypes = {
    showFreePointsModal: PropTypes.bool,
    freePointsField: PropTypes.bool,
    hideFreePointsModal: PropTypes.func,
    errorFieldFreePoints: PropTypes.func,
    freePoints: PropTypes.func,
    showInfoModal: PropTypes.func
};

const mapStateToProps = (state) => {
    return {
        showFreePointsModal: state.modal.showFreePointsModal,
        freePointsField: state.errorFields.freePoints,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideFreePointsModal: () => dispatch(hideFreePointsModal()),
        freePoints: (code) => dispatch(freePoints(code)),
        errorFieldFreePoints: (fields) => dispatch(errorFieldFreePoints(fields)),
        showInfoModal: (title, text) => dispatch(showInfoModal(title, text)),

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FreePointsModal);

