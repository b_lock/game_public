/**
 * Created by Vlad on 22.02.2017.
 */
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { getFaq } from '../../actions';

// import {config} from '../../config';
//
// import { logout, showLoginModal, showRegistrationModal, showUserSettingsModal } from '../../actions/user';
// import {getUsersOnlineInfo} from '../../actions/usersInfo';
// import { Link } from 'react-router';
import './style.scss';
class FaqComponent extends Component {

    componentDidMount() {
        this.props.getFaq();
    }

    render() {
        function getClassRoot(faq) {
            if (faq.parent_id) {
                return "";
            }
            return "root";
        }

        function openFaq(event) {
            event.currentTarget.parentNode.parentNode.classList.toggle('open');
            event.stopPropagation();
        }

        function rendFaq(faq, fKey) {
            return (
                <ul>
                    {Object.keys(faq).map(function (key, index) {
                        return (
                            <li key={key}
                                className={faq[key].child ? getClassRoot(faq[key]) : getClassRoot(faq[key]) + " last "}>
                                <p className="faq-title">
                                    <span
                                        onClick={openFaq}>{fKey ? fKey + "." + (+index + 1) : (+index + 1) + ". "} {faq[key].title}</span>
                                </p>
                                {faq[key].description ? (<p className="text">{faq[key].description}</p>) : ""}
                                {faq[key].child ? rendFaq(faq[key].child, (+index + 1)) : ""}
                            </li>
                        );
                    })}
                </ul>
            );
        }

        return (
            <div>
                <h1 className="title-table"><span>KNB GAME</span></h1>
                <div className="faq-info">
                    <div className={this.props.spinner ? "spinner content-block" : "content-block"}>
                        <div className="title">
                            <h1>
                           <span>
                               FAQ
                               <i className="deliv"/>
                            </span>
                            </h1>
                        </div>
                        <p className="desc">
                            Здесь вы найдете ответы на самые частозадаваемые вопросы.
                            Если вы не нашли ответ на свой вопрос, <a href={"mailto:" + this.props.supportEmail}><span
                            className="gold">напишите нам</span></a>
                        </p>
                        <div className="faq-content">
                            {rendFaq(this.props.faq)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
FaqComponent.propTypes = {
    getFaq: PropTypes.func,
    faq: PropTypes.object,
    supportEmail: PropTypes.string,
    spinner: PropTypes.bool
};
const mapStateToProps = (state) => {
    return {
        faq: state.main.faq,
        supportEmail: state.main.init.support_email,
        spinner: state.main.spinner
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFaq: () => dispatch(getFaq()),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FaqComponent);

// export default Header;

// import React from 'react';


// const Header = () =>
//     <div>
//         <h1>Header</h1>
//     </div>;
//
//
// export default Header;

