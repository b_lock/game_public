import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import { filterTable, testApi } from '../../actions';
import {
    logout,
    showLoginModal,
    showRegistrationModal,
    showUserSettingsModal,
    showFreePointsModal
} from '../../actions/user';
import LoginModal from '../LoginModal';
import UserSettingsModal from '../UserSettingsModal';
import RegistrationModal from '../RegistrationModal';
import InfoModal from '../InfoModal';
import FreePointsModal from '../FreePointsModal';
import { Link } from 'react-router';
import './style.scss';

class Header extends Component {


    componentDidMount() {
        console.log('component mounted');
        // console.log($);
    }

    showLoginModal(e) {
        e.preventDefault();
        this.props.showLoginModal();
    }

    showUserSettingsModal(e) {
        e.preventDefault();
        this.props.showUserSettingsModal();
    }

    showRegistrationModal(e) {
        e.preventDefault();
        this.props.showRegistrationModal();
    }

    showFreePointsModal(e) {
         e.preventDefault();
        if (this.props.user.isLogged) {
            this.props.showFreePointsModal();
        } else {
            this.props.showLoginModal();
        }
    }

    resetFilter(e) {
        e.preventDefault();
        this.props.onResetFilter();
    }

    addFilter(e, filter) {
        e.preventDefault();
        this.props.onFilter(filter);
    }

    handelTestApi(e) {
        e.preventDefault();
        this.props.testApi();
    }


    handelLogout(e) {
        e.preventDefault();
        this.props.logout();
    }

    render() {
        if (this.props.user.isLogged) {
            this.logButt = (
                <div className="reg">
                    <a href="" onClick={(event) => this.showUserSettingsModal(event)}>{this.props.user.username} </a>
                    <a href="" onClick={(event) => this.handelLogout(event)}> Выход</a>
                </div>
            );
        } else {
            this.logButt = (
                <div className="reg">
                    <a href="" onClick={(event) => this.showRegistrationModal(event)}>Регистрация</a>
                    <a href="" onClick={(event) => this.showLoginModal(event)}>Вход</a>
                </div>
            );
        }

        return (
            <div className="header">
                <div className="container">
                    <div className="logo">
                        <Link to="/" onlyActiveOnIndex>
                            <img src={require('../../img/logo.png')} alt="Logo"/>
                            <span className="logo-text">KNB GAME</span>
                        </Link>
                    </div>
                    <div className="menu">
                        <Link to="/users-online" activeClassName="active" className="online"><span>онлайн <span
                            className="gold">{this.props.usersOnline }</span> </span></Link>
                        <Link to="/history" activeClassName="active"><span>история</span></Link>
                        <Link to="/top" activeClassName="active"><span>топ</span></Link>
                        <Link to="/faq" activeClassName="active"><span>faq</span></Link>
                        <a href="" onClick={(event) => this.showFreePointsModal(event)}>Бесплаьные поинты</a>
                        {/* <a href="" onClick={(event) => this.addFilter(event, 'test')}><span>FAQ</span></a> */}
                        {/* <a href="" onClick={(event) => this.handelTestApi(event)}><span>бесплатные поинты</span></a> */}
                    </div>
                </div>
                {this.logButt}
                <LoginModal/>
                <RegistrationModal/>
                <UserSettingsModal/>
                <InfoModal/>
                <FreePointsModal/>
            </div>
        );
    }
}

Header.propTypes = {
    onResetFilter: PropTypes.func,
    onFilter: PropTypes.func,
    testApi: PropTypes.func,
    logout: PropTypes.func,
    posts: PropTypes.object.isRequired,
    user: PropTypes.object.isRequired,
    showLoginModal: PropTypes.func,
    showRegistrationModal: PropTypes.func,
    showFreePointsModal: PropTypes.func,
    showUserSettingsModal: PropTypes.func,
    usersOnline: PropTypes.number
};
const mapStateToProps = (state) => {
    return {
        filter: state.filter,
        posts: state.posts,
        user: state.user,
        usersOnline: state.games.usersOnline
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        onResetFilter: () => dispatch(filterTable('')),
        onFilter: (text) => dispatch(filterTable(text)),
        testApi: () => dispatch(testApi()),
        logout: () => dispatch(logout()),
        showLoginModal: () => dispatch(showLoginModal()),
        showUserSettingsModal: () => dispatch(showUserSettingsModal()),
        showRegistrationModal: () => dispatch(showRegistrationModal()),
        showFreePointsModal: () => dispatch(showFreePointsModal())
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
    null,
    {pure: false}
)(Header);
