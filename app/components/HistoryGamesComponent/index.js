import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';

import {config} from '../../config';

// import { logout, showLoginModal, showRegistrationModal, showUserSettingsModal } from '../../actions/user';
import {getGamesHistory} from '../../actions/usersInfo';
// import { Link } from 'react-router';
// import './style.scss';

class HistoryGamesComponent extends Component {

    componentDidMount() {
        this.props.getGamesHistory("duel");
    }

    render() {
        return (
            <table className="table">
                <thead className="status-message head">
                <tr>
                    <th className="col">#Игры</th>
                    <th className="col">Банк</th>
                    <th className="col">Победитель</th>
                </tr>
                </thead>
                <tbody>
                {this.props.gamesHistory ? this.props.gamesHistory.map((game, key) => {
                        return (
                            <tr className="status-message" key={key}>

                                <td className="id-game">#{game.id}</td>
                                <td>{game.bank}</td>
                                <td className="username">
                                    {game.winner ? (
                                            <span className="userAvatar">
                                        <img
                                            srcSet={ game.winner.avatar ? config.host + game.winner.avatar : require('../../img/user-avatar.png')}
                                            alt=""/>
                                    </span>
                                        ) : ''}
                                    {game.winner ? game.winner.userName : ""}
                                </td>
                            </tr>
                        );
                    }) : ''}
                </tbody>
            </table>
        );
    }
}
HistoryGamesComponent.propTypes = {
    getGamesHistory: PropTypes.func,
    gamesHistory: PropTypes.array

};
const mapStateToProps = (state) => {
    return {
        gamesHistory: state.usersInfo.gamesHistory,

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getGamesHistory: (req) => dispatch(getGamesHistory(req)),

    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HistoryGamesComponent);

// export default Header;

// import React from 'react';


// const Header = () =>
//     <div>
//         <h1>Header</h1>
//     </div>;
//
//
// export default Header;

