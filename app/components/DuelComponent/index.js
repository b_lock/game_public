import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router';
import {setActiveGame, setActiveGameType} from '../../actions/games';
import DuelChoice from '../DuelChoice';
import DuelProcComponent from '../DuelProcComponent';

import './style.scss';

class DuelComponent extends Component {

    componentWillMount() {
        this.duel = this.props.duels.find(duel => duel.id === this.props.activeGame);
    }

    componentWillUpdate(nextProps) {
        this.duel = nextProps.duels.find(duel => duel.id === nextProps.activeGame);
    }

    getClassNameRate(duel) {
        const active = this.props.activeGame === duel.id ? "active" : "";
        const ready = duel.user1 || duel.user2 ? "ready" : "";
        return `${active} ${ready}`;
    }

    changeActiveGame(e) {
        this.props.setActiveGame(+e.target.dataset.id);
        this.props.setActiveGameType(+e.target.dataset.type);
    }

    render() {
        return (
            <div className="container-duel">
                <div className="content-block">
                    <div className="title">
                        <h1><span>Дуэль<i className="deliv"/></span></h1>
                        <div className="logo-background"></div>
                    </div>
                    <div className="content">
                        <div className="head-block">
                            <a href="#" className="question"/>
                            <a href="#" className="security"/>
                            {(this.duel && this.duel.status === 1 && (this.duel.user1.id === this.props.userId || this.duel.user2.id === this.props.userId )) ? "" :
                                (<ul className="menu-rate">
                                    {this.props.duels.map((duel) => {
                                        if (+this.props.gameTypes[duel.type].tournament === 0) {
                                            return (
                                                <li key={duel.id}
                                                    className={::this.getClassNameRate(duel)}>
                                                    <a href="#" data-id={duel.id} data-type={duel.type}
                                                       onClick={::this.changeActiveGame}>{this.props.gameTypes[duel.type].rate}</a>
                                                </li>
                                            );
                                        }
                                        return "";
                                    })}
                                </ul>)
                            }
                            {this.props.placed === 'main-page' ? <Link to="/duel" className="expand"/> : <Link to="/" className="expand"/>}
                        </div>
                        {(this.duel && this.duel.status === 1) ? <DuelProcComponent placed={this.props.placed}/> :
                            <DuelChoice placed={this.props.placed}/>}
                    </div>
                </div>
            </div>
        );

        // if (this.duel && this.duel.status === 1) {
        //     return <DuelProcComponent placed={this.props.placed}/>;
        // }
        // return <DuelChoice placed={this.props.placed}/>;
    }
}

DuelComponent.propTypes = {
    hideDuelComponent: PropTypes.bool,
    gameTypes: PropTypes.object,
    setActiveGame: PropTypes.func,
    setActiveGameType: PropTypes.func,
    placed: PropTypes.string,
    duels: PropTypes.array,
    activeGame: PropTypes.number,
    userId: PropTypes.number
};

const mapStateToProps = (state) => {
    return {
        gameTypes: state.games.types,
        duels: state.games.duels,
        activeGame: state.settings.activeGame,
        isLogged: state.user.isLogged,
        userId: state.user.id
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setActiveGame: (id) => dispatch(setActiveGame(id)),
        setActiveGameType: (type) => dispatch(setActiveGameType(type)),
        /* setActiveGame: (id) => dispatch(setActiveGame(id)),
         showLoginModal: () => dispatch(showLoginModal()),
         takePlace: (side, duelId) => dispatch(takePlace(side, duelId)),
         takeOffPlace: (side, duelId) => dispatch(takeOffPlace(side, duelId)), */
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DuelComponent);
// export default DuelChoice;


