import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setUserChoice, setDuelReplyReset, newGame } from '../../actions/games';
import UserAvatarComponent from '../UserAvatarComponent';
import TimerComponent from '../TimerComponent';


import './style.scss';

class DuelProcComponent extends Component {

    constructor() {
        super();
        // this.seconds = 15;
        console.log("constructor", this);
    }

    componentWillMount() {
        if (this.props.activeDuels.active[this.props.activeGame]) {
            this.startSeconds = this.seconds - ((Date.now() - this.props.activeDuels.active[this.props.activeGame].tic) / 1000);
        } else {
            this.startSeconds = this.seconds;
        }
        console.log("startSeconds", this.startSeconds >= 0 ? Math.round(this.startSeconds) : 0);
        this.setState({
            seconds: this.startSeconds >= 0 ? Math.round(this.startSeconds) : 0
        });
        this.replay = false;
        this.choice = null;
        this.duel = this.props.duels.find(duel => duel.id === this.props.activeGame);
        this.duel.startTime = this.seconds;
    }

    componentDidMount() {
        console.log("did mount");
        this.seconds = this.props.duelTime || 10;
        this.startTimer();
    }

    componentWillUpdate(nextProps) {
        this.duel = nextProps.duels.find(duel => duel.id === nextProps.activeGame);
        if (this.props.activeDuels.duels[this.props.activeGame] && this.props.activeDuels.duels[this.props.activeGame].reply === true) {
            this.user1Choice = this.choice;
            this.user2Choice = this.choice;
        }
    }

    componentDidUpdate() {
        console.log(this);
        if (!this.replay && this.props.activeDuels.duels[this.props.activeGame] && this.props.activeDuels.duels[this.props.activeGame].reply === true) {
            console.log('replay');
            this.props.setDuelReplyReset(this.props.activeGame);
            this.refs.titleMessage.innerText = "Ничья";
            setTimeout(function () {
                this.replay = true;
                const rival = document.getElementsByClassName('rival');
                if (rival.length > 0) {
                    for (let i = 0; i < rival.length; i++) {
                        rival[i].classList.remove('rival');
                    }
                }
                const active = document.querySelectorAll('.choice-card .active');
                if (active.length > 0) {
                    for (let i = 0; i < active.length; i++) {
                        active[i].classList.remove('active');
                    }
                }
                this.user1Choice = null;
                this.user2Choice = null;

                this.refs.titleMessage.innerText = "Переирывание";

                this.choice = null;
                this.startTimer();
            }.bind(this), 5000);
        }
        if (this.duel.winner !== null && !this.replay) {
            if (this.duel.user1.id === this.props.user.id) {
                const user2Choice = this.refs[this.duel.user2Choice];
                if (user2Choice) {
                    user2Choice.classList.add('rival');
                }
            } else {
                const user1Choice = this.refs[this.duel.user1Choice];
                if (user1Choice) {
                    user1Choice.classList.add('rival');
                }
            }
        }
        if (this.duel.winner !== 0 && this.duel.winner !== null) {
            if (this.duel.user1.id === this.props.user.id || this.duel.user2.id === this.props.user.id) {
                this.refs.titleMessage.innerText = this.duel.winner === this.props.user.id ? "Вы выиграли" : "Вы проиграли";
            }
            if (!this.user1Choice || !this.user2Choice) {
                this.user1Choice = this.duel.user1Choice;
                this.user2Choice = this.duel.user2Choice;
                this.forceUpdate();
            } else {
                if (!this.newGame) {
                    this.newGame = true;
                    this.props.newGame(this.duel.id);
                }
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    startTimer() {
        this.replay = false;
        this.refs.startTimer.start();
    }

    /* startTimer() {
        let sec;
        console.log(this.seconds);
        if (this.replay) {
            this.setState({
                seconds: this.seconds
            });
            sec = this.seconds;
        } else {
            sec = this.state.seconds;
        }
        const startTime = this.seconds;
        const per = 360 / startTime;
        this.setCanvas(-90, 270 - (startTime - sec) * per);
        this.timerID = setInterval(
            () => {
                if (this.state.seconds > 0) {
                    this.setCanvas(-90, (270 - per) - (startTime - this.state.seconds) * per);
                    this.setState({
                        seconds: this.state.seconds - 1
                    });
                } else {
                    this.replay = false;
                    clearInterval(this.timerID);
                    this.props.setUserChoice(this.choice, this.duel.id);
                }
            },
            1000
        );
    } */

    /* drawDefaultCirc() {
         const canvas = document.getElementById('timer-canvas');
         const ctx = canvas.getContext('2d');
         ctx.beginPath();
         ctx.lineWidth = 2;
         ctx.strokeStyle = "#626262";
         ctx.arc(54, 54, 50, -90 / 180 * Math.PI, 270 / 180 * Math.PI, false);
         ctx.stroke();
     }

     setCanvas(start, end) {
         const canvas = document.getElementById('timer-canvas');
         if (canvas.getContext) {
             const ctx = canvas.getContext('2d');
             ctx.clearRect(0, 0, 108, 108);
             this.drawDefaultCirc();
             ctx.beginPath();
             ctx.lineWidth = 4;
             ctx.strokeStyle = "#b48801";
             ctx.arc(54, 54, 50, start / 180 * Math.PI, end / 180 * Math.PI, false);
             ctx.stroke();

             ctx.beginPath();
             ctx.lineWidth = 8;
             ctx.strokeStyle = "#b48801";
             ctx.lineCap = 'round';
             ctx.arc(54, 54, 50, (end - 1) / 180 * Math.PI, (end + 1) / 180 * Math.PI, false);
             ctx.stroke();
             ctx.beginPath();

             ctx.lineWidth = 6;
             ctx.strokeStyle = "#303030";
             ctx.arc(54, 54, 50, (end) / 180 * Math.PI, (end) / 180 * Math.PI, false);
             ctx.stroke();
             ctx.lineWidth = 3;
             ctx.strokeStyle = "#626262";
             ctx.arc(54, 54, 50, (end) / 180 * Math.PI, (end) / 180 * Math.PI, false);
             ctx.stroke();
         }
     } */


    choiceFn(e) {
        const activeElement = document.querySelectorAll('.choice-card .active');
        if (activeElement.length) {
            activeElement[0].classList.remove('active');
        }
        e.target.classList.add('active');
        this.choice = e.currentTarget.dataset.choice;
    }

    render() {
        return (
            <div className="duel-proc">
                <h2 className="title-message" ref="titleMessage"/>
                <div className="user-places">
                    <div className="user-place">
                        <UserAvatarComponent
                            className={"avatar-place " + (this.user1Choice ? this.user1Choice + "-avatar" : "")}
                            name={this.duel && this.duel.user1 ? this.duel.user1.userName : "\u00A0"}
                            avatarImg={this.duel && this.duel.user1 ? this.duel.user1.avatar : ''}/>
                    </div>
                    <div className="divider"></div>
                    {/* <div className="timer">
                        <span className="seconds">{this.state.seconds}</span>
                        <canvas id="timer-canvas" width={108} height={108}/>
                    </div> */}
                    <TimerComponent duelTimer={this.props.duelTime} ref="startTimer" timerStop={() => {
                        console.log('stop');
                        this.props.setUserChoice(this.choice, this.duel.id);
                    }}/>
                    <div className="divider"></div>
                    <div className="user-place">
                        <UserAvatarComponent
                            className={"avatar-place " + (this.user2Choice ? this.user2Choice + "-avatar" : "")}
                            name={this.duel && this.duel.user2 ? this.duel.user2.userName : "\u00A0"}
                            avatarImg={this.duel && this.duel.user2 ? this.duel.user2.avatar : ''}/>
                    </div>
                </div>
                {/* {(this.duel && this.duel.status === 1 && (this.duel.user1.id === this.props.user.id || this.duel.user2.id === this.props.user.id )) ? (*/}
                    <div
                    className={this.duel && this.duel.status === 1 && (this.duel.user1.id === this.props.user.id || this.duel.user2.id === this.props.user.id )
                    ? 'choice-card'
                    : 'choice-card v-hidden'}>
                    <div className="stone"
                    ref="stone"
                    data-choice="stone"
                    onClick={::this.choiceFn}></div>
                    <div className="scissors"
                    ref="scissors"
                    data-choice="scissors"
                    onClick={::this.choiceFn}></div>
                    <div className="paper"
                    ref="paper"
                    data-choice="paper"
                    onClick={::this.choiceFn}></div>
                    </div>
                {/* ): ""} */}

            </div>
        );
    }
}

DuelProcComponent.propTypes = {
    placed: React.PropTypes.string,
    duels: React.PropTypes.array,
    activeGame: React.PropTypes.number,
    setUserChoice: React.PropTypes.func,
    user: React.PropTypes.object,
    activeDuels: React.PropTypes.object,
    setDuelReplyReset: React.PropTypes.func,
    newGame: React.PropTypes.func,
    duelTime: React.PropTypes.number,
};

const mapStateToProps = (state) => {
    return {
        duels: state.games.duels,
        activeGame: state.settings.activeGame,
        user: state.user,
        activeDuels: state.activeDuels,
        duelTime: state.main.init.duel_time
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUserChoice: (userChoice, duelId) => dispatch(setUserChoice(userChoice, duelId)),
        setDuelReplyReset: (duelId) => dispatch(setDuelReplyReset(duelId)),
        newGame: (duelId) => dispatch(newGame(duelId))
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DuelProcComponent);

