import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

// import {config} from '../../config';
//
// import { logout, showLoginModal, showRegistrationModal, showUserSettingsModal } from '../../actions/user';
// import {getUsersOnlineInfo} from '../../actions/usersInfo';
// import { Link } from 'react-router';
import './style.scss';
class HistoryComponent extends Component {
    render() {
        return (
            <div>
                <h1 className="title-table"><span>История игр</span></h1>
                <div className="users-history-info">
                    <div className="content-block">
                        <div className="title">
                            <h1>
                           <span>
                               <Link to="/history/tournament" activeClassName="active">Турнир</Link>
                               <Link to="/history/duel" activeClassName="active">Дуэль</Link>
                               <i className="deliv"/>
                            </span>
                            </h1>
                        </div>
                        {this.props.children }
                    </div>
                </div>
            </div>
        );
    }
}
HistoryComponent.propTypes = {
    // getUsersOnlineInfo: PropTypes.func,
    children: PropTypes.object

};
const mapStateToProps = (/* state */) => {
    return {
        // userInfoOnline: state.usersInfo.userInfoOnline,
    };
};

const mapDispatchToProps = (/* dispatch */) => {
    return {};
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HistoryComponent);

// export default Header;

// import React from 'react';


// const Header = () =>
//     <div>
//         <h1>Header</h1>
//     </div>;
//
//
// export default Header;

