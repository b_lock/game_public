import React, {Component, PropTypes} from 'react';

import {config} from '../../config';

import './style.scss';

class UserAvatarComponent extends Component {

    componentWillMount() {
    }


    componentDidMount() {
        // this.refs;
        console.log('componentDidMount');
        const imgAvatar = this.refs.imgAvatar;
        this.getImage(imgAvatar, imgAvatar.src);
    }

    componentWillReceiveProps() {

    }

    componentWillUpdate(/* nextProps */) {
    }

    componentDidUpdate() {
        const imgAvatar = this.refs.imgAvatar;
        this.getImage(imgAvatar, imgAvatar.src);
    }

    getImage(imgAvatar, src) {
        const img = new Image();
        img.src = src;
        img.onload = () => {
            if (img.width > img.height) {
                imgAvatar.style.width = "auto";
                imgAvatar.style.height = "100%";
            }else{
                imgAvatar.style.width = "100%";
                imgAvatar.style.height = "auto";
            }
        };
    }

    render() {
        let className = "user-avatar-component " + this.props.className;

        return (
            <div className={className}>
                <div className="avatar-place">
                    <div className="wrap-avatar">
                        <img ref="imgAvatar"
                             src={this.props.avatarImg ? config.host + this.props.avatarImg : require('../../img/user-avatar.png')}
                             alt="avatar" className="avatar"/>
                    </div>
                    <div className="wrapper-top"></div>
                    <div className="wrapper-bottom"></div>
                </div>
                <div className="username">
                    <span>{this.props.name ? this.props.name : "\u00A0"}</span>
                </div>
            </div>
        );
        // if (this.duel && this.duel.status === 1) {
        //     return <DuelProcComponent placed={this.props.placed}/>;
        // }
        // return <DuelChoice placed={this.props.placed}/>;
    }
}

UserAvatarComponent.propTypes = {
    className: PropTypes.string,
    avatarImg: PropTypes.string,
    name: PropTypes.string,
};


export default UserAvatarComponent;


