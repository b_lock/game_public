import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import Boron from 'boron';

import {hideRegistrationModal, registration, errorFieldRegistration} from '../../actions/user';

class RegistrationModal extends Component {

    componentWillReceiveProps(nextProps) {
        if (nextProps.showRegistrationModal) {
            this.showModal();
        }
        if (nextProps.isLogged) {
            this.hideModal();
        }
    }

    inputUserName;
    inputUserPassword;
    inputEmail;

    hideModal() {
        this.refs.modal.hide();
    }

    showModal() {
        this.refs.modal.show();
    }

    handleClose() {
        this.props.hideRegistrationModal();
        this.props.errorFieldRegistration({
            userName: true,
            password: true,
            email: true
        });
    }

    handelSubmit(e) {
        e.preventDefault();
        console.log(this.inputUserName.value);
        if (this.inputUserName.value.length > 1 && this.inputUserPassword.value.length > 5 && this.inputEmail.value.length > 5) {
            this.props.registration(this.inputUserName.value, this.inputEmail.value, this.inputUserPassword.value);
            this.props.errorFieldRegistration({
                userName: true,
                email: true,
                password: true
            });
        } else {
            this.props.errorFieldRegistration({
                userName: this.inputUserName.value.length > 1,
                email: this.inputEmail.value.length > 5,
                password: this.inputUserPassword.value.length > 5
            });
        }
    }

    render() {
        const Modal = Boron.FadeModal;
        const contentStyle = {
            background: false,
        };
        const modalStyle = {
            width: false,
            height: false
        };
        const backdropStyle = {
            backgroundColor: '#171818'
        };
        return (
            <Modal ref="modal" onHide={::this.handleClose} className="Registration-modal" modalStyle={modalStyle}
                   backdropStyle={backdropStyle}
                   contentStyle={contentStyle}>
                <div className="modal-content content-block">
                    <div className="title">
                        <h1><span>Регистрация <i className="deliv"/></span></h1>
                    </div>
                    <span className="close-modal" onClick={::this.hideModal}/>
                    <div className="content">
                        <form onSubmit={::this.handelSubmit}>
                            <input className={this.props.usernameField ? '' : 'error'} name="username" type="text"
                                   placeholder="Логин" ref={node => {
                                this.inputUserName = node;
                            }}/>
                            <input className={this.props.emailField ? '' : 'error'} name="email" type="email"
                                   placeholder="Email" ref={node => {
                                this.inputEmail = node;
                            }}/>
                            <input className={this.props.passwordField ? '' : 'error'} name="password" type="password"
                                   placeholder="Пароль" ref={node => {
                                this.inputUserPassword = node;
                            }}/>
                            <div className="button-wrap">
                                <button>зарегистрироваться</button>
                                <span className="wrapper-top"/>
                                <span className="wrapper-bottom"/>
                            </div>
                        </form>
                    </div>
                </div>
            </Modal>
        );
    }
}

RegistrationModal.propTypes = {
    showRegistrationModal: PropTypes.bool,
    hideRegistrationModal: PropTypes.func,
    errorFieldRegistration: PropTypes.func,
    registration: PropTypes.func,
    usernameField: PropTypes.bool,
    emailField: PropTypes.bool,
    passwordField: PropTypes.bool,

};

const mapStateToProps = (state) => {
    return {
        showRegistrationModal: state.modal.showRegistrationModal,
        usernameField: state.errorFields.userName,
        passwordField: state.errorFields.password,
        emailField: state.errorFields.email,
        isLogged: state.user.isLogged,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        hideRegistrationModal: () => dispatch(hideRegistrationModal()),
        registration: (userName, email, password) => dispatch(registration(userName, email, password)),
        errorFieldRegistration: (fields) => dispatch(errorFieldRegistration(fields)),
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(RegistrationModal);

