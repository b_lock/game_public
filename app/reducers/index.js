import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import * as types from '../actions/types';

const main = (state = {
    faq: {},
    init: {},
    spinner: false
}, action) => {
    switch (action.type) {
        case types.SET_FAQ:
            return {...state, ...{faq: action.faq}};
        case types.INIT_STATE:
            return {...state, ...{init: action.init}};
        case types.SPINNER_ON:
            return {...state, ...{spinner: true}};
        case types.SPINNER_OFF:
            return {...state, ...{spinner: false}};
        default:
            return state;
    }
};

const filter = (state = '', action) => {
    switch (action.type) {
        case types.FILTER:
            return action.filter;
        default:
            return state;
    }
};
const user = (state = {
    isLogged: false,
    username: '',
    token: '',
    id: null,
    avatar: '',
    points: null,
    userInfo: {}
}, action) => {
    switch (action.type) {
        case types.LOGIN:
            return {...state, ...action.user, ...{points: +action.user.points}};
        case types.LOGOUT:
            return action.user;
        case types.SET_USER_AVATAR:
            return {...state, ...{avatar: action.avatar}};
        default:
            return state;
    }
};
const playersInfo = (state = {}, action) => {
    switch (action.type) {
        case types.SET_PLAYERS_INFO:
            return {...state, ...action.playersObj};
        default:
            return state;
    }
};

const modal = (state = {
    showLoginModal: false,
    showRegistrationModal: false,
    showUserSettingsModal: false,
    infoModalProp: {
        title: "",
        infoText: ""
    },
    showFreePointsModal: false
}, action) => {
    switch (action.type) {
        case types.SHOW_LOGIN_MODAL:
            return {
                ...state, showLoginModal: true
            };
        case types.HIDE_LOGIN_MODAL:
            return {
                ...state, showLoginModal: false
            };
        case types.SHOW_REGISTRATION_MODAL:
            return {
                ...state, showRegistrationModal: true
            };
        case types.HIDE_REGISTRATION_MODAL:
            return {
                ...state, showRegistrationModal: false
            };
        case types.SHOW_USER_SETTINGS_MODAL:
            return {
                ...state, showUserSettingsModal: true
            };
        case types.HIDE_USER_SETTINGS_MODAL:
            return {
                ...state, showUserSettingsModal: false
            };
        case types.SHOW_FREE_POINTS_MODAL:
            return {
                ...state, showFreePointsModal: true
            };
        case types.HIDE_FREE_POINTS_MODAL:
            return {
                ...state, showFreePointsModal: false
            };
        case types.HIDE_INFO_MODAL:
            return {
                ...state, showInfoModal: false, infoModalProp: {
                    title: "",
                    infoText: ""
                }
            };
        case types.SHOW_INFO_MODAL:
            return {
                ...state, showInfoModal: true, ...{
                    infoModalProp: action.infoModalProp ? action.infoModalProp : {
                        title: "",
                        infoText: ""
                    }
                }
            };
        default:
            return state;
    }
};

const posts = (state = {
    isFetching: false,
    didInvalidate: false,
    items: []
}, action) => {
    switch (action.type) {
        case types.RECEIVE_POSTS:
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                items: action.posts,
                lastUpdated: action.receivedAt
            });
        default:
            return state;
    }
};

const errorFields = (state = {
    userName: true,
    email: true,
    password: true,
    freePoints: true
}, action) => {
    switch (action.type) {
        case types.ERROR_FIELDS_LOGIN:
            return {...state, ...action.action};
        case types.ERROR_FIELDS_REGISTRATION:
            return {...state, ...action.action};
        case types.ERROR_FIELDS_FREE_POINTS:
            return {...state, ...{freePoints: action.status}};
        default:
            return state;
    }
};

const games = (state = {
    duels: [],
    tournament: {},
    types: {}
}, action) => {
    switch (action.type) {
        case types.SET_GAMES:
            return {...state, ...action.pong};
        default:
            return state;
    }
};

const settings = (state = {
    activeGame: null,
    activeGameType: null
}, action) => {
    switch (action.type) {
        case types.SET_ACTIVE_GAME:
            return {...state, activeGame: action.activeGame};
        case types.SET_ACTIVE_GAME_TYPE:
            return {...state, activeGameType: action.activeGameType};
        default:
            return state;
    }
};

const activeDuels = (state = {
    duels: {},
    active: {},
    count: 0
}, action) => {
    let active;
    switch (action.type) {
        case types.SET_DUEL_REPLY:
            return {...state, duels: {...action.duels}, count: state.count + 1};
        case types.SET_DUEL_REPLY_RESET:
            return {...state, duels: {...action.duels}, count: 0};
        case types.SET_PLAYED_GAME:
            active = {...state.active};
            if (!active[action.activeGame]) {
                active[action.activeGame] = {};
            }
            active[action.activeGame].activeGame = true;
            active[action.activeGame].tic = active[action.activeGame].tic || Date.now();
            return {...state, active: {...active}, count: 0};
        case types.RESET_PLAYED_GAME:
            active = {...state.active};
            if (!active[action.activeGame]) {
                active[action.activeGame] = {};
            }
            active[action.activeGame].activeGame = false;
            return {...state, active: {...active}, count: 0};
        default:
            return state;
    }
};

// SET_ONLINE_USER_COUNT
const usersInfo = (state = {
    userInfoOnline: [],
    usersTop: [],
    gamesHistory: []

}, action) => {
    switch (action.type) {
        case types.SET_ONLINE_USERS_INFO:
            return {...state, ...{userInfoOnline: action.userInfoOnline}};
        case types.SET_USERS_TOP_INFO:
            return {...state, ...{usersTop: action.usersTop}};
        case types.SET_USERS_HISORY_INFO:
            return {...state, ...{gamesHistory: action.gamesHistory}};
        default:
            return state;
    }
};

const rootReducer = combineReducers({
    main,
    filter,
    user,
    errorFields,
    modal,
    posts,
    games,
    settings,
    activeDuels,
    playersInfo,
    usersInfo,
    routing
});

export default rootReducer;
