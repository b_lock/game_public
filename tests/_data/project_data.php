<?php
return [
    [
        'id'                  => '1',
        'user_id'             => '1',
        'project_name'        => 'test_project',
        'project_description' => 'test project description',
        'created_at'          => '1402312317',
        'updated_at'          => '1402312317',
    ],
    [
        'id'                  => '2',
        'user_id'             => '1',
        'project_name'        => 'test_project2',
        'project_description' => 'test project2 description',
        'created_at'          => '1402312317',
        'updated_at'          => '1402312317',
    ],
];