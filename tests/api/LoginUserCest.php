<?php


use app\tests\fixtures\UserFixture;

class LoginUserCest
{
    private function helper(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Access-Control-Allow-Credentials', true);
        $I->haveHttpHeader('Access-Control-Allow-Origin', '*');
    }

    public function _before(\ApiTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class'    => UserFixture::className(),
            ]
        ]);
    }


    public function loginUser(\ApiTester $I)
    {
        $I->wantTo('login a user via API!');

        $this->helper($I);

        $I->sendPOST('/login', [
            'login'    => 'test_user',
            'password' => 'password_0'
        ]);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(
            [
                'username' => 'string:!empty',
                'token'    => 'string:!empty'
            ]);
        $I->seeResponseContainsJson([
            'username' => 'test_user'
        ]);
    }

    public function loginUserWithWrongPassword(\ApiTester $I)
    {
        $I->wantTo('login  a user with wrong password via API!');

        $this->helper($I);

        $I->sendPOST('/login', [
            'login'    => 'test_user',
            'password' => 'fake_password'
        ]);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(
            [
                'name' => 'string:!empty',
                'status'    => 'integer:>0'
            ]);
        $I->seeResponseContainsJson([
            'name' => 'Unauthorized',
            'status' => 401
        ]);
    }
    public function loginUserWithWrongUsername(\ApiTester $I)
    {
        $I->wantTo('login a user with wrong username via API!');

        $this->helper($I);

        $I->sendPOST('/login', [
            'login'    => 'fake_username',
            'password' => 'password_0'
        ]);

        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(
            [
                'name' => 'string:!empty',
                'status'    => 'integer:>0'
            ]);
        $I->seeResponseContainsJson([
            'name' => 'Unauthorized',
            'status' => 401
        ]);
    }

}
