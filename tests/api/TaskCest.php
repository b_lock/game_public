<?php


use app\tests\fixtures\TaskFixture;
use Codeception\Lib\Console\Output;

class TaskCest
{
    private $token;

    private function helper(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Access-Control-Allow-Credentials', true);
        $I->haveHttpHeader('Access-Control-Allow-Origin', '*');
        $I->amBearerAuthenticated($this->token);
    }

    public function _before(\ApiTester $I)
    {
        $I->haveFixtures([
            'task' => [
                'class' => TaskFixture::className(),
            ]
        ]);

        $user        = \app\models\User::findOne(1);
        $this->token = $user->getJWT();

    }

    public function getTasksWithWrongToken(\ApiTester $I)
    {
        $I->wantTo('get tasks with wrong token');

        $this->helper($I);
        $I->amBearerAuthenticated('wrong token');

        $I->sendGET('/get-tasks', ['project_id' => 2]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
    }

    public function getTasksWithEmptyTasksProjectId(\ApiTester $I)
    {
        $I->wantTo('get tasks with empty task on project id');

        $this->helper($I);

        $I->sendGET('/get-tasks', ['project_id' => 2]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
    }

    public function getTasksWithWrongProjectId(\ApiTester $I)
    {
        $I->wantTo('get tasks with wrong project id');

        $this->helper($I);

        $I->sendGET('/get-tasks', ['project_id' => 100]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::NOT_FOUND); // 404
        $I->seeResponseIsJson();
    }

    public function getTasks(\ApiTester $I)
    {
        $I->wantTo('get tasks');

        $this->helper($I);

        $I->sendGET('/get-tasks', ['project_id' => 1]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseContainsJson([
            'id'               => 1,
            'task_name'        => 'test task name',
            'task_description' => 'test task description',
            'task_status'      => 1
        ]);
        $I->seeResponseMatchesJsonType([
            'id'               => 'integer:>0',
            'task_name'        => 'string',
            'task_description' => 'string',
            'task_status'      => 'integer:>0'
        ]);
    }

}
