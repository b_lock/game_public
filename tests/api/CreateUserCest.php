<?php


use app\tests\fixtures\UserFixture;

class CreateUserCest
{
    private function helper(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Access-Control-Allow-Credentials', true);
        $I->haveHttpHeader('Access-Control-Allow-Origin', '*');
    }

    public function _before(\ApiTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class'    => UserFixture::className(),
            ]
        ]);
    }

    public function createUser(\ApiTester $I)
    {
        $I->wantTo('create a user via API!');
//
        $this->helper($I);

        $I->sendPOST('/register', [
            'login'    => 'test_api',
            'email'    => 'test_api@test.com',
            'password' => '123456'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(
            [
                'username' => 'string:!empty',
                'token'    => 'string:!empty'
            ]);
        $I->seeResponseContainsJson([
            'username' => 'test_api'
        ]);

    }

    public function createUserWithWrongName(\ApiTester $I)
    {
        $I->wantTo('create a user with wrong name');

        $this->helper($I);

        $I->sendPOST('/register', [
            'login'    => 'test_user',
            'email'    => 'test_api@test.com',
            'password' => '123456'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(
            [
                'errors' => ['username' => [0 => 'string:!empty']],
            ]);
    }

    public function createUserWithEmptyName(\ApiTester $I)
    {
        $I->wantTo('create a user with empty name');

        $this->helper($I);

        $I->sendPOST('/register', [
            'login'    => '',
            'email'    => 'test_api@test.com',
            'password' => '123456'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(
            [
                'errors' => ['username' => [0 => 'string:!empty']],
            ]);
    }

    public function createUserWithWrongEmail(\ApiTester $I)
    {
        $I->wantTo('create a user with wrong email');

        $this->helper($I);

        $I->sendPOST('/register', [
            'login'    => 'test_api',
            'email'    => 'test_user@mail.com',
            'password' => '123456'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(
            [
                'errors' => ['email' => [0 => 'string:!empty']],
            ]);
    }

    public function createUserWithEmptyEmail(\ApiTester $I)
    {
//        $I = new ApiTester($scenario);
        $I->wantTo('create a user with empty email');

        $this->helper($I);

        $I->sendPOST('/register', [
            'login'    => 'test_api',
            'email'    => '',
            'password' => '123456'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType(
            [
                'errors' => ['email' => [0 => 'string:!empty']],
            ]);
    }
}


