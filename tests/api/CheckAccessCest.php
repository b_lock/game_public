<?php


use app\tests\fixtures\UserFixture;
use Codeception\Lib\Console\Output;

class CheckAccessCest
{
    private $token;

    private function helper(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Access-Control-Allow-Credentials', true);
        $I->haveHttpHeader('Access-Control-Allow-Origin', '*');
        $I->amBearerAuthenticated($this->token);
    }

    public function _before(\ApiTester $I)
    {
        $I->haveFixtures([
            'user' => [
                'class' => UserFixture::className(),
            ]
        ]);

        $user        = \app\models\User::findOne(1);
        $this->token = $user->getJWT();

    }

    public function checkTokenWithWrongToken(\ApiTester $I)
    {
        $I->wantTo('check token with wrong token');

        $this->helper($I);

        $I->amBearerAuthenticated("wrong token");
        $I->sendPOST('/check-token', []);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
    }

    public function checkToken(\ApiTester $I)
    {
        $I->wantTo('check token');

        $this->helper($I);

        $I->sendPOST('/check-token', []);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseContainsJson();
        $I->seeResponseMatchesJsonType(
            [
                'token' => 'boolean',
            ]);
        $I->seeResponseContainsJson(['token' => true]);
    }

    public function checkAccessOption(\ApiTester $I)
    {
        $I->wantTo('check option');

        $this->helper($I);
        $I->amBearerAuthenticated("wrong token");

        $I->sendOPTIONS('/options');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
    }

}
