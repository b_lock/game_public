<?php


use app\tests\fixtures\ProjectFixture;
use Codeception\Lib\Console\Output;

class ProjectCest
{
    private $token;

    private function helper(\ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Access-Control-Allow-Credentials', true);
        $I->haveHttpHeader('Access-Control-Allow-Origin', '*');
        $I->amBearerAuthenticated($this->token);
    }

    public function _before(\ApiTester $I)
    {
        $I->haveFixtures([
            'project' => [
                'class' => ProjectFixture::className(),
            ]
        ]);

        $user        = \app\models\User::findOne(1);
        $this->token = $user->getJWT();

    }


    public function createProjectWithotProjectName(\ApiTester $I)
    {
        $this->helper($I);

        $I->sendPOST('/add-project', [
            'project_name'        => '',
            'project_description' => 'test project 1 decription'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseContainsJson();
        $I->seeResponseMatchesJsonType(
            [
                'errors' => 'array:!empty',
            ]);
    }

    public function createProjectWithWrongToken(\ApiTester $I)
    {
        $this->helper($I);
        $I->amBearerAuthenticated("wrong token");

        $I->sendPOST('/add-project', [
            'project_name'        => '',
            'project_description' => 'test project 1 decription'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNAUTHORIZED); // 401
    }

    public function createProject(\ApiTester $I)
    {
        $I->wantTo('create a project');

        $this->helper($I);

        $I->sendPOST('/add-project', [
            'project_name'        => 'test project 1',
            'project_description' => 'test project 1 decription'
        ]);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseContainsJson();
        $I->seeResponseMatchesJsonType(
            [
                'projects' => 'array:!empty',
            ]);
    }

    public function getProjects(\ApiTester $I)
    {
        $I->wantTo('create a project');

        $this->helper($I);

        $I->sendGET('/projects');
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
        $I->seeResponseContainsJson([
            'id'                  => '1',
            'project_name'        => 'test_project',
            'project_description' => 'test project description',
            'status'              => '1'
        ]);
        $I->seeResponseMatchesJsonType([
            'id'                  => 'string',
            'project_name'        => 'string',
            'project_description' => 'string',
            'status'              => 'string'
        ]);
    }


}
