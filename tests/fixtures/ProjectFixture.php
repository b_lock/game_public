<?php
namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class ProjectFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Project';
    public $dataFile = 'tests\_data\project_data.php';
    public $depends = ['app\tests\fixtures\UserFixture'];
}