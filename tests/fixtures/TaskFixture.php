<?php
namespace app\tests\fixtures;

use yii\test\ActiveFixture;

class TaskFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Task';
    public $dataFile = 'tests\_data\task_data.php';
    public $depends = ['app\tests\fixtures\UserFixture','app\tests\fixtures\ProjectFixture'];
}