<?php
namespace tests\models;
use app\models\LoginForm;
use app\models\User;
use app\tests\fixtures\UserFixture;
use Codeception\Lib\Console\Output;

class UserTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class'    => UserFixture::className(),
            ]
        ]);
    }

    public function testFindUserById()
    {
        expect_that($user = User::findIdentity(1));
        expect($user->username)->equals('test_user');

        expect_not(User::findIdentity(999));
    }
    public function testFindUserByResetToken()
    {
        expect_that($user = User::findByPasswordResetToken('ExzkCOaYc1L8IOBs4wdTGGbgNiG3Wz1I_1402312317'));
        expect($user->username)->equals('test_user');

        expect_not(User::findByPasswordResetToken('ExzkROaYc1L8IOBs4wdTGGbgNiG3Wz1I_1402312317'));
    }

//    public function testFindUserByAccessToken()
//    {
//        $model = new LoginForm([
//            'username' => 'test_user',
//            'password' => 'password_0',
//        ]);
//        $model->login();
//        $token  = User::findByUsername($model->username)->getJWT();
//        $output = new Output([]);
//        $output->debug(" I see \n $token \n token");
//
//        expect_that($user = User::findIdentityByAccessToken($token));
//        expect($user->username)->equals('test_user');
//
//        expect_not(User::findIdentityByAccessToken('non-existing'));
//    }

    public function testFindUserByUsername()
    {
        expect_that($user = User::findByUsername('test_user'));
        expect_not(User::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByUsername('test_user');
        expect_that($user->validateAuthKey('HP187Mvq7Mmm3CTU80dLkGmni_FUH_lR'));
        expect_not($user->validateAuthKey('test102key'));

        expect_that($user->validatePassword('password_0'));
        expect_not($user->validatePassword('123456'));
    }

}
