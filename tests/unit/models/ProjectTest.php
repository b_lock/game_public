<?php

namespace tests\unit\models;

use app\models\Project;
use app\models\User;
use app\tests\fixtures\ProjectFixture;
use app\tests\fixtures\UserFixture;
use Codeception\Lib\Console\Output;
use Yii;

class ProjectTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

//    private $model;

    public function _before()
    {
        $this->tester->haveFixtures([
            'project' => [
                'class' => ProjectFixture::className(),
            ]
        ]);
    }


    public function testCreateProject()
    {
        Yii::$app->user->login(User::findOne('1'));

        $model = new Project();
        expect_that($model->load([
            "Project" => [
                'project_name'        => 'test_project2',
                'project_description' => 'test_project2 description',
            ]
        ]));
        expect('check validation', $model->validate())->true();
        expect('check save project', $model->save())->true();
    }

    public function testCreateProjectWithNoLoginUser()
    {
        $model = new Project();
        expect_that($model->load([
            "Project" => [
                'project_name'        => 'test_project2',
                'project_description' => 'test_project2 description',
            ]
        ]));
        expect('check validation false', $model->validate())->false();
        expect('check save project false', $model->save())->false();
    }

    public function testGetUserProjects()
    {
        expect_that($project = Project::getUserProjects(1));
        expect($project)->notEmpty() ;
        $this->assertArrayHasKey('id',$project[0],'don\'t have a key "id" ');


    }
    public function testGetUserEmptyProjects()
    {
        expect($project = Project::getUserProjects(2))->isEmpty();
    }
}
