<?php

namespace tests\unit\models;

use app\models\LoginForm;
use app\models\User;
use Codeception\Lib\Console\Output;
use app\tests\fixtures\UserFixture;
use Yii;

class LoginTest extends \Codeception\Test\Unit
{
    private $model;

    /**
     * @var \UnitTester
     */
    protected $tester;


    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class'    => UserFixture::className(),
            ]
        ]);
    }

    public function testLoginNoUser()
    {
        $model = new LoginForm([
            'username' => 'not_existing_username',
            'password' => 'not_existing_password',
        ]);
        expect('model should not login user', $model->login())->false();
        expect('user should not be logged in', Yii::$app->user->isGuest)->true();
    }

    public function testLoginWrongPassword()
    {
        $model = new LoginForm([
            'username' => 'test_user',
            'password' => 'wrong_password',
        ]);
        expect('model should not login user', $model->login())->false();
        expect('error message should be set', $model->errors)->hasKey('password');
        expect('user should not be logged in', Yii::$app->user->isGuest)->true();
    }

    public function testLoginCorrect()
    {
        $model = new LoginForm([
            'username' => 'test_user',
            'password' => 'password_0',
        ]);
        expect('model should login user', $model->login())->true();
        expect('error message should not be set', $model->errors)->hasntKey('password');
        expect('user should be logged in', Yii::$app->user->isGuest)->false();
    }

    /**
     * @depends testLoginCorrect
     */
    public function testCheckToken()
    {
        $model = new LoginForm([
            'username' => 'test_user',
            'password' => 'password_0',
        ]);
        $model->login();
        $token  = User::findByUsername($model->username)->getJWT();
        $output = new Output([]);
        $output->debug(" I see \n $token \n token");
        expect('check not empty token', $token)->notEmpty();
    }


}
