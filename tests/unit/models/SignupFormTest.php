<?php
namespace tests\unit\models;

use app\tests\fixtures\UserFixture;
use app\models\SignupForm;

class SignupFormTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->tester->haveFixtures([
            'user' => [
                'class'    => UserFixture::className(),
            ]
        ]);
    }

    public function testCorrectSignup()
    {
        $model = new SignupForm([
            'username' => 'some_username',
            'email'    => 'some_email@example.com',
            'password' => 'some_password',
        ]);
        $user  = $model->signup();
        expect($user)->isInstanceOf('app\models\User');
        expect($user->username)->equals('some_username');
        expect($user->email)->equals('some_email@example.com');
        expect($user->validatePassword('some_password'))->true();
    }

    public function testNotCorrectSignup()
    {
        $model = new SignupForm([
            'username' => 'test_user',
            'email'    => 'test_user@mail.com',
            'password' => 'some_password',
        ]);
        expect_not($model->signup());
        expect_that($model->getErrors('username'));
        expect_that($model->getErrors('email'));
        expect($model->getFirstError('username'))
            ->equals('This username has already been taken.');
        expect($model->getFirstError('email'))
            ->equals('This email address has already been taken.');
    }
}