<?php

namespace tests\unit\models;

use app\models\Project;
use app\models\Task;
use app\models\User;
use app\tests\fixtures\TaskFixture;
use Codeception\Lib\Console\Output;
use Yii;

class TaskTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

//    private $model;

    public function _before()
    {
        $this->tester->haveFixtures([
            'task' => [
                'class' => TaskFixture::className(),
            ]
        ]);
    }

    public function testCreateTask()
    {
        Yii::$app->user->login(User::findOne('1'));

        $model = new Task();
        expect_that($model->load([
            "Task" => [
                'task_name'        => 'task test name',
                'project_id'       => '1',
                'task_dependence'  => '1',
                'task_description' => 'test task2 description',
            ]
        ]));
//        $model->validate();
//        $valid = print_r($model->errors,true);
//        $output = new Output([]);
//        $output->debug($valid);
        expect('check validation', $model->validate())->true();
        expect('check save project', $model->save())->true();

    }

    public function testCreateTaskWithNoLoginUser()
    {

        $model = new Task();
        expect_that($model->load([
            "Task" => [
                'task_name'        => 'task test name',
                'project_id'       => '1',
                'task_description' => 'test task2 description',
            ]
        ]));
//        $model->validate();
//        $valid = print_r($model->errors,true);
//        $output = new Output([]);
//        $output->debug($valid);
        expect('check validation', $model->validate())->false();
        expect('check save project', $model->save())->false();

    }

    public function testCreateTaskWithWrongProjectId()
    {
        Yii::$app->user->login(User::findOne('1'));

        $model = new Task();
        expect_that($model->load([
            "Task" => [
                'task_name'        => 'task test name',
                'project_id'       => '100',
                'task_description' => 'test task2 description',
            ]
        ]));
        expect('check validation', $model->validate())->false();
        expect('check save project', $model->save())->false();

    }

    public function testCreateTaskWithWrongTaskDependenceId()
    {
        Yii::$app->user->login(User::findOne('1'));

        $model = new Task();
        expect_that($model->load([
            "Task" => [
                'task_name'        => 'task test name',
                'project_id'       => '1',
                'task_dependence'  => '100',
                'task_description' => 'test task2 description',
            ]
        ]));
        expect('check validation', $model->validate())->false();
        expect('check save project', $model->save())->false();

    }

    public function testGetTasksWithWrongProjectId()
    {
        expect($task = Task::getTasks(100))->isEmpty();
    }

    public function testGetTasks()
    {
        expect_that($task = Task::getTasks(1));
        expect($task)->notEmpty();
        $this->assertArrayHasKey('id', $task[0], 'don\'t have a key "id" ');
    }

    public function testGetTaskWithWrongTaskId()
    {
        $tasks = Task::getUserTask(100, 1);
        expect($tasks)->isEmpty();
    }

    public function testGetTask()
    {
        expect_that($task = Task::getUserTask('1', '1'));
        expect($task)->notEmpty();
        expect($task)->contains('test task name');
        expect($task)->contains('test task description');
        $this->assertArrayHasKey('id', $task, 'don\'t have a key "id" ');
    }
}
