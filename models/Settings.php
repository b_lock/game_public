<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $support_email
 * @property integer $duel_time
 * @property string $tournament_time
 * @property integer $tournament_players_count
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['id', 'integer'],
            [['support_email'], 'required'],
            [['duel_time', 'tournament_time', 'tournament_players_count'], 'integer'],
            [['support_email'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'support_email' => 'Email техподдержки',
            'duel_time' => 'Время дуэли',
            'tournament_time' => 'Время турнира',
            'tournament_players_count' => 'Минимальное количество игроков для начала турнира',
        ];
    }
}
