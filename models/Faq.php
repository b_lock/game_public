<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "faq".
 *
 * @property integer $id
 * @property string $title
 * @property integer $parent_id
 * @property string $description
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Faq $parent
 * @property Faq[] $faqs
 */
class Faq extends \yii\db\ActiveRecord
{
    public $arr = [];

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faq';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Faq::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'parent_id' => 'Родительская категория',
            'description' => 'Описание',
            'status' => 'Статус',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Faq::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqs()
    {
        return $this->hasMany(Faq::className(), ['parent_id' => 'id']);
    }

    public static function getFaqArray()
    {
        $faqs = self::find()->asArray()->all();
        $result = ArrayHelper::index($faqs, 'id');
        $res = self::recursiveFaq($result);
//        echo '<pre>';
//        var_dump($res);
//        echo '</pre>';
        return $res;
    }

    private static function recursiveFaq($faqs, $parent_id = null)
    {
        $filter = array_filter($faqs, function ($faq) use ($parent_id) {
            return $faq['parent_id'] == $parent_id;
        });
        foreach ($filter as &$faq) {
            $child = self::recursiveFaq($faqs, $faq['id']);
            if ($child) {
                $faq['child'] = $child;
            }

        }
        return $filter;
    }

}
