<?php
namespace app\models;

use Firebase\JWT\JWT;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property string $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    use \msheng\JWT\UserTrait;

    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

//    public $loginUrl = ['backend/login'];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['role', 'string'],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

//    /**
//     * @inheritdoc
//     */
//    public static function findIdentityByAccessToken($token, $type = null)
//    {
//        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
//    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function decodeJWT($token)
    {
        try {
            $decoded = JWT::decode($token, Yii::$app->params['JWT_SECRET'], ['HS256']);
        } catch (\Exception $e) {
            return false;
        }
        return $decoded;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserInfo()
    {
        return $this->hasOne(UserInfo::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentsLog()
    {
        return $this->hasMany(PaymentsLog::className(), ['user_id' => 'id']);
    }


    public static function checkUsers()
    {
        $flag = false;
        /**
         * проверка дисконекта пользователя
         * анчекаем из игр и турнира если он там был чекнут.
         */


        /** @var UserInfo $user */
        $games = Game::find()->where(['status' => 0])->all();
        foreach ($games as $game) {
            /** @var Game $game */
            if ($game->user_1) {
                $user_1_info = UserInfo::findOne(['user_id' => $game->user_1]);
                if ($user_1_info && $user_1_info->last_ping == 0) {
                    $game->user_1 = null;
                    $flag = true;
                }
            }
            if ($game->user_2) {
                $user_2_info = UserInfo::findOne(['user_id' => $game->user_2]);
                if ($user_2_info && $user_2_info->last_ping == 0) {
                    $game->user_2 = null;
                    $flag = true;
                }
            }
            $game->update();
        }
        $tournament = Tournament::findOne(['status' => [0, 1]]);
        if ($tournament) {
            $players = unserialize($tournament->players);
            $keys = [];
            if ($players) {
                foreach ($players as $key => $player) {
                    $user_info = UserInfo::findOne(['user_id' => $player['id']]);
                    if ($user_info && $user_info->last_ping == 0) {
                        $keys[] = $key;
                    }
                }
                if (count($keys)) {
                    foreach ($keys as $key) {
                        unset($players[$key]);
                    }
                    $tournament->players = serialize($players);
                    $tournament->save();
                    $flag = true;
                }
            }

            if ($flag) {
                EventEmitter::refresh();
            }

//
//            $find_player_key = Tournament::findKeyPlayer($players, $user->user_id);
//            if ($find_player_key) {
//                unset($players[$find_player_key]);
//                $tournament->players = serialize($players);
//                $tournament->save();
//            }
        }
    }


    public static function getUserOnlineCount()
    {
        return (int)UserInfo::find()->where(['last_ping' => 1])->count();
    }

    public static function getUsersOnlineInfo()
    {

        $users = UserInfo::find()->where(['last_ping' => 1])->all();
        return self::getUsersInfo($users);

    }

    public static function getUserGamesInfo($userId)
    {
        $user = UserInfo::find()->where(['user_id' => $userId])->all();
        $res = self::getUsersInfo($user);
        return count($res) > 0 ? $res[0] : null;
    }

    public static function getUsersInfo($users)
    {
        $usersInfo = [];
        /** @var UserInfo $user */
        foreach ($users as $user) {
            $user_games = Game::find()
                ->where(['or', 'user_1=' . $user->user_id, 'user_2=' . $user->user_id])
                ->andWhere(['status' => 10])->all();
            $user_win_count = 0;
            $user_games_count = count($user_games);;

            /** @var Game $user_game */
            foreach ($user_games as $user_game) {
                if ($user_game->winner === $user->user_id) {
                    $user_win_count = $user_win_count + 1;
                }
            }
//            return $user_games;
            $usersInfo[] = [
                'id' => $user->user_id,
                'userName' => $user->user->username,
                'avatar' => $user->avatar,
                'winPoints' => $user->win_points,
                'losingPoints' => $user->losing_points,
                'gamesCount' => $user_games_count,
                'gamesWinCount' => $user_win_count
            ];
        }
        return $usersInfo;
    }

    public static function getUsersTop($req)
    {
        $usersInfo = [];
        if ($req === 'all') {
//            $users = UserInfo::find()->orderBy(['win_points' => SORT_DESC])->limit(10);
//            $users = $users->all();
//            /** @var UserInfo $user */
//            foreach ($users as $user) {
//                $user_games = Game::find()
//                    ->where(['or', 'user_1=' . $user->user_id, 'user_2=' . $user->user_id])
//                    ->andWhere(['status' => 10])->all();
//                $user_win_count = 0;
//                $user_games_count = count($user_games);;
//
//                /** @var Game $user_game */
//                foreach ($user_games as $user_game) {
//                    if ($user_game->winner === $user->user_id) {
//                        $user_win_count = $user_win_count + 1;
//                    }
//                }
////            return $user_games;
//                $usersInfo[] = [
//                    'id' => $user->user_id,
//                    'userName' => $user->user->username,
//                    'avatar' => $user->avatar,
//                    'winPoints' => $user->win_points,
//                    'losingPoints' => $user->losing_points,
//                    'gamesCount' => $user_games_count,
//                    'gamesWinCount' => $user_win_count
//                ];
//            }
            $games = Game::find()->where(['status' => 10])->all();
            $usersInfo = self::usersInfo($games);
        } elseif ($req === 'day') {
            $last_day = time() - (60 * 60 * 24);
            $games = Game::find()->where(['status' => 10])->andWhere('updated_at >=' . $last_day)->all();
            $usersInfo = self::usersInfo($games);

        } elseif ($req === 'week') {
            $last_day = time() - (60 * 60 * 24 * 7);
            $games = Game::find()->where(['status' => 10])->andWhere('updated_at >=' . $last_day)->all();
            $usersInfo = self::usersInfo($games);
        } elseif ($req === 'month') {
            $last_day = time() - (60 * 60 * 24 * 31);
            $games = Game::find()->where(['status' => 10])->andWhere('updated_at >=' . $last_day)->all();
            $usersInfo = self::usersInfo($games);
        }

        return $usersInfo;
    }

    private static function usersInfo($games)
    {
        $game_users = [];
        $usersInfo = [];
        $game_users_info = [];
        $tournament_user_info = [];

        /** @var Game $game */
        foreach ($games as $game) {
            $game_users[$game->user_1] = $game->user_1;
            $game_users[$game->user_2] = $game->user_2;
            if (!isset($game_users_info[$game->user_1])) {
                $game_users_info[$game->user_1] = [
                    'games' => 0,
                    'wins' => 0,
                    'loose' => 0,
                    'winner_points' => 0,
                    'loose_points' => 0
                ];
            }
            if (!isset($game_users_info[$game->user_2])) {
                $game_users_info[$game->user_2] = [
                    'games' => 0,
                    'wins' => 0,
                    'loose' => 0,
                    'winner_points' => 0,
                    'loose_points' => 0
                ];
            }

            if ($game->tournament_id !== null && !isset($tournament_user_info[$game->tournament_id])) {
                $tournament_user_info[$game->tournament_id] = 0;
            }

            /**
             * Считаем очки без турниров
             * очки выиграша без турниров
             */
            if ($game->winner === $game->user_1) {

                $game_users_info[$game->user_1]['games'] = $game_users_info[$game->user_1]['games'] + 1;

                if (!$game->tournament_id) {
                    $game_users_info[$game->user_1]['winner_points'] = $game_users_info[$game->user_1]['winner_points'] + $game->win_points;
                } else {
                    $tournament_user_info[$game->tournament_id] = $tournament_user_info[$game->tournament_id] + $game->win_points;
                }

                $game_users_info[$game->user_1]['wins'] = $game_users_info[$game->user_1]['wins'] + 1;

                $game_users_info[$game->user_2]['games'] = $game_users_info[$game->user_2]['games'] + 1;
                $game_users_info[$game->user_2]['loose_points'] = $game_users_info[$game->user_2]['loose_points'] + $game->win_points;
                $game_users_info[$game->user_2]['loose'] = $game_users_info[$game->user_2]['loose'] + 1;

            } elseif ($game->winner === $game->user_2) {

                $game_users_info[$game->user_2]['games'] = $game_users_info[$game->user_2]['games'] + 1;

                if (!$game->tournament_id) {
                    $game_users_info[$game->user_2]['winner_points'] = $game_users_info[$game->user_2]['winner_points'] + $game->win_points;
                } else {
                    $tournament_user_info[$game->tournament_id] = $tournament_user_info[$game->tournament_id] + $game->win_points;
                }

                $game_users_info[$game->user_2]['wins'] = $game_users_info[$game->user_2]['wins'] + 1;

                $game_users_info[$game->user_1]['games'] = $game_users_info[$game->user_1]['games'] + 1;
                $game_users_info[$game->user_1]['loose_points'] = $game_users_info[$game->user_1]['loose_points'] + $game->win_points;
                $game_users_info[$game->user_1]['loose'] = $game_users_info[$game->user_1]['loose'] + 1;

            } elseif ($game->winner === -1) {

                $game_users_info[$game->user_1]['games'] = $game_users_info[$game->user_1]['games'] + 1;
                $game_users_info[$game->user_1]['loose_points'] = $game_users_info[$game->user_1]['loose_points'] + $game->win_points;
                $game_users_info[$game->user_1]['loose'] = $game_users_info[$game->user_1]['loose'] + 1;

                $game_users_info[$game->user_2]['games'] = $game_users_info[$game->user_2]['games'] + 1;
                $game_users_info[$game->user_2]['loose_points'] = $game_users_info[$game->user_2]['loose_points'] + $game->win_points;
                $game_users_info[$game->user_2]['loose'] = $game_users_info[$game->user_2]['loose'] + 1;

                $tournament_user_info[$game->tournament_id] = $tournament_user_info[$game->tournament_id] + ($game->win_points * 2);

            }
        }

        $tournaments = Tournament::find()->where(['id' => array_keys($tournament_user_info), 'status' => 10])->all();

        /** @var Tournament $tournament */
        foreach ($tournaments as $tournament) {
            $players = unserialize($tournament->players);
            if ($players && count($players) > 0) {
                $player = array_shift($players);
                if (isset($game_users_info[$player['id']])) {
                    $game_users_info[$player['id']]['winner_points'] = $game_users_info[$player['id']]['winner_points'] + $tournament_user_info[$tournament->id];
                }
            }
        }


        $users = UserInfo::find()->with('user')->where(['user_id' => $game_users])->all();

        /** @var UserInfo $user */
        foreach ($users as $user) {
            $usersInfo[] = [
                'userId' => $user->user_id,
                'userName' => $user->user->username,
                'avatar' => $user->avatar,
                'winPoints' => $game_users_info[$user->user_id]['winner_points'],
                'losingPoints' => $game_users_info[$user->user_id]['loose_points'],
                'gamesCount' => $game_users_info[$user->user_id]['games'],
                'gamesWinCount' => $game_users_info[$user->user_id]['wins'],
                'gamesInfo' => $game_users_info[$user->user_id]
            ];
        }

        usort($usersInfo, function ($u1, $u2) {
            return $u1['gamesInfo']['winner_points'] < $u2['gamesInfo']['winner_points'];
        });
        return $usersInfo;
    }

    public static function exit()
    {
        $user_id = Yii::$app->user->id;
        Game::GameExit($user_id);
        Tournament::ToggleUserPlace(null);
    }

}
