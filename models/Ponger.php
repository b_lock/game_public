<?php

namespace app\models;

use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\helpers\ArrayHelper;
use app\models\User;

class Ponger
{
    public static function getPonger($id = null)
    {

        if (!Yii::$app->request->isConsoleRequest) {
//            $user = new User();
//            $authHeader = Yii::$app->request->getHeaders()->get('Authorization');
//            if ($authHeader !== null && preg_match('/^Bearer\s+(.*?)$/', $authHeader, $matches)) {
//                $identity = $user->findIdentityByAccessToken($matches[1]);
////            $identity = $user-> loginByAccessToken($matches[1]);
//                if ($identity) {
//                    Yii::$app->user->login($identity);
//                }
//            }

            if (!Yii::$app->user->isGuest) {
                UserInfo::setLastPing();
            }
        }


        $shmod = SharedMemory::getMem();

        if ($shmod['time'] < (time() - 10)) {
            Game::checkGames();
            Tournament::checkTournament();
            User::checkUsers();
            SharedMemory::setTimeMem();
        }

        if ($id !== $shmod['id']) {

            return [
                'status' => 'updated',
                'data' => [
                    'types' => ArrayHelper::index(GameType::find()->select([
                        'id',
                        'name',
                        'description',
                        'tournament',
                        'rate'
                    ])->where(['status' => 1])->asArray()->all(), 'id'),
                    'tournament' => self::getTournament(),
                    'duels' => self::getDuels(),
                    'logs' => Game::getLogs(),
                    'usersOnline' => User::getUserOnlineCount(),
                    'shmod' => $shmod['id']
                ]
            ];
        }

        return ['status' => false,];

    }

    public static function getTournament()
    {
        $tournament = Tournament::findOne(['status' => [0, 1, 2]]);
        if (is_null($tournament)) {
            $tournament = new Tournament();
            $tournament->save();
        }
        $players = unserialize($tournament->players);

        if ($tournament->status == 2) {
            $duels = Game::findAll(['tournament_id' => $tournament->id]);

        }

        return [
            'id' => $tournament->id,
            'status' => $tournament->status,
            'players' => $players ? $players : []
        ];
    }

    public static function getDuels()
    {
        $list_games = [];
        $game_types = GameType::findAll(['status' => 1]);
        foreach ($game_types as $type) {
            $game = Game::find()->where(['id_type' => $type->id, 'status' => [0, 1]])->with(['user1', 'user2'])->one();
            if (!$game && $type->tournament != 1) {
                $game = new Game();
                $game->id_type = $type->id;
                $game->status = 0;
                $game->save();
            } elseif (!$game && $type->tournament == 1) {
                continue;
            }
            $list_games[] = [
                'id' => $game->id,
                'type' => $game->id_type,
                'status' => $game->status,
                'user1' => $game->user_1 ? self::getUserInfo($game->user1) : null,
                'user2' => $game->user_2 ? self::getUserInfo($game->user2) : null,
                'user1Choice' => $game->winner !== 0 ? $game->user_1_choice : null,
                'user2Choice' => $game->winner !== 0 ? $game->user_2_choice : null,
                'winner' => $game->winner,
                'tournament' => $game->tournament_id,
            ];
        }

        return $list_games;
    }

    private static function getUserInfo($user)
    {
//        $user_info = UserInfo::findOne(['user_id' => $user->id]);
        return $user ? [
            'id' => $user->id,
            'userName' => $user->username,
            'avatar' => $user->userInfo->avatar
        ] : null;
    }
}