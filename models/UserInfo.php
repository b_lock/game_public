<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "user_info".
 *
 * @property integer $id
 * @property integer $user_id
 * @property float $balance
 * @property integer $win_points
 * @property integer $losing_points
 * @property string $avatar
 * @property string $notes
 * @property integer $last_ping
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class UserInfo extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_info';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'last_ping', 'created_at', 'updated_at','win_points','losing_points'], 'integer'],
            [['balance'], 'number'],
            [['avatar', 'notes'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'balance' => 'Баланс',
            'win_points' => 'Выинранные очки',
            'losing_points' => 'проигранные очки',
            'avatar' => 'Avatar',
            'notes' => 'Notes',
            'last_ping' => 'last ping',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function setLastPing()
    {
        $user = self::findOne(['user_id' => Yii::$app->user->id]);
        if (!$user) {
            $user = new UserInfo();
            $user->user_id = Yii::$app->user->id;
        }
        $user->last_ping = 1;
        $user->save();
    }

}
