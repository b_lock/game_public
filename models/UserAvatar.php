<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UserAvatar extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $avatar;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['avatar'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif','maxSize' => 1024*1024*2],
        ];
    }
}