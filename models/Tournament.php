<?php

namespace app\models;

use phpDocumentor\Reflection\Types\Self_;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "tournament".
 *
 * @property integer $id
 * @property integer $status
 * @property string $players
 * @property integer $start_time
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Game[] $games
 */
class Tournament extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tournament';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'start_time'], 'integer'],
            [['created_at', 'updated_at'], 'integer'],
            [['players'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Status',
            'players' => 'Players',
            '$start_time' => 'Start tournament',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::className(), ['tournament_id' => 'id']);
    }

    public static function ToggleUserPlace($place)
    {
        $tournament = self::findOne(['status' => [0, 1]]);
        if ($tournament && Yii::$app->user->identity->userInfo->balance >= GameType::findOne(['tournament' => 1])->rate) {
            $players = unserialize($tournament->players);
            $user_avatar = UserInfo::findOne(['user_id' => Yii::$app->user->id])->avatar;
            if ($players) {

                $find_player_key = self::findKeyPlayer($players, Yii::$app->user->id);

                if (!$players[$place]) {
                    $players[$place] = [
                        'id' => Yii::$app->user->id,
                        'avatar' => $user_avatar ? $user_avatar : ""
                    ];
                    if ($find_player_key) {
                        unset($players[$find_player_key]);
                    }
                } elseif ($find_player_key && $find_player_key == $place) {
                    unset($players[$place]);
                } else {
                    return true;
                }
            } else {
                $players = [];
                $players[$place] = [
                    'id' => Yii::$app->user->id,
                    'avatar' => $user_avatar ? $user_avatar : ""
                ];
            }
            $tournament->players = serialize($players);
            if ($tournament->save()) {
                return true;
            };

        }
        return false;
    }

    public static function checkTournament()
    {
        $flag = false;
        /** @var integer $count_players_width_start
         */
        $count_players_width_start = Yii::$app->params['settings']['tournament_players_count'];

        /** @var integer $pause_start //
         */
        $pause_start = Yii::$app->params['settings']['tournament_time'];

        $dateNow = time();

        $tournament = self::findOne(['status' => [0, 1, 2]]);
        $players = unserialize($tournament->players);
        if (!$players) {
            return;
        } elseif ($tournament->status == 0 && count($players) >= $count_players_width_start) {
            $tournament->status = 1;
            $tournament->start_time = $dateNow + $pause_start;
            $tournament->save();
            $flag = true;
        } elseif ($tournament->status == 1 && $tournament->start_time <= $dateNow - 3) {
            /** 3 sec delay*/
            $tournament->status = 2;
            if ($tournament->save()) {
                $flag = true;
                self::shufflePlayers($players, $tournament->id);
            }
        } elseif ($tournament->status == 2) {
            $games = Game::findAll(['tournament_id' => $tournament->id]);
            if ($games) {
                $check_finish = true;
                foreach ($games as $game) {
                    /**
                     *Если игра не завершена сбрасываем флаг
                     */
                    if ($game->status !== 10) {
                        $check_finish = false;
                        /**
                         * Если игра завершена
                         * */
                    } elseif ($game->status === 10) {

                        /**
                         * Если никто оба игрока проиграли
                         * удаляем из списка игроков проигравших
                         */
                        if ($game->winner == -1) {
                            $find_player1_key = self::findKeyPlayer($players, $game->user_1);
                            if ($find_player1_key) {
                                unset($players[$find_player1_key]);
                                $tournament->players = serialize($players);
                                $tournament->save();
                                $flag = true;
                            }
                            $find_player2_key = self::findKeyPlayer($players, $game->user_2);
                            if ($find_player2_key) {
                                unset($players[$find_player2_key]);
                                $tournament->players = serialize($players);
                                $tournament->save();
                                $flag = true;
                            }

                            /**
                             * Если один игрок выиграл удаляем из списка игроков проигравшего
                             */
                        } elseif ($game->winner > 0) {
                            $loose = $game->winner == $game->user_1 ? $game->user_2 : $game->user_1;
                            $find_player_key = self::findKeyPlayer($players, $loose);
                            if ($find_player_key) {
                                unset($players[$find_player_key]);
                                $tournament->players = serialize($players);
                                $tournament->save();
                                $flag = true;
                            }
                        }
                    }
                }
                /**
                 * Если нет активных игр
                 */
                if ($check_finish) {
                    /**
                     * Если оставшихся игроков больше 1 перетосовываем их
                     */
                    if (count($players) > 1) {
                        self::shufflePlayers($players, $tournament->id);
                        $flag = true;

                        /**
                         * Если осталось меньше 2 игроков тогда завершаем турнир
                         */
                    } else {
                        $tournament->status = 10;
                        $tournament->save();

                        $game_types = Yii::$app->params['game_types'];

                        $filt = array_filter(
                            $game_types, function ($type) {

                            /** @var GameType $type */
                            return $type->tournament == 1;
                        });

                        $games_tournament = $tournament->games;

                        $win_points = 0;

                        /** @var Game $game */
                        foreach ($games_tournament as $game) {
                            $win_points = $win_points + $game->win_points;
                        }

                        $winner = array_shift($players);
                        if ($winner) {
                            $type = array_shift($filt);
                            $addPoints = new AddPointsForm();
                            $addPoints->user_id = $winner['id'];
                            $addPoints->points = $win_points;
                            $addPoints->type = "Tournament_winner";
                            $addPoints->notes = "Tournament_id: $tournament->id";

                            if (!$addPoints->save()) {
                                $err = print_r([$addPoints->errors, $type], true);
                                throw new \ErrorException("Tournament_winner not save: \n" . $err, 500);
                            }
                        }
                        $flag = true;
                    }
                }
            }
        }
        if ($flag) {
            EventEmitter::refresh();
        }
    }

    public function shufflePlayers($players, $tournament_id)
    {
        $rand_players = $players;
        shuffle($rand_players);
        $rounds = floor(count($rand_players) / 2);
        for ($i = 0; $i < $rounds; $i++) {
            $game = new Game();
            $game->id_type = 5;
            $game->status = 1;
            $game->user_1 = $rand_players[$i * 2]['id'];
            $game->user_2 = $rand_players[($i * 2) + 1]['id'];
            $game->tournament_id = $tournament_id;
            $game->save();
        }
    }

    public static function findKeyPlayer(array $players, int $player_id)
    {
        $find_player_key = null;
        array_filter($players, function ($a, $key) use (&$find_player_key, &$player_id) {
            if ($a["id"] == $player_id) {
                $find_player_key = $key;
                return true;
            }
            return false;
        }, ARRAY_FILTER_USE_BOTH);

        return $find_player_key;
    }

    public static function TournamentExit($user_id)
    {

    }
}
