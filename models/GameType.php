<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "game_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $rate
 * @property integer $status
 * @property string $params
 * @property string $tournament
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Game[] $games
 */
class GameType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game_type';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description', 'params'], 'string'],
            [['rate', 'status', 'created_at', 'updated_at', 'tournament'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'name'        => 'Name',
            'description' => 'Description',
            'rate'        => 'Rate',
            'status'      => 'Status',
            'params'      => 'Params',
            'tournament'  => 'Tournament',
            'created_at'  => 'Created At',
            'updated_at'  => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGames()
    {
        return $this->hasMany(Game::className(), ['id_type' => 'id']);
    }
}
