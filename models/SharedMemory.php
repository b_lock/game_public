<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 24.02.2017
 * Time: 16:51
 */

namespace app\models;


class SharedMemory
{
    public static function getMem()
    {
        $shmid = shmop_open(0xff3, "c", 0644, 100);
        $shmtime = shmop_open(0xff4, "c", 0644, 100);
        $my_string = null;
        $time = null;

        if ($shmid) {
            $shm_size = shmop_size($shmid);
            $my_string = shmop_read($shmid, 0, $shm_size);
//            echo "my_string: " . $my_string;
//            shmop_write($shmid, time(), 0);
            $my_string = trim($my_string);
            if (!$my_string || $my_string == null) {
                $id = time();
                shmop_write($shmid, $id, 0);
                $my_string = $id;
            }
            shmop_close($shmid);

        }
        if ($shmtime) {
            $shm_size = shmop_size($shmtime);
            $time = shmop_read($shmtime, 0, $shm_size);
//            echo "my_string: " . $my_string;
//            shmop_write($shmid, time(), 0);
            $time = trim($time);
            if (!$time) {
                $id = time();
                shmop_write($shmtime, $id, 0);
                $time = $id;
            }
            shmop_close($shmtime);
        }
        return [
            'id' => $my_string,
            'time' => $time
        ];
    }

    public static function setTimeMem()
    {
        $shmtime = shmop_open(0xff4, "c", 0644, 100);
        shmop_write($shmtime, time(), 0);
        shmop_close($shmtime);
    }

    public static function setIdMem()
    {
        $shmid = shmop_open(0xff3, "c", 0644, 100);
        $shm_size = shmop_size($shmid);
        $my_string = shmop_read($shmid, 0, $shm_size);
        shmop_write($shmid, $my_string + 1, 0);
        shmop_close($shmid);
    }

}