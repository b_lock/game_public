<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PaymentsLog;

/**
 * PaymentsLogSearch represents the model behind the search form of `app\models\PaymentsLog`.
 */
class PaymentsLogSearch extends PaymentsLog
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'points', 'created_at', 'updated_at'], 'integer'],
            [['tag', 'notes', 'username', 'user_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PaymentsLog::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
            'attributes' => [
                'id',
                'user_id',
                'points',
                'tag',
                'notes',
                'created_at',
                'updated_at',
                'user_id' => [
                    'asc' => ['user.username' => SORT_ASC],
                    'desc' => ['user.username' => SORT_DESC],
//                    'default' => SORT_ASC
                ]
            ]
        ]);
        $query->joinWith(['user']);

        // grid filtering conditions
        $query->andFilterWhere([
            'payments_log.id' => $this->id,
//            'user_id' => $this->user_id,
            'points' => $this->points,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'tag', $this->tag])
            ->andFilterWhere(['like', 'user.username', $this->user_id])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        return $dataProvider;
    }
}
