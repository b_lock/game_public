<?php

namespace app\models;

use Codeception\Extension\Logger;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

/**
 * This is the model class for table "game".
 *
 * @property integer $id
 * @property integer $id_type
 * @property integer $status
 * @property integer $user_1
 * @property integer $user_2
 * @property string $user_1_choice
 * @property string $user_2_choice
 * @property integer $winner
 * @property integer $win_points
 * @property integer $tournament_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property GameType $type
 * @property Tournament $tournament
 * @property User $user1
 * @property User $user2
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'game';
    }


    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_type'], 'required'],
            [
                ['id_type', 'status', 'user_1', 'user_2', 'winner', 'win_points', 'tournament_id', 'created_at', 'updated_at'],
                'integer'
            ],
            [['user_1_choice', 'user_2_choice'], 'string', 'max' => 255],
            [
                ['id_type'],
                'exist',
                'skipOnError' => true,
                'targetClass' => GameType::className(),
                'targetAttribute' => ['id_type' => 'id']
            ],
            [
                ['tournament_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Tournament::className(),
                'targetAttribute' => ['tournament_id' => 'id']
            ],
            [
                ['user_1'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_1' => 'id']
            ],
            [
                ['user_2'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['user_2' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_type' => 'Id Type',
            'status' => 'Status',
            'user_1' => 'User 1',
            'user_2' => 'User 2',
            'user_1_choice' => 'User 1 Choice',
            'user_2_choice' => 'User 2 Choice',
            'winner' => 'Winner',
            'win_points' => 'winner points',
            'tournament_id' => 'Tournament ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(GameType::className(), ['id' => 'id_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTournament()
    {
        return $this->hasOne(Tournament::className(), ['id' => 'tournament_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser1()
    {
        return $this->hasOne(User::className(), ['id' => 'user_1']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser2()
    {
        return $this->hasOne(User::className(), ['id' => 'user_2']);
    }


    public static function takePlace($duelId, $side)
    {
        $cond = [
            'id' => $duelId,
            'status' => [0, 1],
        ];
        if ($side == 1) {
            $user = 'user_1';
            $userTwo = 'user_2';
        } else {
            $user = 'user_2';
            $userTwo = 'user_1';
        }
        $cond[$user] = null;
        $game = self::findOne($cond);
//        $print =  print_r($game,true);
//        Yii::info($print);
        if ($game[$userTwo] != Yii::$app->user->id && Yii::$app->user->identity->userInfo->balance >= GameType::findOne(['id' => $game->id_type])->rate) {
            $game[$user] = Yii::$app->user->id;
            if ($game->user_1 !== null && $game->user_2 !== null) {
                $game->status = 1;
            }
            return $game->save();
        }
        return false;
    }

    public static function takeOffPlace($duelId, $side)
    {
        $cond = [
            'id' => $duelId,
            'status' => 0,
        ];
        if ($side == 1) {
            $user = 'user_1';
        } else {
            $user = 'user_2';
        }
        $cond[$user] = Yii::$app->user->id;
        $game = self::findOne($cond);
        if ($game) {
            $game['user_' . (integer)$side] = null;
            return $game->save();
        }
        return false;
    }

    public static function SetUserChoice($data)
    {
        $game = self::findOne(['id' => (int)$data->duelId, 'status' => 1]); //$data->duelId
        if ($game) {
            if ($game->user_1_choice && $game->user_2_choice && ($game->winner > 0 || $game->winner === -1)) {
                return [
                    'duelId' => $game->id,
                    'winner' => $game->winner
                ];
            }
            if (($game->user_1_choice === null || $game->user_2_choice === null)
                && $game->winner === null
                && ($data->userChoice === 'stone' || $data->userChoice === 'scissors' || $data->userChoice === 'paper')
            ) {
                if ($game->user_1 === Yii::$app->user->id) {
                    $game->user_1_choice = $data->userChoice;
                } elseif ($game->user_2 === Yii::$app->user->id) {
                    $game->user_2_choice = $data->userChoice;
                }

                if ($game->user_1_choice && $game->user_2_choice) {
                    $winner = self::getWinner($game->user_1_choice, $game->user_2_choice);
                    if ($winner === 0) {
                        $game->user_1_choice = null;
                        $game->user_2_choice = null;
                        $game->save();
                        return [
                            'duelId' => $game->id,
                            'winner' => 0
                        ];
                    } else {
                        $game->winner = $winner === 1 ? $game->user_1 : $game->user_2;
                        $game->save();
                        return [
                            'duelId' => $game->id,
                            'winner' => $game->winner
                        ];
                    }
                }
                $game->save();

                $time = time();
                while (true) {
                    if (($time + 10) <= time()) {
                        return [
                            'duelId' => $game->id,
                            'winner' => null
                        ];
                    }
                    sleep(1);
                    $game = self::findOne(['id' => (int)$data->duelId, 'status' => 1]); //$data->duelId
                    Yii::info(print_r(ArrayHelper::toArray($game), true));
                    if ($game->user_1_choice && $game->user_2_choice) {
                        $winner = self::getWinner($game->user_1_choice, $game->user_2_choice);
                        if ($winner === 0) {
                            $game->user_1_choice = null;
                            $game->user_2_choice = null;
                            $game->save();
                            return [
                                'duelId' => $game->id,
                                'winner' => 0
                            ];
                        } else {
                            $game->winner = $winner === 1 ? $game->user_1 : $game->user_2;
                            $game->save();
                            return [
                                'duelId' => $game->id,
                                'winner' => $game->winner
                            ];
                        }
                    } elseif ($game->user_1_choice === null && $game->user_2_choice === null) {
                        return [
                            'duelId' => $game->id,
                            'winner' => 0
                        ];
                    }
                }
            }

            /*----------------------------------------------------------*/
//            /**
//             * Если из предыдущего состояние очищены выборы пользователя,
//             * тогда это переигровка и мы возвращаем переигровку и очищаем winner
//             * */
//            if ($game->winner === 0 && $game->user_1_choice === null && $game->user_2_choice === null) {
//                $game->winner = null;
//                $game->save();
//                return [
//                    'duelId' => $game->id,
//                    'winner' => 0
//                ];
//            }
//
//            if ($game->winner > 0 || $game->winner == -1) {
//                return [
//                    'duelId' => $game->id,
//                    'winner' => $game->winner
//                ];
//            }
//
//
//            /** @var array of GameType $game_types */
//            $game_types = Yii::$app->params['game_types'];
//            if ($game->user_1 && $game->user_1 == Yii::$app->user->id) {
//                if ($data->userChoice === 'stone' || $data->userChoice === 'scissors' || $data->userChoice === 'paper') {
//                    $game->user_1_choice = $data->userChoice;
//                } else {
//                    $game->user_1_choice = "fail";
//                }
//
//                if ($game->user_2_choice && $game->user_2_choice != "fail" && $data->userChoice) {
//                    $res = self::getWinner($data->userChoice, $game->user_2_choice);
//                    if ($res == 0) {
//                        /**
//                         * если совпали выборы пользователей тогда очищаем выборы и устанавливаем winner в 0
//                         * и возвращаем переигровку
//                         */
//                        $game->winner = 0;
//                        $game->user_1_choice = null;
//                        $game->user_2_choice = null;
//                    } elseif ($res == 1) {
//                        $game->winner = $game->user_1;
//                        $game->win_points = $game_types[$game->id_type]->rate;
//                    } elseif ($res == 2) {
//                        $game->winner = $game->user_2;
//                        $game->win_points = $game_types[$game->id_type]->rate;
//                    }
//                } elseif ($game->user_2_choice && $game->user_2_choice != "fail") {
//                    $game->winner = $game->user_2;
//                    $game->win_points = $game_types[$game->id_type]->rate;
//                } elseif ($game->user_2_choice == "fail") {
//                    $game->winner = -1;
//                    $game->win_points = $game_types[$game->id_type]->rate;
//                }
//                /*if ($game->winner !== 0 && $game->winner !== null && $game->tournament_id !== null) {
//                    $game->status = 10;
//                }*/
//                $game->save();
//            } elseif ($game->user_2 && $game->user_2 == Yii::$app->user->id) {
//                if ($data->userChoice === 'stone' || $data->userChoice === 'scissors' || $data->userChoice === 'paper') {
//                    $game->user_2_choice = $data->userChoice;
//                } else {
//                    $game->user_2_choice = "fail";
//                }
//
//                if ($game->user_1_choice && $game->user_1_choice != "fail" && $data->userChoice) {
//                    $res = self::getWinner($data->userChoice, $game->user_1_choice);
//                    if ($res == 0) {
//                        /**
//                         * если совпали выборы пользователей тогда очищаем выборы и устанавливаем winner в 0
//                         * и возвращаем переигровку
//                         */
//                        $game->winner = 0;
//                        $game->user_1_choice = null;
//                        $game->user_2_choice = null;
//                    } elseif ($res == 1) {
//                        $game->winner = $game->user_2;
//                        $game->win_points = $game_types[$game->id_type]->rate;
//                    } elseif ($res == 2) {
//                        $game->winner = $game->user_1;
//                        $game->win_points = $game_types[$game->id_type]->rate;
//                    }
//                } elseif ($game->user_1_choice && $game->user_1_choice != "fail") {
//                    $game->winner = $game->user_1;
//                    $game->win_points = $game_types[$game->id_type]->rate;
//                } elseif ($game->user_1_choice == "fail") {
//                    $game->winner = -1;
//                    $game->win_points = $game_types[$game->id_type]->rate;
//                }
//                /* if ($game->winner !== 0 && $game->winner !== null && $game->tournament_id !== null) {
//                     $game->status = 10;
//                 }*/
//                $game->save();
//            }
//            if ($game->winner > 0) {
//
//                if (!$game->tournament_id) {
//                    $addPoints = new AddPointsForm();
//                    $addPoints->user_id = $game->winner;
//                    $addPoints->points = $game->win_points;
//                    $addPoints->type = "duel_winner";
//                    $addPoints->notes = "Duel_id: $game->id, Type: $game->id_type";
//                    $addPoints->save();
//                }
//
//                $addPoints = new AddPointsForm();
//                $addPoints->user_id = $game->user_1 == $game->winner ? $game->user_2 : $game->user_1;
//                $addPoints->points = -$game->win_points;
//                if ($game->tournament_id) {
//                    $addPoints->type = "Tournament_loose";
//                    $addPoints->notes = "Tournament_id: $game->tournament_id";
//                } else {
//                    $addPoints->type = "duel_loose";
//                    $addPoints->notes = "Duel_id: $game->id, Type: $game->id_type";
//                }
//
//
//                $addPoints->save();
//            } elseif ($game->winner == -1) {
//                $addPoints = new AddPointsForm();
//                $addPoints->user_id = $game->user_1;
//                $addPoints->points = -$game->win_points;
//                if ($game->tournament_id) {
//                    $addPoints->type = "Tournament_loose";
//                    $addPoints->notes = "Tournament_id: $game->tournament_id";
//                } else {
//                    $addPoints->type = "duel_loose";
//                    $addPoints->notes = "Duel_id: $game->id, Type: $game->id_type";
//                }
//                $addPoints->save();
//
//                $addPoints = new AddPointsForm();
//                $addPoints->user_id = $game->user_2;
//                $addPoints->points = -$game->win_points;
//                if ($game->tournament_id) {
//                    $addPoints->type = "Tournament_loose";
//                    $addPoints->notes = "Tournament_id: $game->tournament_id";
//                } else {
//                    $addPoints->type = "duel_loose";
//                    $addPoints->notes = "Duel_id: $game->id, Type: $game->id_type";
//                }
//                $addPoints->save();
//            }
//
//            $resGame = [
//                'duelId' => $game->id,
//                'winner' => $game->winner
//            ];
//            return $resGame;
        }
        return false;
    }

    public static function getWinner($choice_1, $choice_2)
    {
        switch ($choice_1) {
            case $choice_1 == 'stone':
                if ($choice_2 == 'stone') {
                    return 0;
                } elseif ($choice_2 == 'scissors') {
                    return 1;
                }
                return 2;//paper
                break;
            case $choice_1 == 'scissors':
                if ($choice_2 == 'stone') {
                    return 2;
                } elseif ($choice_2 == 'scissors') {
                    return 0;
                }
                return 1;//paper
                break;
            case $choice_1 == 'paper':
                if ($choice_2 == 'stone') {
                    return 1;
                } elseif ($choice_2 == 'scissors') {
                    return 2;
                }
                return 0;//paper
                break;
            default:
                return 0;
        }
    }

    public static function newGame($duelId)
    {
        $duel = Game::findOne(['id' => $duelId, 'status' => 1]);
        $dateNow = time();
        if ($dateNow <= ($duel->updated_at + 5)) {
            $duel->status = 10;
            $duel->save();
        }
    }


    public static function checkGames()
    {
        $flag = false;
        $dateNow = time();
        $duels = Game::find()->where('updated_at <= ' . ($dateNow - 20) . " AND status = 1 AND (winner <> 0 OR winner IS NULL)")->all();
        foreach ($duels as $duel) {
            /** @var Game $duel */
            if ($duel->winner == null) {
                if ($duel->user_1_choice !== null && $duel->user_1_choice !== 'fail') {
                    $duel->winner = ($duel->user_2_choice == null || $duel->user_2_choice == 'fail') ?
                        $duel->user_1 :
                        self::getWinner($duel->user_1_choice, $duel->user_2_choice);
                } else {
                    $duel->winner = ($duel->user_2_choice == null || $duel->user_2_choice == 'fail') ? -1 : $duel->user_2;
                }
            }
            $duel->win_points = Yii::$app->params['game_types'][$duel->id_type]->rate;
            $duel->status = 10;
            $duel->update();
            $flag = true;
        }
        $duels = Game::find()->where('updated_at <= ' . ($dateNow - 60) . " AND status = 1")->all();
        foreach ($duels as $duel) {
            $duel->status = 10;
            $duel->win_points = Yii::$app->params['game_types'][$duel->id_type]->rate;
            $duel->update();
            $flag = true;
        }
        if ($flag) {
            EventEmitter::refresh();
        }
    }


    public static function getLogs()
    {
        $logs = [];
        $types = GameType::find()->select('id')->where(['status' => 1])->all();
        foreach ($types as $type) {
            /** @var GameType $type */
            $games = self::find()->where(['id_type' => $type->id, 'status' => 10])->limit(10)->orderBy(['id' => SORT_DESC])->all();
            foreach ($games as $game) {
                /** @var Game $game */
                $user1 = User::findOne($game->user_1);
                $user2 = User::findOne($game->user_2);
                $logs[$type->id][] = [
                    'id' => $game->id,
                    'user1' => [
                        'id' => $user1->id,
                        'username' => $user1->username,
                        'avatar' => $user1->userInfo->avatar,
                        'user_choice' => $game->user_1_choice,
                    ],
                    'user2' => [
                        'id' => $user2->id,
                        'username' => $user2->username,
                        'avatar' => $user2->userInfo->avatar,
                        'user_choice' => $game->user_2_choice,
                    ],
                    'winner' => $game->winner
                ];
            }

        }
        return $logs;
    }

    public static function getGamesHistory($req)
    {
        if ($req === 'tournament') {
            $tournaments = Tournament::find()->where(['status' => 10])->with('games')->limit(10)->orderBy(['id' => SORT_DESC])->all();
            $res = [];
            /** @var Tournament $tournament */
            foreach ($tournaments as $tournament) {
                $bank = 0;
                $winner = null;
                /** @var Game $game */
                foreach ($tournament->games as $game) {
                    $bank = $bank + $game->win_points;
                }
                $players = unserialize($tournament->players);
                if ($players) {
                    $winner = User::findOne(array_shift($players)['id']);
                }
                $res[] = [
                    'id' => $tournament->id,
                    'bank' => $bank,
                    'winner' => [
                        'id' => $winner->id,
                        'userName' => $winner->username,
                        'avatar' => $winner->userInfo->avatar
                    ]
                ];
            }

            return $res;
        }
        if ($req === 'duel') {
            $games = Game::find()->where(['status' => 10])->andWhere('tournament_id IS NULL')->limit(10)->orderBy(['id' => SORT_DESC])->all();
            $res = [];
            /** @var Game $game */
            foreach ($games as $game) {
                if ($game->winner > 0) {
                    /** @var User $winner */
                    $winner = $game->winner === $game->user_1 ? $game->user1 : $game->user2;
                } else {
                    $winner = null;
                }

                $res[] = [
                    'id' => $game->id,
                    'bank' => $game->win_points,
                    'winner' => $winner ? [
                        'id' => $winner->id,
                        'avatar' => $winner->userInfo->avatar,
                        'userName' => $winner->username
                    ] : null
                ];
            }
            return $res;
        }
        throw new BadRequestHttpException();
    }

    public static function GameExit($user_id)
    {
        /** @var Game $game */
        $game = self::find()->where(['status' => 0])->andWhere('tournament_id is NULL')->andWhere(['or', ['user_1' => $user_id], ['user_2' => $user_id]])->one();
        if ($game) {
            if ($game->user_1 === $user_id) {
                $game->user_1 = null;
            } elseif ($game->user_2 === $user_id) {
                $game->user_2 = null;
            }
            $game->save();
        }

    }

}
