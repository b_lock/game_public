<?php

namespace app\models;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;


class EventEmitter
{
    public static function refresh()
    {
        $client = new Client(new Version1X('http://localhost:8081'));
        $client->initialize();
        $client->of('/server');

        $ponger = Ponger::getPonger();

        $client->emit('refresh', [
            'key' => \Yii::$app->params['NodeKey'],
            'data' => $ponger]);
        $client->close();
    }


}