<?php
namespace app\models;

use Yii;
use yii\base\Model;

/**
 * Login form
 */
class AddPointsForm extends Model
{
    public $user_id;
    public $points;
    public $notes;
    public $type;

    private $_user;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'points', 'type'], 'required'],
            [['user_id', 'points'], 'integer'],
            [['notes', 'type'], 'string'],
            ['type', 'in', 'range' => ['add', 'sub', 'free_points', 'duel_winner','duel_loose','Tournament_winner','Tournament_loose']]
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            $payments_log = new PaymentsLog();
            $user_info = UserInfo::findOne(['user_id' => $this->user_id]);
            if ($this->type == 'add') {
                $user_info->balance = $user_info->balance + $this->points;
                $user_info->save();
            } elseif ($this->type == 'sub') {
                $user_info->balance = $user_info->balance - $this->points;
                $user_info->save();
            } else {
                $user_info->balance = $user_info->balance + $this->points;
                $user_info->save();
            }
            $payments_log->tag = $this->type;
            $payments_log->points = $this->points;
            $payments_log->user_id = $this->user_id;
            $payments_log->notes = $this->notes;
            $payments_log->save();

            return true;
        }
        return false;

    }


    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
