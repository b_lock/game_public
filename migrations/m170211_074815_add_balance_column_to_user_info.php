<?php

use yii\db\Migration;

class m170211_074815_add_balance_column_to_user_info extends Migration
{
    public function up()
    {
        $this->addColumn('user_info', 'balance', $this->decimal(14,2)->after('user_id'));
    }

    public function down()
    {
        $this->dropColumn('user_info', 'balance');
    }
}
