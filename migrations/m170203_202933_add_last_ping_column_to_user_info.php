<?php

use yii\db\Migration;

class m170203_202933_add_last_ping_column_to_user_info extends Migration
{
    public function up()
    {
        $this->addColumn('user_info', 'last_ping', $this->integer()->after('notes'));

    }

    public function down()
    {
        $this->dropColumn('user_info', 'last_ping');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
