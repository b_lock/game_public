<?php

use yii\db\Migration;

/**
 * Handles the creation of table `game`.
 */
class m161123_152909_create_game_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('game', [
            'id' => $this->primaryKey(),
            'id_type' => $this->integer()->notNull(),
            'status' => $this->integer()->defaultValue(0)->notNull()->comment('статус игры: в ожидании, завершена'),
            'user_1' => $this->integer(),
            'user_2' => $this->integer(),
            'user_1_choice' => $this->string(),
            'user_2_choice' => $this->string(),
            'winner' => $this->integer()->comment('id победителя'),
            'tournament_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-game-user_1',
            'game',
            'user_1'
        );
        $this->addForeignKey(
            'fk-game-user_1',
            'game',
            'user_1',
            'user',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-game-user_2',
            'game',
            'user_2'
        );
        $this->addForeignKey(
            'fk-game-user_2',
            'game',
            'user_2',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-game-id_type',
            'game',
            'id_type'
        );
        $this->addForeignKey(
            'fk-game-id_type',
            'game',
            'id_type',
            'game_type',
            'id',
            'CASCADE'
        );
        $this->createIndex(
            'idx-game-tournament_id',
            'game',
            'tournament_id'
        );
        $this->addForeignKey(
            'fk-game-tournament_id',
            'game',
            'tournament_id',
            'tournament',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropForeignKey(
            'fk-game-tournament_id',
            'game'
        );
        $this->dropForeignKey(
            'fk-game-id_type',
            'game'
        );

        $this->dropForeignKey(
            'fk-game-user_2',
            'game'
        );

        $this->dropForeignKey(
            'fk-game-user_1',
            'game'
        );

        $this->dropIndex(
            'idx-game-tournament_id',
            'game'
        );

        $this->dropIndex(
            'idx-game-id_type',
            'game'
        );

        $this->dropIndex(
            'idx-game-user_2',
            'game'
        );

        $this->dropIndex(
            'idx-game-user_1',
            'game'
        );

        $this->dropTable('game');
    }
}
