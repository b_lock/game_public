<?php

use yii\db\Migration;

class m161228_090520_add_column_tournament_to_game_type extends Migration
{
    public function up()
    {
        $this->addColumn('game_type', 'tournament', 'integer default 0');
        $this->update('game_type', ['tournament' => 1], ['id' => 5]);

    }

    public function down()
    {
        $this->dropColumn('game_type', 'tournament');
    }
}
