<?php

use yii\db\Migration;

class m170222_095138_create_table_faq extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('faq', [
            'id' => $this->primaryKey(),

            'title' => $this->string()->comment('Название'),

            'parent_id' => $this->integer()->defaultValue(0)->comment('Родительская категория'),

            'description' => $this->text()->comment('Описание'),

            'status' => $this->integer()->comment('Статус'),

            'created_at' => $this->integer()->notNull(),

            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex(
            'idx-faq-parent_id',
            'faq',
            'parent_id'
        );
        $this->addForeignKey(
            'fk-faq-parent_id',
            'faq',
            'parent_id',
            'faq',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-faq-parent_id',
            'faq'
        );

        $this->dropIndex(
            'idx-faq-parent_id',
            'faq'
        );

        $this->dropTable('faq');
    }
}
