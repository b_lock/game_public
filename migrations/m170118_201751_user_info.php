<?php

use yii\db\Migration;

class m170118_201751_user_info extends Migration
{
    public function up()
    {
        $this->createTable('user_info', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'avatar' => $this->string(),
            'notes' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-user_info-user_id',
            'user_info',
            'user_id'
        );
        $this->addForeignKey(
            'fk-user_info-user_id',
            'user_info',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-user_info-user_id',
            'user_info'
        );

        $this->dropIndex(
            'idx-user_info-user_id',
            'user_info'
        );

        $this->dropTable('user_info');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
