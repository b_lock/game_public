<?php

use yii\db\Migration;

/**
 * Handles adding start_time to table `tournament`.
 */
class m170122_153415_add_start_time_column_to_tournament_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('tournament', 'start_time', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('tournament', 'start_time');
    }
}
