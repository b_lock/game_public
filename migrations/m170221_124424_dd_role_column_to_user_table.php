<?php

use yii\db\Migration;
use app\models\User;

class m170221_124424_dd_role_column_to_user_table extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'role', $this->string()->after('status'));


    }

    public function down()
    {
        $this->dropColumn('user', 'role');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
