<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tournament`.
 */
class m161123_152031_create_tournament_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tournament', [
            'id'         => $this->primaryKey(),
            'status'     => $this->integer()->defaultValue(0)->notNull()->comment('статус турнира 0:ждет игроков, 1: обраный отсчет, 2: идет, завершён'),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);


    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tournament');
    }
}
