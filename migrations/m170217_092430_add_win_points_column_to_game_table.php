<?php

use yii\db\Migration;

/**
 * Handles adding win_points to table `game`.
 */
class m170217_092430_add_win_points_column_to_game_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('game', 'win_points', $this->integer()->after('winner'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('game', 'win_points');
    }
}
