<?php

use yii\db\Migration;

class m170211_091258_add_win_points_and_losing_points_column_to_user_info extends Migration
{
    public function up()
    {
        $this->addColumn('user_info', 'win_points', $this->integer()->after('balance'));
        $this->addColumn('user_info', 'losing_points', $this->integer()->after('win_points'));
    }

    public function down()
    {
        $this->dropColumn('user_info', 'win_points');
        $this->dropColumn('user_info', 'losing_points');
    }
}
