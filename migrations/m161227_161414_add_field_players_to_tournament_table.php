<?php

use yii\db\Migration;

class m161227_161414_add_field_players_to_tournament_table extends Migration
{
    public function up()
    {
        $this->addColumn('tournament', 'players', 'text');

    }

    public function down()
    {
        $this->dropColumn('tournament', 'players');

        /*
        // Use safeUp/safeDown to run migration code within a transaction
        public function safeUp()
        {
        }

        public function safeDown()
        {
        }
        */
    }
}
