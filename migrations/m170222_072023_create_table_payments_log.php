<?php

use yii\db\Migration;

class m170222_072023_create_table_payments_log extends Migration
{
    public function up()
    {
        $this->createTable('payments_log', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'tag' => $this->string(),
            'points' => $this->integer(),
            'notes' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex(
            'idx-payments_log-user_id',
            'payments_log',
            'user_id'
        );
        $this->addForeignKey(
            'fk-payments_log-user_id',
            'payments_log',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

    }

    public function down()
    {
        $this->dropForeignKey(
            'fk-payments_log-user_id',
            'payments_log'
        );

        $this->dropIndex(
            'idx-payments_log-user_id',
            'payments_log'
        );

        $this->dropTable('payments_log');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
