<?php

use yii\db\Migration;


/**
 * Handles the creation of table `game_type`.
 */
class m161123_151123_create_game_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('game_type', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'description' => $this->text(),
            'rate'        => $this->integer(),
            'status'      => $this->integer()->notNull()->defaultValue(1)->comment('статус типа игры: активна, не активна'),
            'params'      => $this->text()->comment('параметры игры'),
            'created_at'  => $this->integer()->notNull(),
            'updated_at'  => $this->integer()->notNull(),
        ]);

        $this->insert('game_type', [
            'name'        => 'duel_100',
            'description' => 'duel description 100',
            'rate'        => '100',
            'created_at'  => '1482478133',
            'updated_at'  => '1482478133'
        ]);
        $this->insert('game_type', [
            'name'        => 'duel_500',
            'description' => 'duel description 500',
            'rate'        => '500',
            'created_at'  => '1482478133',
            'updated_at'  => '1482478133'
        ]);
        $this->insert('game_type', [
            'name'        => 'duel_1000',
            'description' => 'duel description 1000',
            'rate'        => '1000',
            'created_at'  => '1482478133',
            'updated_at'  => '1482478133'
        ]);
        $this->insert('game_type', [
            'name'        => 'duel_5000',
            'description' => 'duel description 5000',
            'rate'        => '5000',
            'created_at'  => '1482478133',
            'updated_at'  => '1482478133'
        ]);
        $this->insert('game_type', [
            'name'        => 'duel_tournament',
            'description' => 'duel tournament description',
            'rate'        => '100',
            'created_at'  => '1482478133',
            'updated_at'  => '1482478133'
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {

        $this->dropTable('game_type');
    }
}
