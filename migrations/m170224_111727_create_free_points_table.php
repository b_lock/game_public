<?php

use yii\db\Migration;

/**
 * Handles the creation of table `free_points`.
 */
class m170224_111727_create_free_points_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('free_points', [
            'id' => $this->primaryKey(),
            'points'=>$this->integer()->notNull(),
            'code'=>$this->string()->notNull()->unique(),
            'paid_off'=>$this->boolean()->defaultValue(false)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('free_points');
    }
}
