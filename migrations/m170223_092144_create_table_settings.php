<?php

use yii\db\Migration;

class m170223_092144_create_table_settings extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('settings', [
            'id' => $this->primaryKey(),

            'support_email' => $this->string()->notNull()->unique()->comment('Email техподдержки'),

            'duel_time' => $this->integer()->comment('Время дуэли'),

            'tournament_time' => $this->integer()->comment('Время турнира'),

            'tournament_players_count' => $this->integer()->comment('Минимальное количество игроков для начала турнира')

        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
